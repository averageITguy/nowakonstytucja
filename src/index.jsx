require('styles/Main.style.scss');

import 'core-js/fn/object/assign';
import React from 'react';
import ReactDOM from 'react-dom';

import './services/moment.setup';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import MainLayout from './components/Reusable/Main.layout';

// Import application components
import Home from './components/Home/HomePage.component';

import InitiativeList from './components/Initiatives/InitiativeList.component';
import InitiativeDetails from './components/Initiatives/InitiativeDetails.component';
import InitiativeNew from './components/Initiatives/InitiativeProposalNew.component';

import PollList from './components/Polls/PollList/PollList.component';
import PollDetails from './components/Polls/PollDetails.component';
import PollArchiveList from './components/Polls/PollArchiveList/PollArchiveList.component';

import ParliamentList from './components/Parliament/ParliamentList/ParliamentList.component';
import ParliamentDetails from './components/Parliament/ParliamentDetails.component';
import ParliamentNew from './components/Parliament/ParliamentProposalNew.component';

import ConstitutionProblemList from './components/Constitution/ConstitutionProblemList/ConstitutionProblemList.component';
import ConstitutionProblemDetails from './components/Constitution/ConstitutionProblemDetails/ConstitutionProblemDetails.component'
import ConstitutionProblemNew from './components/Constitution/ConstitutionProblemProposalNew.component';
import ConstitutionSolutionNew from './components/Constitution/ConstitutionSolutionProposalNew.component';
import ConstitutionSolutionDetails from './components/Constitution/ConstitutionSolutionDetails.component';

import About from './components/About/About.component';
import Terms from './components/Terms/Terms.component';

import ModeratePollProposalList from './moderator/PollProposalList/PollProposalList.component';
import ModeratePollExchange from './moderator/PollExchange/PollExchange.component';
import ModerateInitiativeProposalList from './moderator/InitiativeProposalList.component';
import ModerateParliamentProposalsList from './moderator/ParliamentProposalList.component';
import ModerateProblemProposalsList from './moderator/ProblemProposalList.component';
import ModerateSolutionProposalsList from './moderator/SolutionProposalList.component';

import CloseAfterLoggedIn from './components/Auth/CloseAfterLoggedIn.component';

// Routing components
import { Router, Route, browserHistory } from 'react-router';
let hashHistory = browserHistory;

//auth
import authService from './services/auth.service';

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();


authService.init().then(() => {
    // Render the main component into the dom
    ReactDOM.render(
        <MuiThemeProvider>
            <Router onUpdate={fixScroll} history={hashHistory}>
                <Route component={MainLayout}>
                    <Route exact path="/" component={Home} />

                    <Route path="/problemy" component={ConstitutionProblemList} />
                    <Route path="/problemy/nowy" component={ConstitutionProblemNew} />
                    <Route path="/problem/:id" component={ConstitutionProblemDetails} />

                    <Route path="/problem/:problemId/rozwiazanie/:solutionId"
                        component={ConstitutionSolutionDetails} />
                    <Route path="/rozwiazania/nowe/:problemId"
                        component={ConstitutionSolutionNew} />

                    <Route path="/petycje" component={InitiativeList} />
                    <Route path="/petycje/nowa" component={InitiativeNew} />
                    <Route path="/petycja/:id" component={InitiativeDetails} />

                    <Route path="/sondaze" component={PollList} />
                    <Route path="/sondaze/archiwum" component={PollArchiveList} />
                    <Route path="/sondaz/:id" component={PollDetails} />

                    <Route path="/ustawy" component={ParliamentList} />
                    <Route path="/ustawy/nowa" component={ParliamentNew} />
                    <Route path="/ustawa/:id" component={ParliamentDetails} />

                    <Route path="/moderate/polls" component={ModeratePollProposalList} />
                    <Route path="/moderate/polls/exchange/:proposalId"
                        component={ModeratePollExchange} />
                    <Route path="/moderate/initiatives"
                        component={ModerateInitiativeProposalList} />
                    <Route path="/moderate/parliaments"
                        component={ModerateParliamentProposalsList} />
                    <Route path="/moderate/problems" component={ModerateProblemProposalsList} />
                    <Route path="/moderate/solutions" component={ModerateSolutionProposalsList} />

                    <Route path="/closeAfterLoggedIn" component={CloseAfterLoggedIn} />

                    <Route path="/onas" component={About} />
                    <Route path="/regulamin" component={Terms} />
                </Route>
            </Router>
        </MuiThemeProvider>, document.getElementById('main-container'));
});


//react-router scroll bugfix
function fixScroll() {
    let {
        action
    } = this.state.location;

    if (action === 'PUSH') {
        window.scrollTo(0, 0);
    }
}
