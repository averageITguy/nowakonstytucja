import React from 'react';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import ImagePickerService from '../services/imagePicker.service';

import './moderator.style.scss';

export default class ImagePicker extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            images: null
        };
    }

    getImages = () => {
        ImagePickerService.getImages(this.props.imgType).then(images => {
            this.setState({images});
        });
    };

    handleOpen = () => {
        this.getImages();
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false, images: null});
    };

    imageSelected = imgId => {
        this.handleClose();
        this.props.imageSelected(this.props.entityId, this.props.imgType, imgId);
    };

    generateImages = () => {
        if(!this.state.images) return null;

        return this.state.images.map(image => {
            return (
                <div className="responsive" key={image}>
                    <div className="gallery">
                        <img src={`../images/picker/${this.props.imgType}/${image}`}
                             width="600" height="400" />

                        <div className="desc">
                            <button
                                onClick={() => this.imageSelected(image)}>Wybierz
                            </button>
                        </div>
                    </div>
                </div>
            );
        });
    };

    render() {
        const actions = [
            <FlatButton
                label="Anuluj"
                primary={true}
                onTouchTap={this.handleClose}
            />
        ];

        return (
            <span>
                <button onClick={this.handleOpen}
                        className="action">
                    Przypisz
                </button>

                <Dialog
                    title={'Wybierz obraz, typ: ' + this.props.imgType}
                    actions={actions}
                    modal={false}
                    contentClassName="image-picker"
                    open={this.state.open}
                    bodyStyle={{'overflowY': 'auto'}}
                    onRequestClose={this.handleClose}>

                    {this.generateImages()}

                    <div className="clearfix"></div>
                </Dialog>
            </span>
        );
    }
}
