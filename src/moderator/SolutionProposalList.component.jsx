import React from 'react';
import {Row, Col, Grid} from 'react-flexbox-grid';
import ModeratorMenu from './ModeratorMenu.component';
import ConstitutionProposalsService from '../services/constitutionProposals.service';

import './moderator.style.scss';

export default class ParliamentProposalslList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            proposals: null,
            userId: null
        };

        this.type = 'solution';

        this.getProposals();

    }

    getProposals() {
        ConstitutionProposalsService.getSolutionProposals().then(proposals => {
            this.setState({proposals});
        });
    }

    acceptProposal(proposal) {


        ConstitutionProposalsService.acceptProposal(this.type, proposal).then(() => {
            this.getProposals();
        });
    }

    markAsInteresting(problem, proposalId) {


        const proposal = problem.solutions.find(proposal => proposal._id === proposalId);

        proposal.interesting = !proposal.interesting;

        ConstitutionProposalsService.markAsInteresting(this.type, proposal._id, proposal.interesting)
            .then(() => {
                this.setState({
                    proposals: this.state.proposals
                });
            });
    }

    rejectProposal(problem, proposalId) {
        

        ConstitutionProposalsService.rejectProposal(this.type, proposalId)
            .then(() => {
                const proposalIdx = problem.solutions.findIndex(
                    proposal => proposal._id === proposalId);

                problem.solutions.splice(proposalIdx, 1);

                this.setState({
                    proposals: this.state.proposals
                });
            });
    }

    generateList() {
        // Problem1 and its solution proposals, Problem2 and its solution proposals etc
        return this.state.proposals.map(wrapper => {
            return (
                <div key={wrapper._id}>
                    <div className="poll-proposal">
                        <h1 className="problem-separator">Problem: {wrapper.problem.descShort}</h1>
                    </div>
                    <div>
                        {this.generateSolutionProposals(wrapper)}
                    </div>
                </div>
            );
        });
    }

    generateSolutionProposals(problem) {
        return problem.solutions.map(proposal => {
            return (
                <div className="list-proposal-wrapper solutions-separator" key={proposal._id}>
                    <div className="list-proposal-item">
                        <h2>{proposal.descShort}</h2>
                        <p>{proposal.descDetailed}</p>
                    </div>

                    <div className="list-proposal-actions">
                        <button onClick={this.markAsInteresting.bind(this, problem, proposal._id)}
                                className={proposal.interesting ? 'action active' : 'action'}>
                            Ciekawe
                        </button>
                        <button onClick={this.rejectProposal.bind(this, problem, proposal._id)}
                                className="action">
                            Odrzuć
                        </button>
                        <button onClick={this.acceptProposal.bind(this, proposal)}
                                className="action">
                            Zatwierdź
                        </button>
                    </div>
                </div>
            );
        });
    }

    render() {
        if(!this.state.proposals) return null;

        return (
            <div>
                <div className="col-xs-12 moderator-panel-jumbotron">
                    <Grid fluid>
                        <Row>
                            <Col xs={10} xsOffset={1}>
                                <h1>Panel moderatora</h1>
                            </Col>
                        </Row>
                    </Grid>
                    <ModeratorMenu />
                </div>
                <Grid fluid className="moderator-panel-content">
                    <Row>
                        <Col xs={10} xsOffset={1}>
                            <header className="proposals-filters">
                                <input placeholder="Wyszukaj..." />
                                <div className="button-filter-panel">
                                    <button className="filter-btn">Dzisiaj</button>
                                    <button className="filter-btn">Ostatatnie 3 dni</button>
                                    <button className="filter-btn">Ten tydzień</button>
                                    <button className="filter-btn">Ciekawe</button>
                                </div>
                            </header>
                            <div>
                                {this.generateList()}
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}