import React from 'react';

import ConstitutionProposalsService from '../services/constitutionProposals.service';

import ModeratorMenu from './ModeratorMenu.component';
import { Row, Col, Grid } from 'react-flexbox-grid';

import './moderator.style.scss';

export default class ParliamentProposalslList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            proposals: null,
            userId: null
        };

        this.type = 'problem';

        this.getProposals();

    }

    getProposals() {
        ConstitutionProposalsService.getProblemProposals().then(proposals => {
            this.setState({proposals});
        });
    }

    acceptProposal(proposal) {


        ConstitutionProposalsService.acceptProposal(this.type, proposal).then(() => {
            this.getProposals();
        });
    }

    markAsInteresting(proposalId) {


        const proposal = this.state.proposals.find(proposal => proposal._id === proposalId);

        proposal.interesting = !proposal.interesting;

        ConstitutionProposalsService.markAsInteresting(this.type, proposal._id, proposal.interesting)
            .then(() => {
                this.setState({
                    proposals: this.state.proposals
                });
            });
    }

    rejectProposal(proposalId) {


        ConstitutionProposalsService.rejectProposal(this.type, proposalId)
            .then(() => {
                const proposalIdx = this.state.proposals.findIndex(
                    proposal => proposal._id === proposalId);

                this.state.proposals.splice(proposalIdx, 1);

                this.setState({
                    proposals: this.state.proposals
                });
            });
    }

    generateList() {
        return this.state.proposals.map(proposal => {
            return (
                <div className="list-proposal-wrapper" key={proposal._id}>
                    <div className="list-proposal-item">
                        <h2>Alan Czerwiński, 2 dni temu</h2>

                        <h3>{proposal.descShort}</h3>

                        <p>{proposal.category}</p>

                        <p>{proposal.descDetailed}</p>
                    </div>

                    <div className="list-proposal-actions">
                        <button onClick={this.markAsInteresting.bind(this, proposal._id)}
                                className={proposal.interesting ? 'action active' : 'action'}>
                            Ciekawe
                        </button>
                        <button onClick={this.rejectProposal.bind(this, proposal._id)}
                                className="action">
                            Odrzuć
                        </button>
                        <button onClick={this.acceptProposal.bind(this, proposal)}
                                className="action">
                            Zatwierdź
                        </button>
                    </div>
                </div>
            );
        });
    }

    render() {
        if(!this.state.proposals) return null;

        return (
            <div>
                <div className="col-xs-12 moderator-panel-jumbotron">
                    <Grid fluid>
                        <Row>
                            <Col xs={10} xsOffset={1}>
                                <h1>Panel moderatora</h1>
                            </Col>
                        </Row>
                    </Grid>
                    <ModeratorMenu />
                </div>
                <Grid fluid className="moderator-panel-content">
                    <Row>
                        <Col xs={10} xsOffset={1}>
                            <header className="proposals-filters">
                                <input placeholder="Wyszukaj..." />
                                <div className="button-filter-panel">
                                    <button className="filter-btn">Dzisiaj</button>
                                    <button className="filter-btn">Ostatatnie 3 dni</button>
                                    <button className="filter-btn">Ten tydzień</button>
                                    <button className="filter-btn">Ciekawe</button>
                                </div>
                            </header>
                            <div>
                                {this.generateList()}
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}