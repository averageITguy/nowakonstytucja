import React from 'react';

import PollsService from '../../services/polls.service';
import PollsProposalsService from '../../services/pollsProposals.service';

import {Row, Col, Grid} from 'react-flexbox-grid';
import ModeratorMenu from '../ModeratorMenu.component';

import PollExchangeListItem from './_listItem.component';

import '../moderator.style.scss';

export default class PollExchange extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            polls: null,
            proposal: null,
            userId: null
        };

        this.getPolls();
        this.getProposal(props.params.proposalId);
    }

    getPolls() {
        PollsService.getPollsForExchange().then(polls => {
            this.setState({polls});
        });
    }

    getProposal(proposalId) {
        PollsProposalsService.getProposal(proposalId)
            .then(proposal => {
                this.setState({
                    proposal
                });
            });
    }

    addPoll() {
        PollsService.exchange(null, this.state.proposal).then(() => {
            this.getPolls();
        });
    }

    handlePollExchange() {
        this.getPolls();
    }

    generateList() {
        return this.state.polls.map((poll, idx) => {
            return (
                <PollExchangeListItem key={poll._id}
                                      poll={poll}
                                      proposal={this.state.proposal}
                                      handlePollExchange={this.handlePollExchange.bind(this)}
                                      idx={idx}
                                      userId={this.state.userId} />
            );
        });
    }

    generateAddBtn() {
        return (
            <button onClick={this.addPoll.bind(this)}>+ Dodaj</button>
        );
    }

    render() {
        if(!this.state.polls) return null;

        return (
            <div>
                <div className="col-xs-12 moderator-panel-jumbotron">
                    <Grid fluid>
                        <Row>
                            <Col xs={10} xsOffset={1}>
                                <h1>Panel moderatora</h1>
                            </Col>
                        </Row>
                    </Grid>
                    <ModeratorMenu />
                </div>
                <Grid fluid className="moderator-panel-content">
                    <Row>
                        <Col xs={10} xsOffset={1}>
                            <header className="proposals-filters exchange-poll-button">
                                {this.generateAddBtn()}
                            </header>
                            <div>
                                {this.generateList()}
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}