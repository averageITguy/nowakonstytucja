//TODO: dodac modal z potwierdzeniem odrzucenia

import React from 'react';

import PollsService from '../../services/polls.service';

import '../moderator.style.scss';

export default function PollExchangeListItem(props) {
    const {
        poll,
        proposal,
        userId,
        idx,
        handlePollExchange
    } = props;

    const exchange = (dbUser) => {
        const userLoggedIn = dbUser && dbUser._id || userId;

        if(userLoggedIn) {
            PollsService.exchange(poll._id, proposal).then(() => {
                handlePollExchange();
            });
        } else {

        }
    };

    return (
        <div className="list-proposal-wrapper">
            <h3>#{idx + 1} Zatwierdzony 1 tydzień temu</h3>
            <div className="list-proposal-item">
                <h2 className="question">
                    {poll.question}
                </h2>
                <p>Ilość głosów: {poll.votesCtr}</p>
                <p>Głosów przez ostatnie 2 dni: 120</p>
            </div>
            <div className="list-proposal-actions">
                <button className="action" onClick={exchange}>Wymień</button>
            </div>
        </div>
    );

}
