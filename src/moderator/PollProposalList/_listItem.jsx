//TODO: dodac modal z potwierdzeniem odrzucenia

import React from 'react';

import PollsProposalsService from '../../services/pollsProposals.service';

import ImagePicker from '../ImagePicker.component';

import '../moderator.style.scss';

export default function PollProposalListItem(props) {
    const {
        proposal,
        userId,
        handleMarkAsInteresting,
        handleProposalRejection,
        handleImageAssigned
    } = props;

    proposal.images = proposal.images || {};

    const markAsInteresting = (dbUser) => {
        const userLoggedIn = dbUser && dbUser._id || userId;

        if(userLoggedIn) {
            PollsProposalsService.markAsInteresting(proposal._id, !proposal.interesting).then(() => {
                handleMarkAsInteresting(proposal._id, !proposal.interesting);
            });
        } else {
            //TODO
        }
    };

    const rejectProposal = (dbUser) => {
        const userLoggedIn = dbUser && dbUser._id || userId;

        if(userLoggedIn) {
            PollsProposalsService.rejectProposal(proposal._id).then(() => {
                handleProposalRejection(proposal._id);
            });
        } else {
            //TODO
        }
    };

    const generateImageHandler = (imgType) => {
        const currImg = proposal[imgType];

        if(currImg) {
            return (
                <a href={`../images/picker/${imgType}/${currImg}`} target="_blank">{currImg}</a>
            );
        } else {
            return (
                <ImagePicker entityId={proposal._id}
                             imgType={imgType}
                             imageSelected={imageSelected} />
            );
        }
    };

    const imageSelected = (entityId, imgType, imgId) => {
        handleImageAssigned(entityId, imgType, imgId);
    };

    const exchange = () => {
        location.href = '/moderate/polls/exchange/' + proposal._id;
    };

    return (
        <div className="list-proposal-wrapper">
            <div className="list-proposal-item">
                <h2>Alan Czerwiński, 2 dni temu</h2>
                <h3>{proposal.question}</h3>
                <p>{proposal.descShort}</p>

                <div>
                    Zdjęcie lista:
                    {generateImageHandler('200x150')}
                </div>
                <div>
                    Zdjęcie detale:
                    {generateImageHandler('300x240')}
                </div>
                <div>
                    Zdjęcie swiper:
                    {generateImageHandler('600x200')}
                </div>

            </div>

            <div className="list-proposal-actions">
                <button onClick={markAsInteresting}
                        className={proposal.interesting ? 'action active' : 'action'}>
                    Ciekawe
                </button>
                <button onClick={rejectProposal}
                        className="action">
                    Odrzuć
                </button>
                <button onClick={exchange}
                        disabled={!proposal['200x150'] || !proposal['300x240'] || !proposal['600x200']}
                        className="action">
                    Wymień
                </button>

            </div>
        </div>
    );
}
