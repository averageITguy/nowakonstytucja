import React from 'react';

import {Row, Col, Grid} from 'react-flexbox-grid';

import PollsProposalsService from '../../services/pollsProposals.service';
import ImageService from '../../services/imagePicker.service';

import PollProposalListItem from './_listItem';
import ModeratorMenu from '../ModeratorMenu.component';

import '../moderator.style.scss';

export default class PollProposalList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            proposals: null,
            userId: null
        };

        this.getPollsProposals();
    }

    getPollsProposals() {
        PollsProposalsService.getPollsProposals().then(proposals => {
            this.setState({proposals});
        });
    }

    handleMarkAsInteresting(proposalId, interesting) {
        const proposal = this.state.proposals.find(proposal => proposal._id === proposalId);

        proposal.interesting = interesting;

        this.setState({
            proposals: this.state.proposals
        });
    }

    handleImageAssigned(proposalId, imgType, imgId) {
        ImageService.addImage('poll', proposalId, imgType, imgId)
            .then(() => {
                const proposal = this.state.proposals.find(proposal => proposal._id === proposalId);

                proposal[imgType] = imgId;

                this.setState({
                    proposals: this.state.proposals
                });
            });
    }

    handleProposalRejection(proposalId) {
        const proposalIdx = this.state.proposals.findIndex(proposal => proposal._id === proposalId);

        this.state.proposals.splice(proposalIdx, 1);

        this.setState({
            proposals: this.state.proposals
        });
    }

    generateList() {
        return this.state.proposals.map(proposal => {
            return (
                <PollProposalListItem key={proposal._id}
                                      proposal={proposal}
                                      handleMarkAsInteresting={this.handleMarkAsInteresting.bind(this)}
                                      handleProposalRejection={this.handleProposalRejection.bind(this)}
                                      handleImageAssigned={this.handleImageAssigned.bind(this)}
                                      userId={this.state.userId} />
            );
        });
    }

    render() {
        return (
            <div>
                <div className="col-xs-12 moderator-panel-jumbotron">
                    <Grid fluid>
                        <Row>
                            <Col xs={10} xsOffset={1}>
                                <h1>Panel moderatora</h1>
                            </Col>
                        </Row>
                    </Grid>
                    <ModeratorMenu />
                </div>
                <Grid fluid className="moderator-panel-content">
                    <Row>
                        <Col xs={10} xsOffset={1}>
                            <header className="proposals-filters">
                                <input placeholder="Wyszukaj..." />
                                <div className="button-filter-panel">
                                    <button className="filter-btn">Dzisiaj</button>
                                    <button className="filter-btn">Ostatatnie 3 dni</button>
                                    <button className="filter-btn">Ten tydzień</button>
                                    <button className="filter-btn">Ciekawe</button>
                                </div>
                            </header>
                            <div>
                                {this.state.proposals && this.generateList()}
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}