import React from 'react';

import {Row, Col, Grid} from 'react-flexbox-grid';

import ParliamentProposalsService from '../services/parliamentProposals.service';
import ImageService from '../services/imagePicker.service';

import ModeratorMenu from './ModeratorMenu.component';

import ImagePicker from './ImagePicker.component';

import './moderator.style.scss';

export default class ParliamentProposalslList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            proposals: null,
            userId: null
        };

        this.getProposals();

    }

    getProposals() {
        ParliamentProposalsService.getProposals().then(proposals => {
            this.setState({proposals});
        });
    }

    acceptProposal(proposal) {


        ParliamentProposalsService.acceptProposal(proposal).then(() => {
            this.getProposals();
        });

    }

    markAsInteresting(proposalId) {


        const proposal = this.state.proposals.find(proposal => proposal._id === proposalId);

        proposal.interesting = !proposal.interesting;

        ParliamentProposalsService.markAsInteresting(proposal._id, proposal.interesting).then(() => {
            this.setState({
                proposals: this.state.proposals
            });
        });
    }

    rejectProposal(proposalId) {


        ParliamentProposalsService.rejectProposal(proposalId).then(() => {
            const proposalIdx = this.state.proposals.findIndex(
                proposal => proposal._id === proposalId);

            this.state.proposals.splice(proposalIdx, 1);

            this.setState({
                proposals: this.state.proposals
            });
        });
    }

    generateImageHandler(proposal, imgType) {
        const currImg = proposal[imgType];

        if(currImg) {
            return (
                <a href={`../images/picker/${imgType}/${currImg}`} target="_blank">{currImg}</a>
            );
        } else {
            return (
                <ImagePicker imgType={imgType}
                             imageSelected={this.imageSelected.bind(this)}
                             entityId={proposal._id} />
            );
        }
    }

    imageSelected(proposalId, imgType, imgId) {
        ImageService.addImage('parliament', proposalId, imgType, imgId).then(() => {
            const proposal = this.state.proposals.find(proposal => proposal._id === proposalId);

            proposal[imgType] = imgId;

            this.setState({
                proposals: this.state.proposals
            });
        });
    }

    generateList() {
        return this.state.proposals.map(proposal => {
            return (
                <div className="list-proposal-wrapper" key={proposal._id}>
                    <div className="list-proposal-item">
                        <h2>Alan Czerwiński, 2 dni temu</h2>

                        <h3>{proposal.title}</h3>

                        <p>{proposal.printNb}</p>

                        <p>{proposal.descDetailed}</p>

                        <div className="link-wrapper">
                            <a href={proposal.legislationUrl} target="_blank">
                                Przebieg procesu legislacyjnego
                            </a>
                        </div>

                        <a href={proposal.pdfFileUrl} target="_blank">
                            <i className="icon icon-pdf"></i>Pobierz projekt ustawy
                        </a>

                        <div>
                            Zdjęcie lista:
                            {this.generateImageHandler(proposal, '200x150')}
                        </div>
                        <div>
                            Zdjęcie detale:
                            {this.generateImageHandler(proposal, '300x240')}
                        </div>
                    </div>

                    <div className="list-proposal-actions">
                        <button onClick={this.markAsInteresting.bind(this, proposal._id)}
                                className={proposal.interesting ? 'action active' : 'action'}>
                            Ciekawe
                        </button>
                        <button onClick={this.rejectProposal.bind(this, proposal._id)}
                                className="action">
                            Odrzuć
                        </button>
                        <button disabled={!proposal['200x150'] || !proposal['300x240']}
                                onClick={this.acceptProposal.bind(this, proposal)}
                                className="action">
                            Zatwierdź
                        </button>
                    </div>
                </div>
            );
        });
    }

    render() {
        if(!this.state.proposals) return null;

        return (
            <div>
                <div className="col-xs-12 moderator-panel-jumbotron">
                    <Grid fluid>
                        <Row>
                            <Col xs={10} xsOffset={1}>
                                <h1>Panel moderatora</h1>
                            </Col>
                        </Row>
                    </Grid>
                    <ModeratorMenu />
                </div>
                <Grid fluid className="moderator-panel-content">
                    <Row>
                        <Col xs={10} xsOffset={1}>
                            <header className="proposals-filters">
                                <input placeholder="Wyszukaj..." />
                                <div className="button-filter-panel">
                                    <button className="filter-btn">Dzisiaj</button>
                                    <button className="filter-btn">Ostatatnie 3 dni</button>
                                    <button className="filter-btn">Ten tydzień</button>
                                    <button className="filter-btn">Ciekawe</button>
                                </div>
                            </header>
                            <div>
                                {this.generateList()}
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}