import React from 'react';
import {Row, Col, Grid} from 'react-flexbox-grid';

import AuthService from '../services/auth.service';

import LoggedInBtns from '../components/Auth/LoggedInBtns.component';

export default class ModeratorMenuComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: null
        };

        this.subscribeForUser = this.subscribeForUser.bind(this);
        AuthService.subscribeForUser(this.subscribeForUser);
        AuthService.getUserFromDb();
    }

    subscribeForUser(user) {
        this.setState({user});
    }

    componentWillUnmount() {
        AuthService.unsubscribe(this.subscribeForUser);
    }

    openFacebookLoginForm() {
        AuthService.openFacebookLoginForm();
    }

    getLogInCmp() {
        if(this.state.user) {
            return <LoggedInBtns user={this.state.user} />;
        } else {
            return (
                <a className="rectangle-button"
                   onClick={this.openFacebookLoginForm.bind(this)}>
                    <i className="icon icon-facebook"></i>Zaloguj się
                </a>);
        }
    }

    render() {
        const logger = this.getLogInCmp();

        return (
            <Grid fluid>
                <Row>
                    <Col xs={10} xsOffset={1}>
                        <nav className="moderator-menu menu">
                            <div className="links">
                                <a className="menu-element" href="/moderate/polls">Moderuj
                                    sondaże</a>
                                <a className="menu-element" href="/moderate/problems">Moderuj
                                    problemy</a>
                                <a className="menu-element" href="/moderate/solutions">Moderuj
                                    rozwiązania</a>
                                <a className="menu-element" href="/moderate/parliaments">Moderuj
                                    ustawy</a>
                                <a className="menu-element" href="/moderate/initiatives">Moderuj
                                    petycje</a>

                                {logger}
                            </div>
                        </nav>
                    </Col>
                </Row>
            </Grid>
        );
    }
}