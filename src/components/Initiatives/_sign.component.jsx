import React from 'react';

import InitiativeService from '../../services/initiatives.service';

export default function SignInitiative(props) {

    const sign = () => {
        InitiativeService.sign(props.initiativeId)
            .then(userAlreadyVoted => props.userSignedHandler(userAlreadyVoted));
    };


    const generateSignElement = () => {
        if(!props.userSigned) {
            return (
                <button className="sign-button deny-button rectangle-button" onClick={sign}>
                    Podpisz się!
                </button>
            );
        } else {
            return (
                <div className="signed">Podpisano.</div>
            );
        }
    };

    return (
        <div>
            {generateSignElement()}
        </div>
    );
}
