require('styles/Initiatives.style.scss');

import React from 'react';
import {Link} from 'react-router';

import _ from 'lodash';

import InfiniteScroll from 'react-infinite-scroller';
import Filters from '../Reusable/Filters.component';
import Jumbotron from '../Reusable/Jumbotron.component';
import {Row, Col, Grid} from 'react-flexbox-grid';
import Loader from '../Reusable/Loader.component';

import InitiativeService from '../../services/initiatives.service';

import InitiativesListItem from './_listItem.component';

const image  = require('../../images/loader.svg');

export default class InitiativesList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            initiatives: null,
            activeTab: 'popular',
            currPage: 0,
            loaded: false
        };

        this.moreItemsToPaginate = true;

        this.getInitiatives();
    }

    getInitiatives() {
        let page = this.state.currPage + 1;

        InitiativeService.getInitiatives(this.state.activeTab, page)
            .then(initiatives => {
                if(this.unmounted) return;

                if(page === 1) {//reset list
                    return this.setState({
                        initiatives,
                        currPage: page,
                        loaded: true
                    });
                }

                this.setState({
                    initiatives: _.uniqBy([...this.state.initiatives, ...initiatives], '_id'),
                    currPage: page,
                    loaded: true
                });

                this.moreItemsToPaginate = initiatives.length === 4; //hardcoded - items per page
            });
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    listItemUpdated(updatedInitiative) {
        const initiatives = this.state.initiatives;

        const initiative = initiatives.find(
            _initiative => _initiative._id === updatedInitiative._id);

        Object.assign(initiative, updatedInitiative);

        this.setState({initiatives});
    }

    generateList() {
        if(!this.state.initiatives) return null;

        return this.state.initiatives.map(initiative => {
            return (
                <InitiativesListItem key={initiative._id}
                                     initiative={initiative}
                                     listItemUpdated={this.listItemUpdated.bind(this)} />
            );
        });
    }

    tabClicked(activeTab) {
        if(this.state.activeTab === activeTab) return;

        this.moreItemsToPaginate = true;
        this.setState({
            activeTab: activeTab,
            currPage: 0, //reset
            initiatives: null
        }, ()=> this.getInitiatives());
    }

    render() {
        const list = this.generateList();

        return (
            <Loader loaded={this.state.loaded}>
                <div>
                    <Jumbotron header="Lista petycji obywatelskich"
                               hint="Przeglądaj i podpisuj petycje obywatelskie." />
                    <Grid fluid className="filters-wrapper">
                        <Filters activeTab={this.state.activeTab}
                                 tabClicked={this.tabClicked.bind(this)} />
                    </Grid>
                    <Grid fluid className="initiatives-wrapper">
                        <Row>
                            <Col xs={12} md={10} mdOffset={1} className="initiatives-column">

                                <Link className="suggest-initiative-button" to={'/petycje/nowa'}>
                                    <i className="icon icon-plus"></i>Zaproponuj petycję
                                </Link>

                                <InfiniteScroll
                                    pageStart={0}
                                    loadMore={this.getInitiatives.bind(this)}
                                    hasMore={this.moreItemsToPaginate}
                                    loader={(<div id="loader-wrapper">
                                        <div className="table-row">
                                            <div id="table-cell-loader">
                                                <img src={image} />
                                            </div>
                                        </div>
                                    </div>)}>

                                    <div className="tracks">
                                        {list}
                                    </div>
                                </InfiniteScroll>
                            </Col>
                        </Row>
                    </Grid>
                </div>
            </Loader>
        );
    }
}
