import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  header: {
    marginBottom: [{ unit: 'px', value: 30 }]
  },
  'header > header__title': {
    textDecoration: 'bol'
  },
  control: {
    marginBottom: [{ unit: 'px', value: 10 }],
    border: [{ unit: 'string', value: 'none' }]
  },
  'control > control__label': {
    display: 'block',
    textTransform: 'uppercase',
    color: 'grey'
  },
  'form--submited > control > control__input:invalid': {
    border: [{ unit: 'px', value: 1 }, { unit: 'string', value: 'solid' }, { unit: 'string', value: '#BA3029' }]
  }
});
