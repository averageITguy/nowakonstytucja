import React from 'react';
import {Link} from 'react-router';

import SignInitiative from './_sign.component';

export default function InitiativesListItem(props) {
    const {initiative, listItemUpdated} = props;

    const userSignedHandler = userAlreadyVoted => {
        initiative.userSigned = true;

        if(!userAlreadyVoted) {
            initiative.signsCtr = initiative.signsCtr + 1;
        }

        listItemUpdated(initiative);
    };

    return (
        <div key={initiative._id} className="initiative-item list-item">
            <Link className="plain-style-link" to={'/petycja/' + initiative._id}>
                <img
                    src={initiative && require('../../images/picker/200x150/' + initiative['200x150'])} />

            </Link>
            <div className="initiative-description">
                <Link className="plain-style-link" to={'/petycja/' + initiative._id}>
                    <h2>{initiative.title}</h2>
                </Link>
                <p>{initiative.descShort}</p>
                <Link to={'/petycja/' + initiative._id} className="simple-anchor">
                    Zobacz szczegółowy opis
                </Link>
            </div>
            <div className="sign-initiative">
                <SignInitiative initiativeId={initiative._id}
                                userSigned={initiative.userSigned}
                                userSignedHandler={userSignedHandler} />
                <div className="counter">Ilość podpisów: {initiative.signsCtr}</div>
            </div>
        </div>
    );
}
