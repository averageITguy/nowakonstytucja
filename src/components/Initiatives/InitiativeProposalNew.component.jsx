require('styles/Initiatives.style.scss');

import React from 'react';

import {Row, Col, Grid} from 'react-flexbox-grid';

import './InitiativeNew.style.scss';

import InitiativeProposalService from '../../services/initiativesProposals.service';
import Toast from '../../services/toast.service';

export default class InitiativeProposalNew extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            descShortCount: 0,
            descDetailedCount: 0
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    proposeInitiative(form) {
        const data = new FormData(form);

        InitiativeProposalService.proposeInitiative(data)
            .then(() => {
                Toast.display('Zgłosiłeś propozycję!',
                    'Stanie się ona dostępna dla innych użytkowników po zatwierdzeniu przez moderatora');
                this.props.router.push('/');
            });
    }

    handleSubmit(event) {
        event.preventDefault();

        markFormAsSubmitted(this.form);

        const formValid = this.form.checkValidity();
        if(formValid) {
            this.proposeInitiative(this.form);
        }
    }

    countCharacters(inputId) {
        const element = document.getElementById(inputId),
            count = element.value.length;

        const statePropertyName = `${inputId}Count`;

        this.setState({[statePropertyName]: count});
    }

    componentDidMount() {
        this.form = document.getElementById('initiativeForm');

        disableDefaultBrowserValidation();
    }

    render() {
        return (
            <Grid fluid className="initiatives-wrapper">
                <Row>
                    <Col xs={12} md={10} mdOffset={1} className="add-initiative-form">

                        <header className="header">
                            <h3>Formularz dodawania petycji</h3>
                            <div>Dodaj petycję i skieruj uwagę innych na ważne problemy.
                            </div>
                        </header>

                        <form id="initiativeForm" className="initiative-form">

                            <fieldset className="control">
                                <label htmlFor="title" className="control__label">
                                    Tytuł oficjalny<div className="form-hint">(wymagany)</div>
                                </label>
                                <input type="text" placeholder="Wpisz tytuł..."
                                       className="control__input" maxLength="200"
                                       id="title" name="title" required />
                            </fieldset>

                            <fieldset className="control">
                                <label htmlFor="descShort" className="control__label">
                                    Krótki opis<div className="form-hint">(wymagany)</div>
                                </label>
                                <textarea onChange={() => this.countCharacters('descShort')}
                                          cols="30" rows="3" placeholder="Krótki opis..."
                                          className="control__input" id="descShort" name="descShort"
                                          required maxLength="200"></textarea>
                                <span
                                    className="characters-counter">{this.state.descShortCount}/200</span>
                            </fieldset>

                            <fieldset className="control">
                                <label htmlFor="descDetailed" className="control__label">
                                    Opis szczegółowy<div className="form-hint optional">(opcjonalne)</div>
                                </label>
                                <textarea onChange={() => this.countCharacters('descDetailed')}
                                          cols="30" rows="8" placeholder="Szczegóły..."
                                          className="control__input" id="descDetailed"
                                          name="descDetailed" maxLength="620"></textarea>
                                <span className="characters-counter">{this.state.descDetailedCount}/620</span>
                            </fieldset>

                            <div className="submit-button rectangle-button"
                                 onClick={this.handleSubmit}>Wyślij
                            </div>
                        </form>
                    </Col>
                </Row>
            </Grid>
        );
    }
}

function disableDefaultBrowserValidation() {
    const form = document.getElementById('initiativeForm');
    form.addEventListener('invalid', e => e.preventDefault(), true);
}

function markFormAsSubmitted(form) {
    form.classList.add('form--submited');
}
