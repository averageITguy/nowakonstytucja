import React from 'react';
import InitiativesNew from './InitiativeProposalNew.component';

import { Row, Col, Grid } from 'react-flexbox-grid';

export default class Initiatives extends React.Component {
	render() {
		return (
	      	<Grid fluid>
		        <Row>
		            <Col xs={10} xsOffset={1}>
						<InitiativesNew />
            		</Col>
            	</Row>
            </Grid>
		);
	}
}