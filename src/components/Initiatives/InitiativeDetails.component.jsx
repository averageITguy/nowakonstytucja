require('styles/Initiatives.style.scss');

import React from 'react';
import {Link} from 'react-router';
import {Row, Col, Grid} from 'react-flexbox-grid';

import InitiativeService from '../../services/initiatives.service';

import SignInitiative from './_sign.component';
import Comments from '../Reusable/Comments/Comments.component';
import Jumbotron from '../Reusable/Jumbotron.component';
import Loader from '../Reusable/Loader.component';

export default class Initiative extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            initiative: null,
            loaded: false
        };

        this.getInitiative(props.params.id);
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    getInitiative(initiativeId) {
        InitiativeService.getInitiative(initiativeId).then(initiative => {
            if(!this.unmounted) this.setState({initiative, loaded: true});
        });
    }

    userSignedHandler() {
        const initiative = this.state.initiative;

        initiative.userSigned = true;
        initiative.signsCtr = initiative.signsCtr + 1;

        this.setState({
            initiative
        });
    }

    render() {
        const initiative = this.state.initiative;

        const article = initiative && initiative.article ? (
            <a className="article"
               href={"http://wiadomosci.nowakonstytucja.org/" + initiative.article.url}
               target="_blank">
                <span className="article-badge">Naszym zdaniem</span>
                <span
                    className="article-title">{initiative.article.title}</span>
            </a>
        ) : null;

        const detailedDesc = initiative && initiative.descDetailed ? (
            <div className="short-description">
                <h4>Szczegółowy opis</h4>
                <p>{initiative && initiative.descDetailed}</p>
            </div>
        ) : null;

        return (
            <Loader loaded={this.state.loaded}>
                <div>
                    <Jumbotron header="Szczegóły petycji"
                               hint="Szczegółowe informacje na temat petycji wniesionej przez obywateli." />
                    <Grid fluid className="initiatives-wrapper intiative-detail-wrapper">
                        <div className="white-pane">
                            <Grid fluid>
                                <Row>
                                    <Col xs={10} xsOffset={1}>
                                        <Link className="previous-page" to="/petycje">
                                            <i className="icon-arrow-pointer"></i>Wróć do listy
                                            petycji
                                        </Link>
                                    </Col>
                                </Row>
                            </Grid>
                        </div>
                        <Row>
                            <Col xs={12} md={6} mdOffset={1}
                                 className="initiatives-column initiatives-detail">
                                <img
                                    src={initiative && require('../../images/picker/300x240/' + initiative['300x240'])} />

                                <h2>{initiative && initiative.title}</h2>
                                <div className="short-description">
                                    <h4>Krótki opis</h4>
                                    <p>{initiative && initiative.descShort}</p>
                                </div>

                                {detailedDesc}

                                <div className="sign-initiative">
                                    <SignInitiative initiativeId={initiative && initiative._id}
                                                    userSigned={initiative && initiative.userSigned}
                                                    userSignedHandler={this.userSignedHandler.bind(this)} />
                                    <div className="counter">Ilość
                                        podpisów: {initiative && initiative.signsCtr}</div>
                                </div>
                                <div className="discussion">
                                    <Comments entityId={initiative && initiative._id} />
                                </div>
                            </Col>
                            <Col xs={12} md={3} mdOffset={1} className="initiatives-detail">
                                {article}
                            </Col>
                        </Row>
                    </Grid>
                </div>
            </Loader>
        );
    }
}