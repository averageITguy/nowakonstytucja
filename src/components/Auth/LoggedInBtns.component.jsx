import React from 'react';

import AuthService from '../../services/auth.service';

export default function LoggedInBtns(props) {
    const logOut = () => {
        AuthService.logOut();
    };

    return (
        <a className="rectangle-button log-out-button" onClick={logOut}>
        	<img src={`https://graph.facebook.com/${props.user.facebook.id}/picture?type=normal`} alt="user_avatar" />
            <div className="logout">Wyloguj się</div>
        </a>
    )
}
