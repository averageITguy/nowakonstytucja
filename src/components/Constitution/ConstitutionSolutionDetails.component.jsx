import React from 'react';
import {Link} from 'react-router';

import Jumbotron from '../Reusable/Jumbotron.component';
import {Row, Col, Grid} from 'react-flexbox-grid';

import Comments from '../Reusable/Comments/Comments.component';
import Loader from '../Reusable/Loader.component';

import ConstitutionService from '../../services/constitution.service';

import '../../styles/SolutionDetails.style.scss';

export default class ConstitutionSolutionDetails extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            solution: null,
            loaded: false
        };

        this.getSolution(props.params.solutionId, props.params.problemId);
    }

    getSolution(solutionId, problemId) {
        ConstitutionService.getSolution(solutionId, problemId).then(solution => {
            if(!this.unmounted) this.setState({
                solution,
                loaded: true
            });
        });
    }

    vote(value) {
        const solutionId = this.state.solution._id,
            problemId = this.props.params.problemId,
            voteObj = {value};

        ConstitutionService.voteForSolution(problemId, solutionId, voteObj).then(() => { //success - notify parent to update this item
            const solution = this.state.solution;

            solution.userVote = value;
            solution.votesCtr = solution.votesCtr + 1;

            if(value) solution.yesVotesCtr = solution.yesVotesCtr + 1;
            else solution.noVotesCtr = solution.noVotesCtr + 1;

            this.setState({solution});
        });
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    getVotePercent(voteValue) {
        if(!this.state.solution) return null;

        const solution = this.state.solution;

        if(solution.votesCtr === 0) return '0%';

        if(voteValue) return ((solution.yesVotesCtr / solution.votesCtr) * 100).toFixed(1) + '%';
        else return ((solution.noVotesCtr / solution.votesCtr) * 100).toFixed(1) + '%';
    }

    render() {
        const solution = this.state.solution;

        const userVote = solution && solution.userVote;
        const votingDisabled = typeof userVote === 'boolean';

        const yesVotesPerc = this.getVotePercent(true);
        const noVotesPerc = this.getVotePercent(false);

        const detailedDesc = solution && solution.descDetailed ? (
            <div className="short-description">
                <h4>Szczegółowy opis</h4>
                <p>{solution && solution.descDetailed}</p>
            </div>
        ) : null;

        return (
            <Loader loaded={this.state.loaded}>
                <div>
                    <Jumbotron header="Szczegóły rozwiązania"
                               hint="Dowiedz się szczegółów na temat rozwiązania konkretnego problemu." />
                    <Grid fluid className="solution-detail-wrapper">
                        <Row>
                            <Col xs={12} md={7} mdOffset={1} xsOffset={1}
                                 className="solution-detail-column solution-detail">
                                <Link className="previous-page"
                                      to={'/problem/' + this.props.params.problemId}>
                                    <i className="icon-arrow-pointer"></i>Wróć do strony problemu
                                </Link>
                                <div className="short-description">
                                    <h4>Krótki opis</h4>
                                    {solution && solution.descShort}
                                </div>

                                {detailedDesc}

                                <div className="vote-options">
                                    <button onClick={() => this.vote(false)}
                                            disabled={votingDisabled}
                                            className={userVote ? 'vote-btn--selected deny-button rectangle-button' : 'rectangle-button deny-button'}>
                                        {noVotesPerc}
                                        NIE {userVote == false ? '*' : ''}
                                    </button>
                                    <button onClick={() => this.vote(true)}
                                            disabled={votingDisabled}
                                            className={userVote ? 'vote-btn--selected accept-button rectangle-button' : 'rectangle-button accept-button'}>
                                        {yesVotesPerc}
                                        TAK {userVote ? '*' : ''}
                                    </button>
                                </div>
                                <Comments entityId={solution && solution._id} />
                            </Col>
                        </Row>
                    </Grid>
                </div>
            </Loader>
        );
    }
}
