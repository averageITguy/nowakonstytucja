import React from 'react';

import {Row, Col, Grid} from 'react-flexbox-grid';

import ConstitutionProposalService from '../../services/constitutionProposals.service';
import Toast from '../../services/toast.service';
import global from '../../services/global.service';

import '../../styles/Constitution.style.scss';

export default class ConstitutionSolutionNew extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            descShortCount: 0,
            descDetailedCount: 0
        };
    }

    handleSubmit(event) {
        event.preventDefault();

        global.markFormAsSubmitted(this.form);

        const formValid = this.form.checkValidity();
        if(formValid) {
            this.createSolution(this.form);
        }
    }

    createSolution(form) {
        const data = global.formDataToJSON(form);

        data.problemId = this.props.params.problemId;
        ConstitutionProposalService.proposeSolution(data)
            .then(() => {
                Toast.display('Zgłosiłeś propozycję!',
                    'Stanie się ona dostępna dla innych użytkowników po zatwierdzeniu przez moderatora');
                this.props.router.push('/problem/' + this.props.params.problemId);
            });
    }

    countCharacters(inputId) {
        const element = document.getElementById(inputId),
            count = element.value.length;

        const statePropertyName = `${inputId}Count`;

        this.setState({[statePropertyName]: count});
    }

    componentDidMount() {
        this.form = document.getElementById('initiativeForm');

        global.disableDefaultBrowserValidation(this.form);
    }

    render() {
        return (
            <Grid fluid className="initiatives-wrapper add-solution-wrapper">
                <Row>
                    <Col xs={12} md={7} mdOffset={1}>
                        <header className="header">
                            <h3>Formularz dodawania rozwiązania</h3>
                            <div>Aby uniknąć powtórzeń, zanim dodasz rozwiązanie przejrzyj listę
                                rozwiązań, które już zostały dodane!.
                            </div>
                        </header>
                        <form id="initiativeForm" className="initiative-form">
                            <fieldset className="control">
                                <label htmlFor="descShort" className="control__label">
                                    Krótki opis rozwiązania<div className="form-hint">(wymagane)</div>
                                </label>
                                <textarea onChange={() => this.countCharacters('descShort')}
                                          cols="30" rows="3" placeholder="Wpisz krótki opis..."
                                          className="control__input" id="descShort" name="descShort"
                                          required maxLength="200"></textarea>
                                <span
                                    className="characters-counter">{this.state.descShortCount}/200</span>
                            </fieldset>
                            <fieldset className="control">
                                <label htmlFor="descDetailed" className="control__label">
                                    Szczegółowy opis rozwiązania<div className="form-hint optional">(opcjonalne)</div>
                                </label>
                                <textarea onChange={() => this.countCharacters('descDetailed')}
                                          cols="30" rows="8" placeholder="Szczegóły..."
                                          className="control__input" id="descDetailed"
                                          name="descDetailed" maxLength="620"></textarea>
                                <span className="characters-counter">{this.state.descDetailedCount}/620</span>
                            </fieldset>
                            <div className="submit-button rectangle-button"
                                 style={{'margin-top': '15px'}}
                                 onClick={this.handleSubmit.bind(this)}>
                                Wyślij
                            </div>
                        </form>
                    </Col>
                </Row>
            </Grid>
        );
    }
}
