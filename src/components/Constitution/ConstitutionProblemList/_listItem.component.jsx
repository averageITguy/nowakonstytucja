import React from 'react';
import {Link} from 'react-router';

import ConstitutionService from '../../../services/constitution.service';

export default function ConstitutionProblemListItem(props) {
    const {problem, userVotedHandler} = props;

    const sign = () => {
        ConstitutionService.signProblem(problem._id)
            .then(userAlreadyVoted => {
                userVotedHandler(userAlreadyVoted, problem._id);
            });
    };

    const generateSignElement = ()=> {
        if(!problem.userSigned) {
            return (
                <button className="sign-button rectangle-button" onClick={sign}>
                    Poważny problem!
                </button>
            );
        } else {
            return <button className="sign-button disabled rectangle-button" disabled="disabled"
                           onClick={sign}>Uznano problem jako ważny.</button>;
        }
    };

    const signMsg = ConstitutionService.getSignsCounterMsg(problem.signsCtr, problem.userSigned);
    return (
        <div className="problem-items-wrapper">
            <div className="problem-item clearfix">
                <div className="problem-item-content">
                    <Link className="plain-style-link" to={'/problem/' + problem._id}>
                        <h2>{problem.descShort}</h2>
                    </Link>
                    <div className="author">Autor: anonim</div>
                    <Link to={'/problem/' + problem._id} className="simple-anchor">
                        Zobacz szczegółowy opis
                    </Link>
                    <div className="suggest-solution-from-list">
                        <Link to={'/rozwiazania/nowe/' + problem._id}>
                            <div className="sign-initiative important-problem-button-wrapper">
                                <button className="sign-button rectangle-button">
                                    Zaproponuj rozwiązanie
                                </button>
                            </div>
                        </Link>
                    </div>
                    <div className="sign-initiative important-problem-button-wrapper">
                        {generateSignElement()}

                        <div className="counter">
                            <div className="important-problem-tip">
                                <p>
                                    {signMsg}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
