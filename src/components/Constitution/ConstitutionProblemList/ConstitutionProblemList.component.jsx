require('styles/ProblemList.style.scss');
import React from 'react';
import {Link} from 'react-router';

import _ from 'lodash';

import InfiniteScroll from 'react-infinite-scroller';
import Filters from '../../Reusable/Filters.component';
import Jumbotron from '../../Reusable/Jumbotron.component';
import {Row, Col, Grid} from 'react-flexbox-grid';
import Loader from '../../Reusable/Loader.component';

import ConstitutionService from '../../../services/constitution.service';

import ConstitutionProblemListItem from './_listItem.component';

const image  = require('../../../images/loader.svg');

export default class ConstitutionProblemList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            problems: null,
            activeTab: 'popular',
            filters: {
                category: null,
                phrase: null
            },
            categories: null,
            currPage: 0,
            loaded: false
        };

        this.moreItemsToPaginate = true;
        this.searchTimer = 0;

        this.getProblems();
        this.getCategories();
    }

    getProblems() {
        let page = this.state.currPage + 1;

        this.getProblemsPromise = ConstitutionService.getProblems(
            this.state.activeTab, {filters: this.state.filters}, page
        ).then(problems => {
            if(this.unmounted) return;

            if(page === 1) {//reset list
                return this.setState({
                    problems,
                    currPage: page,
                    loaded: true
                });
            }

            this.setState({
                problems: _.uniqBy([...this.state.problems, ...problems], '_id'),
                currPage: page,
                loaded: true
            });

            this.moreItemsToPaginate = problems.length === 4; //hardcoded - items per page
        });
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    getCategories() {
        ConstitutionService.getCategories().then(categories => {
            this.setState({categories});
        });
    }

    generateList() {
        if(!this.state.problems) return null;

        if(this.state.problems.length === 0) return (
            <div className="problem-items-wrapper no-results">
                Brak wyników...
            </div>
        );

        return this.state.problems.map(problem => {
            return (
                <ConstitutionProblemListItem key={problem._id}
                                             userVotedHandler={this.userVotedHandler.bind(this)}
                                             problem={problem} />
            );
        });
    }

    generateCategories() {
        if(!this.state.categories) return null;

        return this.state.categories.map(category => {
            return <option key={category}
                           value={category}>
                {category}
            </option>
        });
    }

    userVotedHandler(userAlreadyVoted, problemId) {
        this.getProblemsPromise.then(()=> { //to make sure there is no pending request to server
            const problems = this.state.problems;

            const problem = problems.find(_problem => _problem._id === problemId);

            problem.userSigned = true;

            if(!userAlreadyVoted) {
                problem.signsCtr = problem.signsCtr + 1;
            }

            this.setState({problems});
        });
    }

    tabClicked(activeTab) {
        if(this.state.activeTab === activeTab) return;

        this.moreItemsToPaginate = true;
        this.setState({
            activeTab: activeTab,
            problems: null,
            currPage: 0 //reset
        }, ()=> this.getProblems());
    }

    categoryChanged(e) {
        const value = e.target.value;

        const filters = this.state.filters;
        filters.category = value;

        this.moreItemsToPaginate = true;

        this.setState({
            filters: filters,
            problems: null,
            currPage: 0 //reset
        }, () => this.getProblems());
    }

    searchChanged(e) {
        const value = e.target.value;

        const _search = ()=> {
            const filters = this.state.filters;
            filters.phrase = value;

            this.moreItemsToPaginate = true;

            this.setState({
                filters: filters,
                currPage: 0 //reset
            }, () => this.getProblems());
        };

        clearTimeout(this.searchTimer);
        this.searchTimer = setTimeout(_search, 1000); //1s delay
    }

    render() {
        const problems = this.generateList();

        return (
            <Loader loaded={this.state.loaded}>
                <div>
                    <Jumbotron header="Lista problemów"
                               hint="Przeglądaj problemy z jakimi borykają się rodacy. Proponuj i głosuj na rozwiązania!" />
                    <Grid fluid className="filters-wrapper">
                        <Filters activeTab={this.state.activeTab}
                                 tabClicked={this.tabClicked.bind(this)} />
                    </Grid>
                    <Grid fluid className="problem-list-wrapper-fluid">
                        <Row>
                            <Col xs={12} md={10} mdOffset={1} className="problem-list-panel">
                                <h1>WYBIERZ PROBLEM</h1>
                                <p>Wybierz problem z listy klikając na jego tytuł lub skorzystaj z
                                    wyszukiwarki. Możesz też wybrać kategorię.</p>
                                <div className="panel-item-wrapper category">
                                    <label>Kategoria</label>
                                    <select onChange={this.categoryChanged.bind(this)}>
                                        <option value="">Wybierz kategorię</option>
                                        {this.generateCategories()}
                                    </select>
                                </div>
                                <div className="panel-item-wrapper search">
                                    <label>Wyszukiwarka tekstowa</label>
                                    <input placeholder="Wpisz frazę..."
                                           onChange={this.searchChanged.bind(this)}></input>
                                </div>
                            </Col>
                        </Row>
                    </Grid>
                    <Grid fluid className="poll-list-wrapper problem-list-wrapper">
                        <Row>
                            <Col xs={12} md={10} mdOffset={1} className="problem-list-column">
                                <Link className="suggest-problem-button" to={'/problemy/nowy'}>
                                    <i className="icon icon-plus"></i>Dodaj problem
                                </Link>

                                <InfiniteScroll
                                    pageStart={0}
                                    loadMore={this.getProblems.bind(this)}
                                    hasMore={this.moreItemsToPaginate}
                                    loader={(<div id="loader-wrapper">
                                        <div className="table-row">
                                            <div id="table-cell-loader">
                                                <img src={image} />
                                            </div>
                                        </div>
                                    </div>)}>

                                    <div className="tracks">
                                        {problems}
                                    </div>
                                </InfiniteScroll>
                            </Col>
                        </Row>
                    </Grid>
                </div>
            </Loader>
        );
    }
}
