require('styles/Constitution.style.scss');

import React from 'react';
import {withRouter} from 'react-router';
import {Link} from 'react-router';

import ConstitutionService from '../../../services/constitution.service';

import Comments from '../../Reusable/Comments/Comments.component';
import Loader from '../../Reusable/Loader.component';

class ConstitutionSwiperComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currCategoryIdx: null,
            categories: null,
            currProblemIdx: null,
            problems: null,
            currSolutionIdx: null,
            categoryClasses: 'category shadow-border clearfix',
            problemClasses: 'problem shadow-border',
            solutionClasses: 'solution shadow-border',
            loaded: false
        };

        this.getCategories().then(selectedCategory => {
            this.getSwiperData(selectedCategory);
        });
    }

    getCategories() {
        return ConstitutionService.getCategories().then(categories => {
            this.setState({
                categories,
                currCategoryIdx: 0
            });

            return categories[0];
        });
    }

    getSwiperData(selectedCategory) {
        ConstitutionService.getSwiperData(selectedCategory).then(problems => {
            this.setState({
                problems,
                currProblemIdx: 0,
                currSolutionIdx: 0,
                loaded: true
            });
        });
    }

    nextCategory() {
        const categories = this.state.categories;
        let currCategoryIdx = ++this.state.currCategoryIdx;

        if(currCategoryIdx === categories.length) currCategoryIdx = 0;

        this.setState({
            currCategoryIdx
        });

        this.setState({
            categoryClasses: 'category shadow-border clearfix swiper-hide-animation disabled-actions',
            problemClasses: 'problem shadow-border swiper-hide-animation disabled-actions',
            solutionClasses: 'solution shadow-border swiper-hide-animation disabled-actions'
        });

        let that = this;

        setTimeout(function() {
            that.setState({
                categoryClasses: 'category shadow-border clearfix',
                problemClasses: 'problem shadow-border',
                solutionClasses: 'solution shadow-border'
            })
        }, 500);

        this.getSwiperData(this.state.categories[currCategoryIdx]);
    }

    prevCategory() {
        const categories = this.state.categories;
        let currCategoryIdx = --this.state.currCategoryIdx;

        if(currCategoryIdx === -1) currCategoryIdx = categories.length - 1;

        this.setState({
            currCategoryIdx
        });
        this.getSwiperData(this.state.categories[currCategoryIdx]);

        this.setState({
            categoryClasses: 'category shadow-border clearfix swiper-hide-animation disabled-actions',
            problemClasses: 'problem shadow-border swiper-hide-animation disabled-actions',
            solutionClasses: 'solution shadow-border swiper-hide-animation disabled-actions'
        });

        let that = this;

        setTimeout(function() {
            that.setState({
                categoryClasses: 'category shadow-border clearfix',
                problemClasses: 'problem shadow-border',
                solutionClasses: 'solution shadow-border'
            })
        }, 500);
    }

    nextProblem() {
        //TODO: nie ladowac wszystkich problemow na raz - np. za pierwszym razem zaladowac
        ///10, gdy gosc dojdzie do 5 doladowac 5 (15), gdy dojdzie do 10 znowu 5 (20)
        const problems = this.state.problems;
        let currProblemIdx = ++this.state.currProblemIdx;

        if(currProblemIdx === problems.length) currProblemIdx = 0;

        this.setState({
            currProblemIdx
        });

        this.setState({
            problemClasses: 'problem shadow-border swiper-hide-animation disabled-actions',
            solutionClasses: 'solution shadow-border swiper-hide-animation disabled-actions'
        });

        let that = this;

        setTimeout(function() {
            that.setState({
                problemClasses: 'problem shadow-border',
                solutionClasses: 'solution shadow-border'
            })
        }, 500);
    }

    prevProblem() {
        const problems = this.state.problems;
        let currProblemIdx = --this.state.currProblemIdx;

        if(currProblemIdx === -1) currProblemIdx = problems.length - 1;

        this.setState({
            currProblemIdx
        });

        this.setState({
            problemClasses: 'problem shadow-border swiper-hide-animation disabled-actions',
            solutionClasses: 'solution shadow-border swiper-hide-animation disabled-actions'
        });

        let that = this;

        setTimeout(function() {
            that.setState({
                problemClasses: 'problem shadow-border',
                solutionClasses: 'solution shadow-border'
            })
        }, 500);
    }

    nextSolution() {
        //TODO: nie wszystkie solution pobierac na raz
        this.setState(prevState => {
            return {
                currSolutionIdx: ++prevState.currSolutionIdx
            };
        });

        this.setState({
            solutionClasses: 'solution shadow-border swiper-hide-animation disabled-actions'
        });

        let that = this;

        setTimeout(function() {
            that.setState({
                solutionClasses: 'solution shadow-border'
            })
        }, 500);
    }

    voteSolution(value) {
        const currProblem = this.state.problems[this.state.currProblemIdx];
        const currSolution = currProblem.solutions[this.state.currSolutionIdx];
        const voteObj = {value};

        ConstitutionService.voteForSolution(currProblem._id, currSolution._id, voteObj);

        this.nextSolution();
    }

    generateProblemsSwiper() {
        if(!this.state.problems) return null;

        let classes = this.state.problemClasses;
        if(this.state.problems.length > 0) {
            const currProblem = this.state.problems[this.state.currProblemIdx];

            return (
                <div className={classes}>
                    <Link className="suggest-problem tooltip suggest-item-toast"
                          to={'/problemy/nowy'}>
                        <i className="icon icon-plus"></i><span>Dodaj problem</span>
                    </Link>

                    <Link className="plain-style-link" to={'/problem/' + currProblem._id}>
                        <h3>OPIS PROBLEMU</h3>
                    </Link>
                    <Link className="plain-style-link" to={'/problem/' + currProblem._id}>
                        <p>{currProblem.descShort}</p>
                    </Link>

                    <div className="switcher">
                        <div className="content clearfix">
                                    <span onClick={this.prevProblem.bind(this)}
                                          className="arrow-wrapper">
                                        <i className="icon icon-arrow-left"></i>POPRZEDNI
                                    </span>
                            <span onClick={this.nextProblem.bind(this)}
                                  className="arrow-wrapper">
                                        NASTĘPNY<i className="icon icon-arrow-right"></i>
                                    </span>
                        </div>
                    </div>
                </div>
            );
        }
        else {
            return (
                <div className={classes}>
                    <Link className="suggest-problem tooltip suggest-item-toast"
                          to={'/problemy/nowy'}>
                        <i className="icon icon-plus"></i><span>Dodaj problem</span>
                    </Link>

                    <p>
                        Wygląda na to, że oceniłeś każde rozwiązanie dla wszystkich zgłoszonych w
                        tej
                        kategorii problemów lub chwilowo rozwiązania są niedostępne.
                    </p>
                    <p>
                        Zgłoś nowy problem w tej kategorii lub przejdź do innej. Razem możemy
                        wiele dokonać!
                    </p>
                </div>
            );
        }
    }

    generateSolutionsSwiper() {
        if(!this.state.problems || this.state.problems.length === 0) return null;

        let classes = this.state.solutionClasses;
        const currProblem = this.state.problems[this.state.currProblemIdx];
        const currSolution = currProblem.solutions[this.state.currSolutionIdx];

        if(currSolution) {
            return (
                <div>
                    <div className={classes}>
                        <Link className="suggest-solution tooltip suggest-item-toast"
                              to={'/rozwiazania/nowe/' + currProblem._id}>
                            <i className="icon icon-plus"></i><span>Zaproponuj rozwiązanie</span>
                        </Link>

                        <Link className="plain-style-link"
                              to={'/problem/' + currProblem._id + '/rozwiazanie/' + currSolution._id}>
                            <h3>ROZWIĄZANIE PROBLEMU</h3>
                        </Link>
                        <Link className="plain-style-link"
                              to={'/problem/' + currProblem._id + '/rozwiazanie/' + currSolution._id}>
                            <p>{currSolution.descShort}</p>
                        </Link>
                    </div>
                    <div className="vote-options">
                        <div onClick={() => this.voteSolution(false)}
                             className="deny-button"></div>
                        <div onClick={() => this.voteSolution(true)}
                             className="accept-button"></div>
                    </div>
                    <div className="discussion-component-wrapper">
                        <Comments entityId={currSolution._id} single={true} />
                    </div>
                </div>
            );
        } else if(this.state.currSolutionIdx === currProblem.solutions.length && currProblem.solutions.length !== 0) {
            return (
                <div>
                    <div className={classes}>
                        <Link className="suggest-solution tooltip suggest-item-toast"
                              to={'/rozwiazania/nowe/' + currProblem._id}>
                            <i className="icon icon-plus"></i><span>Zaproponuj rozwiązanie</span>
                        </Link>

                        <h3>ROZWIĄZANIE PROBLEMU</h3>
                        <p>
                            Oceniłeś już wszystkie dodane rozwiązania. Zawsze możesz zaproponować
                            nowe lub pomóc nam rozwiązać kolejny problem !
                        </p>
                        <Link to={'/problem/' + currProblem._id}>
                            Tutaj znajdziesz listę rozwiązań dla tego problemu
                        </Link>
                    </div>
                </div>
            );
        } else {
            return (
                <div>
                    <div className="solution shadow-border">
                        <Link className="suggest-solution tooltip suggest-item-toast"
                              to={'/rozwiazania/nowe/' + currProblem._id}>
                            <i className="icon icon-plus"></i><span>Zaproponuj rozwiązanie</span>
                        </Link>

                        <h3>ROZWIĄZANIE PROBLEMU</h3>
                        <p>Zaproponuj własne rozwiązanie!</p>
                    </div>
                </div>
            );
        }
    }

    render() {
        const currCategory = this.state.categories && this.state.categories[this.state.currCategoryIdx];
        let classes = this.state.categoryClasses;

        return (
            <Loader loaded={this.state.loaded}>
                <div className="container-fluid constitution-swiper-wrapper">
                    <div className="row">
                        <div className="constitution-swiper col-xs-12 col-md-10 col-md-offset-1">
                            <h1>PISZ KONSTYTUCJĘ!</h1>
                            <div className={classes}>
                                <h3>{currCategory}</h3>
                                <div className="switcher">
                                    <i onClick={this.prevCategory.bind(this)}
                                       className="icon icon-arrow-left"></i>
                                    <i onClick={this.nextCategory.bind(this)}
                                       className="icon icon-arrow-right"></i>
                                </div>
                            </div>

                            {this.generateProblemsSwiper()}

                            {this.generateSolutionsSwiper()}
                        </div>
                    </div>
                </div>
            </Loader>
        );
    }
}

export default withRouter(ConstitutionSwiperComponent);
