require('styles/ProblemDetail.style.scss');

import _ from 'lodash';
import React from 'react';
import {Link} from 'react-router';

import Jumbotron from '../../Reusable/Jumbotron.component';

import {Row, Col, Grid} from 'react-flexbox-grid';
import Loader from '../../Reusable/Loader.component';
import Comments from '../../Reusable/Comments/Comments.component';

import ConstitutionService from '../../../services/constitution.service';

import SolutionsList from './_solutionsList';

export default class ConstitutionProblemDetails extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            problem: null,
            loaded: false,
            solutionsListPage: 1,
            hasMoreSolutions: true
        };

        this.getProblem(props.params.id);

        this.sign = this.sign.bind(this);
    }

    getProblem(id) {
        ConstitutionService.getProblem(id).then(problem => {
            if(!this.unmounted) this.setState({
                problem,
                loaded: true,
                solutionsListPage: 1,
                hasMoreSolutions: true
            });
        });
    }

    getNextSolutions() {
        this.setState(state => {
            return {
                solutionsListPage: state.solutionsListPage + 1
            }
        }, ()=> {
            ConstitutionService.getSolutionsForProblem(this.props.params.id, this.state.solutionsListPage)
                .then(solutions => {
                    if(solutions && solutions.length > 0) {
                        const problem = this.state.problem;
                        problem.solutions = _.uniqBy([...problem.solutions, ...solutions], '_id');

                        if(!this.unmounted) {
                            this.setState({
                                problem
                            });
                        }
                    } else {
                        if(!this.unmounted) {
                            this.setState({
                                hasMoreSolutions: false
                            });
                        }
                    }
                });
        });
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    userVotedHandler(solutionId, userVote) {
        const solution = this.state.problem.solutions.find(solution => solution._id === solutionId);

        solution.userVote = userVote;
        solution.votesCtr = solution.votesCtr + 1;

        if(userVote) solution.yesVotesCtr = solution.yesVotesCtr + 1;
        else solution.noVotesCtr = solution.noVotesCtr + 1;

        this.setState({problem: this.state.problem});
    }

    sign() {
        ConstitutionService.signProblem(this.state.problem._id)
            .then(() => {
                const problem = this.state.problem;

                problem.userSigned = true;
                problem.signsCtr = problem.signsCtr + 1;

                this.setState({problem});
            });
    }

    generateSignElement() {
        if(!this.state.problem) return null;

        if(!this.state.problem.userSigned) {
            return (
                <button className="sign-button rectangle-button" onClick={this.sign}>
                    Poważny problem!
                </button>
            );
        } else {
            return <button className="sign-button disabled rectangle-button" disabled="disabled"
                           onClick={this.sign}>Uznano problem jako ważny.</button>;
        }
    }

    render() {
        const problem = this.state.problem;

        const signMsg = problem ? ConstitutionService.getSignsCounterMsg(
            problem.signsCtr, problem.userSigned) : null;

        const article = problem && problem.article ? (
            <a className="article"
               href={"http://wiadomosci.nowakonstytucja.org/" + problem.article.url}
               target="_blank">
                <span className="article-badge">Naszym zdaniem</span>
                <span
                    className="article-title">{problem.article.title}</span>
            </a>
        ) : null;

        const detailedDesc = problem && problem.descDetailed ? (
            <div className="short-description long-description">
                <h4>Szczegółowy opis</h4>
                <p>{problem && problem.descDetailed}</p>
            </div>
        ) : null;

        let getMoreSolutions = null;

        if(this.state.hasMoreSolutions && problem && problem.solutions &&
            problem.solutions.length >= 5) {

            getMoreSolutions = (
                <a onClick={this.getNextSolutions.bind(this)}
                   className="get-solutions-btn">
                    Pobierz więcej
                </a>
            );
        }

        return (
            <Loader loaded={this.state.loaded}>
                <div>
                    <Jumbotron header="Szczegóły problemu"
                               hint="Dowiedz się szczegółów na temat, który Cię zaintrygował." />
                    <Grid fluid className="problem-detail-wrapper">
                        <div className="white-pane">
                            <Grid fluid>
                                <Row>
                                    <Col xs={10} xsOffset={1}>
                                        <Link className="previous-page" to="/problemy">
                                            <i className="icon-arrow-pointer"></i>
                                            Wróć do listy problemów
                                        </Link>
                                    </Col>
                                </Row>
                            </Grid>
                        </div>
                        <Row>
                            <Col xs={12} md={6} mdOffset={1}
                                 className="problem-detail-column problem-detail">
                                <h2>Problem:</h2>

                                <Link className="suggest-solution-button" to={'/rozwiazania/nowe/' + (problem && problem._id)}>
                                    <i className="icon icon-plus"></i>Dodaj rozwiązanie
                                </Link>

                                <div className="category">
                                    Kategoria: {problem && problem.category}
                                </div>
                                <div className="short-description">
                                    <h4>Tytuł problemu:</h4>
                                    {problem && problem.descShort}
                                </div>

                                {detailedDesc}

                                <div className="clearfix problem-item-content">
                                    <div
                                        className="sign-initiative important-problem-button-wrapper">
                                        {this.generateSignElement()}
                                        <div className="counter">
                                            <div className="important-problem-tip">
                                                <p>
                                                    {signMsg}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <Comments entityId={problem && problem._id} />
                            </Col>
                            <Col xs={12} md={4} className="solutions-infinity-container">
                                {article}

                                {problem && problem.solutions && problem.solutions.length > 0 ?
                                    <h2>Proponowane rozwiązania:</h2> : null}

                                <SolutionsList solutions={problem && problem.solutions}
                                               problemId={problem && problem._id}
                                               userVotedHandler={this.userVotedHandler.bind(this)} />

                                {getMoreSolutions}
                            </Col>
                        </Row>
                    </Grid>
                </div>
            </Loader>
        );
    }
}
