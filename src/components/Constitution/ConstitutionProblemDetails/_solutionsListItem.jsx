import React from 'react';
import {Link} from 'react-router';

import ConstitutionService from '../../../services/constitution.service';

export default class SolutionsListItem extends React.Component {
    vote(value) {
        const solutionId = this.props.solution._id,
            problemId = this.props.problemId,
            voteObj = {value};

        ConstitutionService.voteForSolution(problemId, solutionId, voteObj)
            .then(() => { //success - notify parent to update this item
                this.props.userVotedHandler(solutionId, value);
            });
    }

    getVotePercent(voteValue) {
        const solution = this.props.solution;

        if(solution.votesCtr === 0) return '0%';

        if(voteValue) return ((solution.yesVotesCtr / solution.votesCtr) * 100).toFixed(1) + '%';
        else return ((solution.noVotesCtr / solution.votesCtr) * 100).toFixed(1) + '%';
    }

    render() {
        const solution = this.props.solution;

        const userVote = solution.userVote;
        const votingDisabled = typeof userVote === 'boolean';

        const yesVotesPerc = this.getVotePercent(true);
        const noVotesPerc = this.getVotePercent(false);

        return (
            <div className="law-items-wrapper">
                <div className="law-item clearfix">
                    <div className="law-item-content solution-item">
                        <div className="description">
                            {solution.descShort}
                        </div>
                        <Link className="simple-anchor" to={'/problem/' + this.props.problemId + '/rozwiazanie/' + solution._id}>
                            Przejdź do rozwiązania
                        </Link>
                        <div className="vote-options">
                            <button onClick={() => this.vote(false)}
                                    disabled={votingDisabled}
                                    className={userVote ? 'vote-btn--selected deny-button rectangle-button' : 'rectangle-button deny-button'}>
                                {noVotesPerc} NIE {userVote == false ? '*' : ''}
                            </button>
                            <button onClick={() => this.vote(true)}
                                    disabled={votingDisabled}
                                    className={userVote ? 'vote-btn--selected accept-button rectangle-button' : 'rectangle-button accept-button'}>
                                {yesVotesPerc} TAK {userVote ? '*' : ''}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}
