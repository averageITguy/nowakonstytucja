import React from 'react';

import SolutionsListItem from './_solutionsListItem';

export default function SolutionsList(props) {
    const {solutions, userVotedHandler, problemId} = props;

    const generateList = () => {
        if(!solutions) return null;
        
        return solutions.map(solution => {
            return <SolutionsListItem key={solution._id}
                                      solution={solution}
                                      problemId={problemId}
                                      userVotedHandler={userVotedHandler} />
        });
    };

    return (
        <div>
            {generateList()}
        </div>
    );
}