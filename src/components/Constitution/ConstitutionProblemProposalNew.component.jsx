import React from 'react';

import {Row, Col, Grid} from 'react-flexbox-grid';

import ConstitutionService from '../../services/constitution.service';
import ConstitutionProposalService from '../../services/constitutionProposals.service';
import Toast from '../../services/toast.service';
import global from '../../services/global.service';

import '../../styles/Constitution.style.scss';

export default class ConstitutionProblemNew extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            descShortCount: 0,
            descDetailedCount: 0,
            categories: []
        };

        this.getCategories();
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();

        global.markFormAsSubmitted(this.form);

        const formValid = this.form.checkValidity();
        if(formValid) {
            this.createProblem(this.form);
        }
    }

    getCategories() {
        ConstitutionService.getCategories().then(categories => {
            this.setState({categories});
        });
    }

    createProblem(form) {
        const data = global.formDataToJSON(form);

        ConstitutionProposalService.proposeProblem(data)
            .then(() => {
                Toast.display('Zgłosiłeś propozycję!',
                    'Stanie się ona dostępna dla innych użytkowników po zatwierdzeniu przez moderatora');
                this.props.router.push('/');
            });
    }

    countCharacters(inputId) {
        const element = document.getElementById(inputId),
            count = element.value.length;

        const statePropertyName = `${inputId}Count`;

        this.setState({[statePropertyName]: count});
    }

    componentDidMount() {
        this.form = document.getElementById('initiativeForm');

        global.disableDefaultBrowserValidation(this.form);
    }

    generateCategories() {
        return this.state.categories.map(category => {
            return (
                <option key={category}
                        value={category}>{category}</option>
            );
        });
    }

    render() {
        return (
            <Grid fluid className="initiatives-wrapper">
                <Row>
                    <Col xs={12} md={10} mdOffset={1} className="add-problem-wrapper">

                        <header className="header">
                            <h3>Formularz dodawania problemu</h3>
                            <div>Dodaj problem, który uważasz za ciekawy i spróbujmy wspólnie
                                znaleźć rozwiązanie.
                            </div>
                        </header>

                        <form id="initiativeForm" className="initiative-form">

                            <fieldset className="control">
                                <label htmlFor="descShort" className="control__label">
                                    Opis problemu <div className="form-hint">(wymagany)</div>
                                </label>
                                <textarea onChange={() => this.countCharacters('descShort')}
                                          cols="30" rows="3" placeholder="Opis problemu..."
                                          className="control__input" id="descShort" name="descShort"
                                          required maxLength="200"></textarea>
                                <span
                                    className="characters-counter">{this.state.descShortCount}/200</span>
                            </fieldset>

                            <fieldset className="control">
                                <label htmlFor="descDetailed" className="control__label">
                                    Szczegółowe informacje<div className="form-hint optional">(opcjonalne)</div>
                                </label>
                                <textarea onChange={() => this.countCharacters('descDetailed')}
                                          cols="30" rows="8" placeholder="Szczegóły..."
                                          className="control__input" id="descDetailed"
                                          name="descDetailed" maxLength="620"></textarea>
                                <span className="characters-counter">{this.state.descDetailedCount}/620</span>
                            </fieldset>

                            <fieldset className="control">
                                <label htmlFor="descShort" className="control__label">
                                    Przypisz do kategorii<div className="form-hint">(wymagane)</div>
                                </label>
                                <select name="category" className="control__input" required>
                                    <option value="">Wybierz</option>
                                    {this.generateCategories()}
                                </select>
                            </fieldset>

                            <div className="submit-button rectangle-button"
                                 onClick={this.handleSubmit}>Wyślij
                            </div>
                        </form>
                    </Col>
                </Row>
            </Grid>
        );
    }
}
