import React from 'react';
import {Link} from 'react-router';

import _ from 'lodash';

import InfiniteScroll from 'react-infinite-scroller';
import Filters from '../../Reusable/Filters.component';
import Jumbotron from '../../Reusable/Jumbotron.component';
import {Row, Col, Grid} from 'react-flexbox-grid';
import Loader from '../../Reusable/Loader.component';

import ParliamentService from '../../../services/parliament.service';

import ParliamentListItem from './_listItem.component';

import '../../../styles/Parliament.style.scss';
const image  = require('../../../images/loader.svg');

export default class ParliamentList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            bills: null,
            activeTab: 'popular',
            currPage: 0,
            loaded: false
        };

        this.moreItemsToPaginate = true;

        this.getBills();
    }

    getBills() {
        let page = this.state.currPage + 1;

        this.getBillsPromise = ParliamentService.getBills(this.state.activeTab, page)
            .then(bills => {
                if(this.unmounted) return;

                if(page === 1) {//reset list
                    return this.setState({
                        bills,
                        currPage: page,
                        loaded: true
                    });
                }

                this.setState({
                    bills: _.uniqBy([...this.state.bills, ...bills], '_id'),
                    currPage: page,
                    loaded: true
                });

                this.moreItemsToPaginate = bills.length === 4; //hardcoded - items per page
            });
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    generateList() {
        if(!this.state.bills) return null;

        return this.state.bills.map(bill => {
            return (
                <ParliamentListItem key={bill._id}
                                    userVotedHandler={this.userVotedHandler.bind(this)}
                                    bill={bill} />
            );
        });
    }

    userVotedHandler(updatedBill) {
        this.getBillsPromise.then(()=> { //to make sure there is no pending request to server
            const bills = this.state.bills;

            const bill = bills.find(_bill => _bill._id === updatedBill._id);

            Object.assign(bill, updatedBill);

            this.setState({bills});
        });
    }

    tabClicked(activeTab) {
        if(this.state.activeTab === activeTab) return;

        this.moreItemsToPaginate = true;
        this.setState({
            activeTab: activeTab,
            currPage: 0, //reset
            bills: null
        }, ()=> this.getBills());
    }

    render() {
        const list = this.generateList();

        return (
            <Loader loaded={this.state.loaded}>
                <div>
                    <Jumbotron header="Lista ustaw"
                               hint="Aktualne ustawy wprowadzane w życie przez nasz rząd." />
                    <Grid fluid className="filters-wrapper">
                        <Filters activeTab={this.state.activeTab}
                                 tabClicked={this.tabClicked.bind(this)} />
                    </Grid>
                    <Grid fluid className="parliament-wrapper">
                        <Row>
                            <Col xs={12} md={10} mdOffset={1} className="parliament-column">
                                <Link className="suggest-parliament-button" to={'/ustawy/nowa'}>
                                    <i className="icon icon-plus"></i>Zaproponuj ustawę
                                </Link>

                                {/*<div className="hot-topic">*/}
                                {/*<div className="title">*/}
                                {/*<div className="label">Gorący temat</div>*/}
                                {/*Tutaj znajduje się oficjalny tytuł ustawy*/}
                                {/*</div>*/}
                                {/*</div>*/}

                                <InfiniteScroll
                                    pageStart={0}
                                    loadMore={this.getBills.bind(this)}
                                    hasMore={this.moreItemsToPaginate}
                                    loader={(<div id="loader-wrapper">
                                        <div className="table-row">
                                            <div id="table-cell-loader">
                                                <img src={image} />
                                            </div>
                                        </div>
                                    </div>)}>

                                    <div className="tracks">
                                        {list}
                                    </div>
                                </InfiniteScroll>

                            </Col>
                        </Row>
                    </Grid>
                </div>
            </Loader>
        );
    }
}
