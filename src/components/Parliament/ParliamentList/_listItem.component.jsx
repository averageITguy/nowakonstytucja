import React from 'react';
import {Link} from 'react-router';

import ParliamentService from '../../../services/parliament.service';

import '../../../styles/Parliament.style.scss';

export default class ParliamentListItem extends React.Component {
    constructor(props) {
        super(props);

        this.vote = this.vote.bind(this);
    }

    vote(value) {
        const billId = this.props.bill._id,
            voteObj = {value};

        ParliamentService.vote(billId, voteObj)
            .then(() => { //success - notify parent (list) to update this item
                const bill = this.props.bill;

                bill.userVote = value;
                bill.votesCtr = bill.votesCtr + 1;

                if(value) bill.yesVotesCtr = bill.yesVotesCtr + 1;
                else bill.noVotesCtr = bill.noVotesCtr + 1;

                this.props.userVotedHandler(bill);
            });
    }

    getVotePercent(voteValue) {
        const bill = this.props.bill;

        if(bill.votesCtr === 0) return '0%';

        if(voteValue) return ((bill.yesVotesCtr / bill.votesCtr) * 100).toFixed(1) + '%';
        else return ((bill.noVotesCtr / bill.votesCtr) * 100).toFixed(1) + '%';
    }

    render() {
        const bill = this.props.bill;
        const userVote = bill.userVote;
        const votingDisabled = typeof userVote === 'boolean';

        const yesVotesPerc = this.getVotePercent(true);
        const noVotesPerc = this.getVotePercent(false);

        return (
            <div className="law-items-wrapper">
                <div className="law-item clearfix">
                    <div className="avatar-wrapper">
                        <Link className="plain-style-link" to={'/ustawa/' + bill._id}>
                            <img
                                src={require('../../../images/picker/300x240/' + bill['200x150'])} />
                        </Link>
                    </div>
                    <div className="law-item-content">
                        <Link className="plain-style-link" to={'/ustawa/' + bill._id}>
                            <h2>{bill.title}</h2>
                        </Link>
                        <p>{bill.descDetailed}</p>
                        <a className={bill.pdfFileUrl ? '' : 'hidden'}
                           href={bill.pdfFileUrl} target="_blank">
                            <i className="icon icon-pdf"></i>Pobierz projekt ustawy
                        </a>
                        <div className="link-wrapper">
                            <Link to={'/ustawa/' + bill._id} className="simple-anchor">
                                Zobacz szczegółowy opis
                            </Link>
                        </div>

                        <p className="votes-counter">
                            Głosów: {this.props.bill.votesCtr}
                        </p>

                        <div className="vote-options">
                            <button onClick={() => this.vote(false)}
                                    disabled={votingDisabled}
                                    className={userVote ? 'vote-btn--selected deny-button rectangle-button' : 'rectangle-button deny-button'}>
                                {noVotesPerc} NIE {userVote == false ? '*' : ''}
                            </button>
                            <button onClick={() => this.vote(true)}
                                    disabled={votingDisabled}
                                    className={userVote == false ? 'vote-btn--selected accept-button rectangle-button' : 'rectangle-button accept-button'}>
                                {yesVotesPerc} TAK {userVote ? '*' : ''}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
