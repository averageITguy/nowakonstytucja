//TODO: 1) numer projektu tylko number (4 cyfry)

import React from 'react';

import {Row, Col, Grid} from 'react-flexbox-grid';

import ParliamentProposalService from '../../services/parliamentProposals.service';
import Toast from '../../services/toast.service';
import global from '../../services/global.service';

//TODO: własne style (?? wspólne)
import '../../styles/Initiatives.style.scss'

export default class ParliamentNew extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            descDetailedCount: 0
        };
    }

    handleSubmit(event) {
        event.preventDefault();

        global.markFormAsSubmitted(this.form);

        const formValid = this.form.checkValidity();
        if(formValid) {
            this.createBill(this.form);
        }
    }

    createBill(form) {
        const data = new FormData(form);

        ParliamentProposalService.proposeBill(data)
            .then(() => {
                Toast.display('Zgłosiłeś propozycję!',
                    'Stanie się ona dostępna dla innych użytkowników po zatwierdzeniu przez moderatora');
                this.props.router.push('/');
            });
    }

    componentDidMount() {
        this.form = document.getElementById('initiativeForm');

        global.disableDefaultBrowserValidation(this.form);
    }

    countCharacters(inputId) {
        const element = document.getElementById(inputId),
            count = element.value.length;

        const statePropertyName = `${inputId}Count`;

        this.setState({[statePropertyName]: count});
    }

    render() {
        return (
            <Grid fluid className="initiatives-wrapper">
                <Row>
                    <Col xs={12} md={10} mdOffset={1} className="add-new-parliament">

                        <header className="header">
                            <h3>Formularz dodawania projektu ustawy</h3>
                            <div>Zgłoś projekt ustawy procedowanej aktualnie w sejmie</div>
                        </header>

                        <form id="initiativeForm" className="initiative-form">

                            <fieldset className="control">
                                <label htmlFor="title" className="control__label">Tytuł
                                    oficjalny<div className="form-hint">(wymagany)</div></label>
                                <input type="text" placeholder="Wpisz tytuł..."
                                       className="control__input" maxLength="350"
                                       id="title" name="title" required />
                            </fieldset>

                            <fieldset className="control">
                                <label htmlFor="printNb" className="control__label">
                                    Numer druku<div className="form-hint">(wymagany)</div>
                                </label>
                                <input type="number" placeholder="Np. 1442"
                                       className="control__input"
                                       id="printNb" name="printNb" required />
                            </fieldset>

                            <fieldset className="control">
                                <label htmlFor="descDetailed" className="control__label">
                                    Komentarz<div className="form-hint optional">(opcjonalne)</div>
                                </label>
                                <textarea onChange={() => this.countCharacters('descDetailed')}
                                          cols="30" rows="8"
                                          placeholder="Postaraj się bezstronnie wytłumaczyć
                                            czego dotyczy ten projekt ustawy..."
                                          className="control__input" id="descDetailed"
                                          name="descDetailed" maxLength="620"></textarea>
                                <span className="characters-counter">{this.state.descDetailedCount}/620</span>
                            </fieldset>

                            <fieldset className="control file-upload">
                                <div>Dozwolony format pliku: <b>Projekt ustawy - PDF</b></div>
                                <div>Maksymalny rozmiar pliku: <b>50MB</b></div>
                                <input className="inputfile" id="pdfFile" type="file"
                                       value="+ Dodaj plik" name="pdfFile" />
                                <label htmlFor="pdfFile">Dodaj plik</label>
                                <div className="submit-button rectangle-button"
                                     onClick={this.handleSubmit.bind(this)}>
                                    Wyślij
                                </div>
                            </fieldset>

                        </form>
                    </Col>
                </Row>
            </Grid>
        );
    }
}
