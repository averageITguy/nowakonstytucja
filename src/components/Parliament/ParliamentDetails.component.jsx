import React from 'react';
import {Link} from 'react-router';

import {Row, Col, Grid} from 'react-flexbox-grid';

import ParliamentService from '../../services/parliament.service';

import Jumbotron from '../Reusable/Jumbotron.component';
import Loader from '../Reusable/Loader.component';
import Comments from '../Reusable/Comments/Comments.component';

import {
    Table,
    TableBody,
    TableRow,
    TableRowColumn
} from 'material-ui/Table';

import '../../styles/Parliament.style.scss';

export default class ParliamentDetails extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            bill: null,
            loaded: false
        };

        this.getBill(props.params.id);

        this.vote = this.vote.bind(this);
    }

    getBill(id) {
        ParliamentService.getBill(id).then(bill => {
            if(!this.unmounted) this.setState({bill, loaded: true});
        });
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    vote(value) {
        const billId = this.state.bill._id,
            voteObj = {value};

        ParliamentService.vote(billId, voteObj)
            .then(() => {
                const bill = this.state.bill;

                bill.userVote = value;
                bill.votesCtr = bill.votesCtr + 1;

                if(value) bill.yesVotesCtr = bill.yesVotesCtr + 1;
                else bill.noVotesCtr = bill.noVotesCtr + 1;

                this.setState({bill});
            });
    }

    generateStatistics() {
        if(!this.state.bill) return null;

        return (
            <div>
                <Table>
                    <TableBody displayRowCheckbox={false}>
                        <TableRow>
                            <TableRowColumn>Głosy za:</TableRowColumn>
                            <TableRowColumn>{this.state.bill.yesVotesCtr}</TableRowColumn>
                        </TableRow>
                        <TableRow>
                            <TableRowColumn>Głosy przeciw:</TableRowColumn>
                            <TableRowColumn>{this.state.bill.noVotesCtr}</TableRowColumn>
                        </TableRow>
                        <TableRow>
                            <TableRowColumn>Głosów ogółem: </TableRowColumn>
                            <TableRowColumn><b>{this.state.bill.votesCtr}</b></TableRowColumn>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }

    getVotePercent(voteValue) {
        if(!this.state.bill) return null;

        const bill = this.state.bill;

        if(bill.votesCtr === 0) return '0%';

        if(voteValue) return ((bill.yesVotesCtr / bill.votesCtr) * 100).toFixed(1) + '%';
        else return ((bill.noVotesCtr / bill.votesCtr) * 100).toFixed(1) + '%';
    }

    render() {
        const bill = this.state.bill;

        const userVote = bill && bill.userVote;
        const votingDisabled = typeof userVote === 'boolean';

        const yesVotesPerc = this.getVotePercent(true);
        const noVotesPerc = this.getVotePercent(false);

        const article = bill && bill.article ? (
            <a className="article"
               href={"http://wiadomosci.nowakonstytucja.org/" + bill.article.url}
               target="_blank">
                <span className="article-badge">Naszym zdaniem</span>
                <span
                    className="article-title">{bill.article.title}</span>
            </a>
        ) : null;

        const detailedDesc = bill && bill.descDetailed ? (
            <div className="short-description">
                <h4>Krótki opis</h4>
                <p>{bill && bill.descDetailed}</p>
            </div>
        ) : null;

        return (
            <Loader loaded={this.state.loaded}>
                <div>
                    <Jumbotron header="Szczegóły ustawy"
                               hint="Dokładne informacje na temat ustawy, nad którą pracują obecnie politycy." />
                    <Grid fluid
                          className="initiatives-wrapper parliament-wrapper parliament-details-wrapper">
                        <div className="white-pane">
                            <Grid fluid>
                                <Row>
                                    <Col xs={10} xsOffset={1}>
                                        <Link className="previous-page" to="/ustawy">
                                            <i className="icon-arrow-pointer"></i>Wróć do listy
                                            ustaw
                                        </Link>
                                    </Col>
                                </Row>
                            </Grid>
                        </div>
                        <Row>
                            <Col xs={12} md={6} mdOffset={1}
                                 className="initiatives-column initiatives-detail parliament-detail">
                                <h3>Szczegółowe informacje</h3>

                                <img
                                    src={bill && require('../../images/picker/300x240/' + bill['300x240'])} />
                                <h2>{bill && bill.title}</h2>

                                {detailedDesc}

                                <div className="link-wrapper">
                                    <a href={bill && bill.legislationUrl} target="_blank">
                                        Przebieg procesu legislacyjnego
                                    </a>
                                </div>
                                <div className="clearfix">
                                    <div className="pdf-download">
                                        <a className={bill && bill.pdfFileUrl ? '' : 'hidden'}
                                           href={bill && bill.pdfFileUrl} target="_blank">
                                            <i className="icon icon-pdf"></i>Pobierz projekt ustawy
                                        </a>
                                    </div>
                                    <div className="vote-options">
                                        <button onClick={() => this.vote(false)}
                                                disabled={votingDisabled}
                                                className={userVote ? 'vote-btn--selected deny-button rectangle-button' : 'rectangle-button deny-button'}>
                                            {noVotesPerc}
                                            NIE {userVote == false ? '*' : ''}
                                        </button>
                                        <button onClick={() => this.vote(true)}
                                                disabled={votingDisabled}
                                                className={userVote == false ? 'vote-btn--selected accept-button rectangle-button' : 'rectangle-button accept-button'}>
                                            {yesVotesPerc}
                                            TAK {userVote ? '*' : ''}
                                        </button>
                                    </div>
                                </div>
                                <Comments entityId={bill && bill._id} />
                            </Col>
                            <Col xs={12} md={3} mdOffset={1}
                                 className="parliament-statistics-wrapper">
                                {article}

                                <h3>Statystyki</h3>
                                {this.generateStatistics()}
                            </Col>
                        </Row>
                    </Grid>
                </div>
            </Loader>
        );
    }
}
