import React from 'react';
import {Link} from 'react-router';

import PollsService from '../../../services/polls.service';

import '../../../styles/Polls.style.scss';

export default class PollListItem extends React.Component {
    constructor(props) {
        super(props);

        this.vote = this.vote.bind(this);
    }

    vote(value) {
        const pollId = this.props.poll._id,
            voteObj = {value};

        PollsService.vote(pollId, voteObj)
            .then(() => { //success - notify parent (list) to update this item
                const poll = this.props.poll;

                poll.userVote = value;
                poll.votesCtr = poll.votesCtr + 1;

                if(value) poll.yesVotesCtr = poll.yesVotesCtr + 1;
                else poll.noVotesCtr = poll.noVotesCtr + 1;

                this.props.userVotedHandler(poll);
            });
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    getVotePercent(voteValue) {
        const poll = this.props.poll;

        if(poll.votesCtr === 0) return '0%';

        if(voteValue) return ((poll.yesVotesCtr / poll.votesCtr) * 100).toFixed(1) + '%';
        else return ((poll.noVotesCtr / poll.votesCtr) * 100).toFixed(1) + '%';
    }

    render() {
        const userVote = this.props.poll.userVote;
        const votingDisabled = typeof userVote === 'boolean';

        const yesVotesPerc = this.getVotePercent(true);
        const noVotesPerc = this.getVotePercent(false);

        return (
            <div className="law-items-wrapper">
                <div className="law-item clearfix">
                    <div className="avatar-wrapper">
                        <Link className="plain-style-link" to={'/sondaz/' + this.props.poll._id}>
                            <img
                                src={this.props.poll && require('../../../images/picker/200x150/' + this.props.poll['200x150'])} />
                        </Link>
                    </div>
                    <div className="law-item-content">
                        <Link className="plain-style-link" to={'/sondaz/' + this.props.poll._id}>
                            <h2>{this.props.poll.question}</h2>
                        </Link>
                        <Link to={'/sondaz/' + this.props.poll._id} className="simple-anchor">Zobacz szczegółowy opis</Link>

                        <p className="votes-counter">
                            Głosów: {this.props.poll.votesCtr}
                        </p>

                        <div className="vote-options">
                            <button onClick={() => this.vote(false)}
                                    disabled={votingDisabled}
                                    className={userVote ? 'vote-btn--selected deny-button rectangle-button' : 'rectangle-button deny-button'}>
                                {noVotesPerc} NIE {userVote == false ? '*' : ''}
                            </button>
                            <button onClick={() => this.vote(true)}
                                    disabled={votingDisabled}
                                    className={userVote == false ? 'vote-btn--selected accept-button rectangle-button' : 'rectangle-button accept-button'}>
                                {yesVotesPerc} TAK {userVote ? '*' : ''}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
