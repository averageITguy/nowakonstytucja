import React from 'react';

import {Row, Col, Grid} from 'react-flexbox-grid';
import Jumbotron from '../../Reusable/Jumbotron.component';
import Loader from '../../Reusable/Loader.component';

import PollsService from '../../../services/polls.service';

import PollNew from '../PollProposalNew.component';
import PollListItem from './_listItem.component';

require('styles/PollList.style.scss');

export default class PollList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            polls: null,
            loaded: false
        };

        this.getPolls();
    }

    getPolls() {
        this.getPollsPromise = PollsService.getPolls().then(polls => {
                if(!this.unmounted)
                    this.setState({polls, loaded: true});
            }
        );
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    generateList() {
        if(!this.state.polls) return null;

        return this.state.polls.map(poll => {
            return (
                <PollListItem key={poll._id}
                              userVotedHandler={this.userVotedHandler.bind(this)}
                              poll={poll} />
            );
        });
    }

    userVotedHandler(updatedPoll) {
        this.getPollsPromise.then(()=> { //to make sure there is no pending request to server
            const polls = this.state.polls;

            const poll = polls.find(_poll => _poll._id === updatedPoll._id);

            Object.assign(poll, updatedPoll);

            this.setState({polls});
        });
    }

    render() {
        return (
            <Loader loaded={this.state.loaded}>
                <div id="pollList">
                    <Jumbotron header="Lista sondaży"
                               hint="Głosuj i dyskutuj na kontrowersyjne tematy polskiej polityki." />

                    <Grid fluid className="poll-list-wrapper poll-list">
                        <Row>
                            <Col xs={12} md={10} mdOffset={1} className="poll-list-column">
                                <PollNew />
                                {this.generateList()}
                            </Col>
                        </Row>
                    </Grid>
                </div>
            </Loader>
        );
    }
}