import React from 'react';
import {Link} from 'react-router';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import {Row, Col, Grid} from 'react-flexbox-grid';

import PollsProposalsService from '../../services/pollsProposals.service';
import Toast from '../../services/toast.service';

import global from '../../services/global.service';


export default class PollProposalNew extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            errorText: {
                question: '',
                descShort: ''
            },
            submitDisabled: true
        };
    }

    proposePoll = () => {
        const form = document.getElementById('proposePollForm'),
            data = global.formDataToJSON(form);

        PollsProposalsService.proposePoll(data)
            .then(() => {
                Toast.display('Zgłosiłeś propozycję!',
                    'Stanie się ona dostępna dla innych użytkowników po zatwierdzeniu przez moderatora');
                this.handleClose();
            });
    };

    handleOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false, errorText: {}, submitDisabled: true});
    };

    validateInput = (event, value) => {
        const errorText = Object.assign(this.state.errorText, {
            [event.target.name]: value ? null : global.ERROR_MESSAGES.REQUIRED
        });

        this.setState({errorText});

        this.validateForm();
    };

    validateForm = () => {
        const form = document.getElementById('proposePollForm');

        this.setState({submitDisabled: !form.checkValidity()});
    };

    render() {
        const actions = [
            <FlatButton
                label="Anuluj"
                primary={true}
                onTouchTap={this.handleClose}
            />,
            <FlatButton
                label="Wyślij propozycję!"
                primary={true}
                keyboardFocused={true}
                disabled={this.state.submitDisabled}
                onTouchTap={this.proposePoll}
            />
        ];

        return (
            <div className="suggest-poll-button-wrapper">
                <Grid fluid className="suggest-poll-fluid">
                    <Row>
                        <Col xs={12} md={10} mdOffset={1}>
                            <Link className="archivesLink" to="/sondaze/archiwum">
                                ARCHIWUM SONDAŻY
                            </Link>
                            <div className="suggest-poll-wrapper">
                                <i className="icon icon-plus icon-absolute-position"></i>
                                <RaisedButton className="suggest-item-toast tooltip"
                                              label="Zaproponuj sondaż"
                                              onTouchTap={this.handleOpen} />
                            </div>
                            <Dialog
                                title="Zaproponuj sondaż"
                                actions={actions}
                                modal={false}
                                className="propose-poll-modal"
                                open={this.state.open}
                                onRequestClose={this.handleClose}>

                                <form id="proposePollForm">
                                    <TextField
                                        hintText="Np. Czy jesteś za tym, by uchodźcy trafiali do Polski?"
                                        floatingLabelText="Sformułuj pytanie tak, by warianty odpowiedzi 'Tak' i 'Nie' były logicznymi odpowiedziami"
                                        name="question"
                                        multiLine={false}
                                        fullWidth={true}
                                        errorText={this.state.errorText.question}
                                        onChange={this.validateInput}
                                        maxLength="200"
                                        rows={1}
                                        required={true}
                                    />

                                    <TextField
                                        hintText="Np. 710 tys. imigrantów trafiło w tym roku do Europy..."
                                        floatingLabelText="Podaj krótki opis przedstawiający kontekst pytania"
                                        name="descShort"
                                        multiLine={true}
                                        fullWidth={true}
                                        maxLength="400"
                                        rows={1}
                                    />
                                </form>

                            </Dialog>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}
