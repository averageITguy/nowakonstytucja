import React from 'react';

import Loader from '../../Reusable/Loader.component';

import PollsService from '../../../services/polls.service';

import PollNew from '../PollProposalNew.component';
import Slider from 'react-slick';
import PollSwiperItem from './_swiperItem.component';
import Comments from '../../Reusable/Comments/Comments.component';

import '../../../styles/Polls.style.scss';

export default class PollsSwiperComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentSlide: 1,
            slidesCounter: 0,
            polls: null,
            loaded: false
        };

        this.getPolls();

        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
    }

    getPolls() {
        this.getPollsPromise = PollsService.getPolls().then(polls => {
            if(!this.unmounted) this.setState({
                polls,
                slidesCounter: polls.length,
                loaded: true
            });
        });
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    next() {
        this.slider.slickNext();
    }

    previous() {
        this.slider.slickPrev()
    }

    afterSlideChanged(index) {
        this.setState({
            currentSlide: ++index
        });
    }

    userVotedHandler(updatedPoll) {
        this.getPollsPromise.then(()=> { //to make sure there is no pending request to server
            const polls = this.state.polls;

            const poll = polls.find(_poll => _poll._id === updatedPoll._id);

            Object.assign(poll, updatedPoll);

            this.setState({polls});

            this.next();
        });
    }

    generateItems() {
        if(!this.state.polls || this.state.polls.length === 0) return <div></div>;

        return this.state.polls.map(poll => {
            return (
                <div key={poll._id}>
                    <PollSwiperItem next={this.next}
                                    prev={this.previous}
                                    poll={poll}
                                    userVotedHandler={this.userVotedHandler.bind(this)} />
                </div>
            );
        });
    }

    render() {
        var settings = {
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            afterChange: this.afterSlideChanged.bind(this)
        };

        const currPoll = this.state.polls && this.state.polls[this.state.currentSlide - 1];

        return (
            <Loader loaded={this.state.loaded}>
                <div className="container-fluid polls-swiper-wrapper">
                    <div className="row">
                        <div className="polls-swiper col-xs-10 col-xs-offset-1">
                            <div className="suggest-poll-component-wrapper">
                                <PollNew />
                            </div>

                            <h1>SONDAŻE</h1>
                            <div className="switcher">
                            <span onClick={this.previous} className="arrow-wrapper">
                                <i className="icon icon-arrow-left"></i>COFNIJ
                            </span>

                                <span className="question-counter">
                                Sondaż <b>{this.state.currentSlide}</b> z <b>{this.state.slidesCounter}</b>
                            </span>

                                <span onClick={this.next} className="arrow-wrapper">
                                DALEJ<i className="icon icon-arrow-right"></i>
                            </span>
                            </div>

                            <Slider ref={c => this.slider = c} {...settings}>
                                {this.generateItems()}
                            </Slider>
                        </div>
                    </div>
                    <div className="discussion-component-wrapper">
                        <Comments entityId={currPoll && currPoll._id} single={true} />
                    </div>
                </div>
            </Loader>
        );
    }
}
