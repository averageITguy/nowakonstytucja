import React from 'react';
import {Link} from 'react-router';

import PollsService from '../../../services/polls.service';

import '../../../styles/Polls.style.scss';

export default class PollSwiperItem extends React.Component {
    constructor(props) {
        super(props);

        this.vote = this.vote.bind(this);
    }

    vote(value) {
        const pollId = this.props.poll._id,
            voteObj = {value};

        PollsService.vote(pollId, voteObj)
            .then(() => { //success - notify parent (list) to update this item
                const poll = this.props.poll;

                poll.userVote = value;
                poll.votesCtr = poll.votesCtr + 1;

                if(value) poll.yesVotesCtr = poll.yesVotesCtr + 1;
                else poll.noVotesCtr = poll.noVotesCtr + 1;

                this.props.userVotedHandler(poll);
            });
    }

    getVotePercent(voteValue) {
        const poll = this.props.poll;

        if(poll.votesCtr === 0) return '0%';

        if(voteValue) return ((poll.yesVotesCtr / poll.votesCtr) * 100).toFixed(1) + '%';
        else return ((poll.noVotesCtr / poll.votesCtr) * 100).toFixed(1) + '%';
    }

    render() {
        const userVote = this.props.poll.userVote;
        const votingDisabled = typeof userVote === 'boolean';

        const yesVotesPerc = this.getVotePercent(true);
        const noVotesPerc = this.getVotePercent(false);

        return (
            <div>
                <div className="swiper-item">
                    <Link to={'/sondaz/' + this.props.poll._id}
                          className="details-link plain-style-link">
                        <img
                            src={this.props.poll && require('../../../images/picker/600x200/' + this.props.poll['600x200'])} />
                    </Link>
                    <Link to={'/sondaz/' + this.props.poll._id}
                          className="details-link plain-style-link">
                        <h3>
                            {this.props.poll.question}
                        </h3>
                    </Link>
                    <p className="votes-counter">
                        Głosów: {this.props.poll.votesCtr}
                    </p>
                    <div className="vote-options">
                        <button onClick={() => this.vote(false)}
                                disabled={votingDisabled}
                                className="deny-button rectangle-button">
                            {noVotesPerc} NIE {userVote == false ? '*' : ''}
                        </button>
                        <button onClick={() => this.vote(true)}
                                disabled={votingDisabled}
                                className="accept-button rectangle-button">
                            {yesVotesPerc} TAK {userVote ? '*' : ''}
                        </button>
                    </div>
                </div>
            </div>
        );
    }

}
