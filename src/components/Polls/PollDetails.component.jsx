require('styles/Polls.style.scss');

import React from 'react';
import {Link} from 'react-router';

import Jumbotron from '../Reusable/Jumbotron.component';
import {Row, Col, Grid} from 'react-flexbox-grid';
import Loader from '../Reusable/Loader.component';

import PollsService from '../../services/polls.service';

import Comments from '../Reusable/Comments/Comments.component';

import '../../styles/Polls.style.scss';

import {
    Table,
    TableBody,
    TableRow,
    TableRowColumn
} from 'material-ui/Table';

export default class PollDetails extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            poll: null,
            loaded: false
        };

        this.getPoll(props.params.id);

        this.vote = this.vote.bind(this);
    }

    getPoll(id) {
        PollsService.getPoll(id).then(poll => {
            this.setState({poll, loaded: true});
        });
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    vote(value) {
        const pollId = this.state.poll._id,
            voteObj = {value};

        PollsService.vote(pollId, voteObj)
            .then(() => {
                const poll = this.state.poll;

                poll.userVote = value;
                poll.votesCtr = poll.votesCtr + 1;

                if(value) poll.yesVotesCtr = poll.yesVotesCtr + 1;
                else poll.noVotesCtr = poll.noVotesCtr + 1;

                this.setState({poll});
            });
    }

    generateStatistics() {
        if(!this.state.poll) return null;

        return (
            <div>
                <Table>
                    <TableBody displayRowCheckbox={false}>
                        <TableRow>
                            <TableRowColumn>Głosy za:</TableRowColumn>
                            <TableRowColumn>{this.state.poll.yesVotesCtr}</TableRowColumn>
                        </TableRow>
                        <TableRow>
                            <TableRowColumn>Głosy przeciw:</TableRowColumn>
                            <TableRowColumn>{this.state.poll.noVotesCtr}</TableRowColumn>
                        </TableRow>
                        <TableRow>
                            <TableRowColumn>Głosów ogółem: </TableRowColumn>
                            <TableRowColumn><b>{this.state.poll.votesCtr}</b></TableRowColumn>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }

    getVotePercent(voteValue) {
        if(!this.state.poll) return null;

        const poll = this.state.poll;

        if(poll.votesCtr === 0) return '0%';

        if(voteValue) return ((poll.yesVotesCtr / poll.votesCtr) * 100).toFixed(1) + '%';
        else return ((poll.noVotesCtr / poll.votesCtr) * 100).toFixed(1) + '%';
    }

    render() {
        var poll = this.state.poll;

        const userVote = poll && poll.userVote;
        const votingDisabled = typeof userVote === 'boolean';

        const yesVotesPerc = this.getVotePercent(true);
        const noVotesPerc = this.getVotePercent(false);

        const article = poll && poll.article ? (
            <a className="article"
               href={"http://wiadomosci.nowakonstytucja.org/" + poll.article.url}
               target="_blank">
                <span className="article-badge">Naszym zdaniem</span>
                <span
                    className="article-title">{poll.article.title}</span>
            </a>
        ) : null;

        const detailedDesc = poll && poll.descShort ? (
            <div className="short-description">
                <h4>Krótki opis</h4>
                <p>{poll && poll.descShort}</p>
            </div>
        ) : null;

        return (
            <div>
                <Jumbotron header="Szczegóły sondażu"
                           hint="Szczegółowe informacje na temat sondażu - opis, statystyki i dyskusja." />

                <Loader loaded={this.state.loaded}>
                    <Grid fluid className="initiatives-wrapper poll-details-wrapper">
                        <div className="white-pane">
                            <Grid fluid>
                                <Row>
                                    <Col xs={10} xsOffset={1}>
                                        <Link className="previous-page" to="/sondaze">
                                            <i className="icon-arrow-pointer"></i>Wróć do listy
                                            sondaży
                                        </Link>
                                    </Col>
                                </Row>
                            </Grid>
                        </div>
                        <Row>
                            <Col xs={12} md={6} mdOffset={1} className="poll-details">
                                <h3>Szczegółowe informacje</h3>
                                <img
                                    src={this.state.poll && require('../../images/picker/300x240/' + this.state.poll['300x240'])} />
                                <h2>{poll && poll.question}</h2>

                                {detailedDesc}

                                <div className="vote-options clearfix">
                                    <button onClick={() => this.vote(false)}
                                            disabled={votingDisabled}
                                            className={userVote ? 'vote-btn--selected deny-button rectangle-button' : 'rectangle-button deny-button'}>
                                        {noVotesPerc}
                                        NIE {userVote == false ? '*' : ''}
                                    </button>
                                    <button onClick={() => this.vote(true)}
                                            disabled={votingDisabled}
                                            className={userVote == false ? 'vote-btn--selected accept-button rectangle-button' : 'rectangle-button accept-button'}>
                                        {yesVotesPerc}
                                        TAK {userVote ? '*' : ''}
                                    </button>
                                </div>
                                <div className="discussion">
                                    <Comments entityId={this.state.poll && this.state.poll._id} />
                                </div>
                            </Col>
                            <Col xs={12} md={3} mdOffset={1} className="poll-details">
                                {article}
                                <div className="statistics">
                                    <h3>Statystyki</h3>
                                    {this.generateStatistics()}
                                </div>
                            </Col>
                        </Row>
                    </Grid>
                </Loader>
            </div>
        );
    }
}
