require('styles/PollList.style.scss');

import React from 'react';

import _ from 'lodash';

import InfiniteScroll from 'react-infinite-scroller';
import Jumbotron from '../../Reusable/Jumbotron.component';
import {Row, Col, Grid} from 'react-flexbox-grid';
import Loader from '../../Reusable/Loader.component';

import PollsService from '../../../services/polls.service';

import PollNew from '../PollProposalNew.component';
import PollArchiveListItem from './_listItem.component';

const image  = require('../../../images/loader.svg');

export default class PollArchiveList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            polls: null,
            filters: {
                phrase: null
            },
            currPage: 0,
            loaded: false
        };

        this.moreItemsToPaginate = true;

        this.getPolls();
    }

    getPolls() {
        let page = this.state.currPage + 1;

        this.getPollsPromise = PollsService.getPolls(true, {filters: this.state.filters}, page)
            .then(polls => {
                if(this.unmounted) return;

                if(page === 1) {//reset list
                    return this.setState({
                        polls,
                        currPage: page,
                        loaded: true
                    });
                }

                this.setState({
                    polls: _.uniqBy([...this.state.polls, ...polls], '_id'),
                    currPage: page,
                    loaded: true
                });

                this.moreItemsToPaginate = polls.length === 4; //hardcoded - items per page
            });
    }

    componentWillUnmount() {
        this.unmounted = true;
    }

    generateList() {
        if(!this.state.polls) return null;

        if(this.state.polls.length === 0) return (
            <div className="law-items-wrapper no-results">
                Brak wyników...
            </div>
        );

        return this.state.polls.map(poll => {
            return (
                <PollArchiveListItem key={poll._id}
                                     userVotedHandler={this.userVotedHandler.bind(this)}
                                     poll={poll} />
            );
        });
    }

    userVotedHandler(updatedPoll) {
        this.getPollsPromise.then(()=> { //to make sure there is no pending request to server
            const polls = this.state.polls;

            const poll = polls.find(_poll => _poll._id === updatedPoll._id);

            Object.assign(poll, updatedPoll);

            this.setState({polls});
        });
    }

    searchChanged(e) {
        const value = e.target.value;

        const _search = ()=> {
            const filters = this.state.filters;
            filters.phrase = value;

            this.moreItemsToPaginate = true;

            this.setState({
                filters: filters,
                currPage: 0 //reset
            }, () => this.getPolls());
        };

        clearTimeout(this.searchTimer);
        this.searchTimer = setTimeout(_search, 1000); //1s delay
    }

    render() {
        return (
            <Loader loaded={this.state.loaded}>
                <div className="poll-list-wrapper archieves-container">
                    <Jumbotron header="Archiwum sondaży"
                               hint="Archiwalne sondaże i powiązane z nimi dyskusje." />
                    <Grid fluid className="poll-list-wrapper archieves-page-content">
                        <div className="sticky-panel">
                            <Grid fluid>
                                <Row>
                                    <Col xs={12} md={10} mdOffset={1}>
                                        <input placeholder="Wpisz frazę..."
                                               onChange={this.searchChanged.bind(this)}></input>
                                        <PollNew />
                                    </Col>
                                </Row>
                            </Grid>
                        </div>
                        <Row>
                            <Col xs={12} md={10} mdOffset={1}>
                                <InfiniteScroll
                                    pageStart={0}
                                    loadMore={this.getPolls.bind(this)}
                                    hasMore={this.moreItemsToPaginate}
                                    loader={(<div id="loader-wrapper">
                                        <div className="table-row">
                                            <div id="table-cell-loader">
                                                <img src={image} />
                                            </div>
                                        </div>
                                    </div>)}>

                                    <div className="tracks">
                                        {this.generateList()}
                                    </div>
                                </InfiniteScroll>
                            </Col>
                        </Row>
                    </Grid>
                </div>
            </Loader>
        );
    }
}