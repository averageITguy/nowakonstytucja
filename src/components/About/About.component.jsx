import React from 'react';

import Jumbotron from '../Reusable/Jumbotron.component';
import {Row, Col, Grid} from 'react-flexbox-grid';

import '../../styles/About.style.scss';

export default class About extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const buttonStyle = {
            marginBottom: '50px',
            marginTop: '40px'
        };

        return (
            <div className="about-wrapper">
                <Jumbotron header="O nas"
                           hint="Informacje o autorach i intencjach projektu nowakonstytucja.org" />
                <Grid fluid>
                    <Row>
                        <Col xs={12} md={7} mdOffset={1} xsOffset={1}>
                            <h1>O projekcie</h1>
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;
                                Obserwujemy jak świat wokół nas zmienia się w zawrotnym tempie. Jak
                                często zdarza się nam
                                patrzeć na kierunek tych zmian z niepokojem? Jaka jest rola nas
                                Polaków w tych niespokojnych
                                czasach? Projekt nowakonstytucja.org powstał w trosce o nasz wspólny
                                dom – Państwo Polskie.
                            </p>
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;
                                Powszechny dostęp do internetu wywarł nieprawdopodobny wpływ na
                                wiele dziedzin naszego
                                życia. Wierzymy, że przyniósł już także istotną zmianę w sferze
                                politycznej. Naszym celem, jako
                                autorów projektu, było stworzenie niezależnej platformy do debaty
                                publicznej oraz narzędzi
                                promujących obywatelskie działania intelektualne na rzecz Naszego
                                Państwa.
                            </p>
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;
                                Uważamy, że nie da się znaleźć dobrego rozwiązania bez
                                wcześniejszego, jasnego
                                zdefiniowania problemu. Myśl ta stała się dla nas motywem przewodnim
                                i znalazło to swoje
                                odzwierciedlenie w aplikacji. Mamy ogromną nadzieję, że razem uda
                                nam się zdefiniować problemy
                                ważne dla nas Polaków i co więcej - wspólnie znaleźć dla nich
                                rozwiązania, co do których jako
                                obywatele, jesteśmy zgodni. Pozwoli to nam nakreślić kierunek, w
                                którym chcemy aby Nasz Kraj
                                zmierzał w najbliższych latach.
                            </p>
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;
                                Liczymy na to, że obywatele zjednoczą się w tak ważnej sprawie i
                                wszelkie podziały odejdą na
                                drugi plan a konsekwencją będzie nic innego jak poprawa jakości
                                życia w Naszym Kraju. Mamy także
                                nadzieję, że dzięki tej metodzie na nowoczesną, proobywatelską
                                demokrację, my jako Polacy, po raz
                                kolejny w swojej historii, posłużymy innym za przykład. Los tej
                                inicjatywy jest w Waszych rękach.
                            </p>
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;
                                Projekt jest realizowany wyłącznie za nasze prywatne pieniądze,
                                poświeciliśmy mu ogrom
                                czasu i pracy. Nie należymy do żadnej partii ani ugrupowania
                                politycznego oraz staramy się być jak
                                najbardziej obiektywni w swoich działaniach.
                            </p>

                            <h1>O autorach</h1>
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;
                                Dwóch młodych chłopaków z branży IT walczących o lepszą Polskę.
                            </p>
                            <h1>Darowizna</h1>
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;
                                Mamy wiele pomysłów jak ulepszyć i rozwijać ten portal, jednak nie
                                jesteśmy w stanie poświęcać mu charytatywnie całego czasu. Jeżeli
                                chcecie wesprzeć rozwój i utrzymanie portalu możecie przekazać nam
                                darowiznę za pomocą przycisku poniżej:
                            </p>
                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post"
                                  target="_top" style={buttonStyle}>
                                <input type="hidden" name="cmd" value="_s-xclick" />
                                <input type="hidden" name="hosted_button_id"
                                       value="47AWZTL5CUDMY" />
                                <input type="image" className="donate-img"
                                       src="https://www.paypalobjects.com/pl_PL/PL/i/btn/btn_donateCC_LG.gif"
                                       name="submit"
                                       alt="PayPal – Płać wygodnie i bezpiecznie" />
                                <img alt=""
                                     src="https://www.paypalobjects.com/pl_PL/i/scr/pixel.gif"
                                     width="1" height="1" />
                            </form>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}