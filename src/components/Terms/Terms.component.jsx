import React from 'react';

import Jumbotron from '../Reusable/Jumbotron.component';
import {Row, Col, Grid} from 'react-flexbox-grid';

export default class About extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Jumbotron header="Regulamin"
                           hint="Regulamin portalu nowakonstytucja.org" />
                <Grid fluid>
                    <Row>
                        <Col xs={12} md={7} mdOffset={1} xsOffset={1}>
                            <h1> Regulamin portalu nowakonstytucja.org</h1>

                            <h4>§ 1. Wstęp</h4>
                            <p>1. Niniejszy regulamin określa zasady korzystania i funkcjonowania z
                                portalu nowakonstytucja.pl</p>
                            <p>2. Regulamin określa prawa i obowiązki Użytkowników Portalu, a także
                                prawa, obowiązki i zakres odpowiedzialności Administratora jako
                                podmiotu
                                zarządzającego i prowadzącego Portal. Każdy potencjalny Użytkownik
                                zobowiązany jest zapoznać się z treścią Regulaminu i może podejmować
                                dalsze czynności po uprzednim wyrażeniu zgody i akceptacji
                                wszystkich
                                jego postanowień.</p>
                            <p>3. Użytkownik publikuje w Portalu wszelkie treści i materiały
                                wyłącznie
                                na własną odpowiedzialność - serwis nie ponosi jakiejkolwiek
                                odpowiedzialności za materiały, treści i wiarygodność danych
                                osobowych i
                                prawdziwość informacji zamieszczonych przez użytkowników.</p>
                            <h4>§ 2. Definicje</h4>
                            <p>1. Regulamin - niniejszy dokument wraz z wszelkimi załącznikami.</p>
                            <p>2. Portal / Serwis - należy przez to rozumieć internetowy
                                portal
                                społecznościowy dostępny pod adresem www.nowakonstytucja.org
                                którego
                                właścicielami i administratorami są Alan Sobieraj i Marcin
                                Trybulec.</p>
                            <p>3. Usługi (Usługa o, Usługa Forum,) na portalu świadczone są
                                przez osoby
                                wymienione wyżej.</p>
                            <p>4. Korzystanie z Usług możliwe jest wyłącznie w oparciu o
                                niniejszy
                                regulamin (dalej „Regulamin”) i w sposób z nim zgodny.</p>
                            <p>5. Przed skorzystaniem z Usług należy uważnie przeczytać
                                poniższy
                                Regulamin. Korzystając z Usługi Użytkownik potwierdza, że
                                akceptuje jego
                                postanowienia i zobowiązuje się do ich przestrzegania. Każde
                                zalogowanie
                                się na stronę Serwisu jest równoznaczne z akceptacją
                                Regulaminu serwisu
                                przez Uczestnika.</p>
                            <p>6. Administrator odpowiedzialny jest za prowadzenie
                                Portalu, w tym w
                                szczególności za wsparcie techniczne dla Użytkowników
                                oraz pilnowanie
                                porządku i przestrzegania przepisów Regulaminu przez
                                Użytkowników.</p>
                            <p>7. Użytkownik - osoba fizyczna, osoba prawna, która
                                poprzez akceptację
                                Regulaminu i przejście procedury rejestracyjnej
                                założy Konto w Portalu,
                                poprzez uzyskanie Loginu i hasła pozwalającego
                                zalogować się na Konto
                                oraz uzyskać dostęp do usług oferowanych przez
                                Portal oraz osoba
                                fizyczna ,osoba prawna , która poprzez akceptację
                                regulaminu uzyskuje
                                dostęp do usług oferowanych przez Portal.
                                Użytkownikami mogą być
                                wyłącznie osoby pełnoletnie oraz osoby
                                niepełnoletnie posiadające zgodę
                                opiekuna bądź przedstawiciela ustawowego na
                                korzystanie z usług portalu
                                nowakonstytucja.org.</p>
                            <p>8. Materiały - wszelkie teksty (o charakterze
                                informacyjnym,
                                publicystycznym, beletrystycznym bądź
                                satyrycznym), opracowania,
                                statystyki, wykresy, komentarze, obrazy (jak
                                fotografie, filmy i inne)
                                oraz inne treści (pliki dźwiękowe i inne),
                                publikowane przez Użytkownika
                                na portalu kolniak24.pl.</p>
                            <p>9. Forum - miejsce na portalu przeznaczone do
                                wyrażania opinii przez
                                Użytkowników.</p>

                            <p>10. Komentarze (posty) - treść opinii
                                wyrażonej przez Użytkownika i
                                opublikowanej przez niego na forum
                                portalu nowakonstytucja.org. Post
                                stanowi wyraz indywidualnej opinii
                                Użytkownika i odnosić może się m.in.
                                do publikowanych w Serwisie treści bądź
                                opinii innych Użytkowników.</p>
                            <p>11. Profil - miejsce w Portalu, gdzie
                                opublikowany jest zbiór informacji
                                zawierających zdjęcie, opis oraz
                                inne dane wpisane przez danego
                                Użytkownika;</p>
                            <p>12. Nick - podpis wybrany przez
                                Użytkownika dla oznaczenia
                                opublikowanego przez niego
                                Posta.</p>
                            <p>13.Login - prywatna i
                                rzeczywista nazwa
                                Użytkownika w Serwisie, jaka
                                została przez niego
                                przybrana na etapie
                                rejestracji Konta;</p>
                            <p>14. Konto - jest to dostępne
                                po zalogowaniu się (podaniu
                                Loginu oraz
                                hasła) miejsce w Serwisie, w
                                którym każdy zarejestrowany
                                Użytkownik
                                Portalu może wprowadzić jak
                                i modyfikować swoje dane,
                                opisy, zdjęcia i
                                inne elementy związane z
                                zarejestrowaniem w Serwisie;</p>
                            <p>15. Baza Profili - zbiór
                                danych, informacji i innych
                                treści przekazanych
                                dobrowolnie przez wszystkich
                                Użytkowników do Serwisu,
                                które są
                                gromadzone i przetwarzane w
                                uporządkowany sposób w
                                systemie
                                informatycznym przez
                                Administratora za zgodą
                                Użytkownika, na potrzeby
                                świadczenia usług Serwisu;</p>

                            <h4>§ 3. Informacje podstawowe
                                oraz techniczne warunki
                                korzystania z Portalu</h4>
                            <p>1. Uczestnictwo w Portalu
                                jest dobrowolne i bezpłatne.</p>
                            <p>2. Dodatkowe odpłatne usługi
                                świadczone są tylko i
                                wyłącznie na wyraźne
                                zlecenie Użytkownika po
                                wniesieniu stosownej opłaty
                                za daną usługę.</p>
                            <p>3. Baza profili podlega
                                ochronie prawnej, a
                                administratorem danych
                                osobowych Użytkowników jest
                                Administrator Serwisu. Każdy
                                Użytkownik ma
                                prawo wglądu do swoich
                                danych przetwarzanych przez
                                powyższe podmioty,
                                prawo do poprawiania tych
                                danych oraz do żądania
                                zaniechania ich
                                przetwarzania poprzez
                                usunięcie Konta. Podając
                                swoje dane osobowe
                                Użytkownik wyraża
                                jednocześnie zgodę na
                                przetwarzanie swoich danych
                                osobowych do celów
                                związanych z działalnością
                                Portalu oraz do celów
                                marketingowych. Dane osobowe
                                są chronione zgodnie z
                                ustawą z dnia 29
                                sierpnia 1997 r. o ochronie
                                danych osobowych (Dz.U.2002,
                                Nr 101, poz.
                                926 ze zm.) w sposób
                                uniemożliwiający dostęp do
                                nich osób trzecich.</p>

                            <h4>§ 4. Zasady ogólne</h4>
                            <p>1.Użytkownikowi niedozwolone
                                jest :
                                - używanie wulgaryzmów i
                                zwrotów mogących w
                                jakikolwiek sposób naruszyć
                                czyjąś godność,
                                - obrażanie innych osób,
                                - promowania innych stron
                                internetowych,
                                - umieszczanie materiałów
                                erotycznych w jakiejkolwiek
                                formie,
                                - łamanie ogólnie przyjętych
                                zasad etyki,
                                - wielokrotne umieszczanie
                                tych samych zdjęć, treści
                                czy innych
                                elementów,</p>
                            <p>2. Użytkownik oświadcza, iż
                                posiada odpowiednie zgody i
                                uprawnienia do
                                materiałów umieszczonych w
                                Serwisie, a w szczególności
                                do zdjęć i
                                filmów.</p>
                            <p>3. Użytkownik ponosi pełną
                                odpowiedzialność za
                                korzystanie z usług
                                portalu nowakonstytucja.org.</p>
                            <p>4. Administrator Portalu
                                może usunąć materiały
                                umieszczone przez
                                Użytkownika niespełniające
                                Regulaminu, bez konieczności
                                informowania go
                                o tym.</p>
                            <p>5. Publikując materiały,
                                treści i dane osobowe
                                użytkownik jednocześnie
                                wyraża zgodę na ich
                                publiczne i nieodpłatne
                                udostępnianie.</p>

                            <h4>§ 5. Usługa Konta</h4>
                            <p>1. Uczestnikami
                                Serwisu są
                                jego Użytkownicy.</p>
                            <p>2. Uczestnikiem
                                Serwisu może
                                być każda osoba
                                fizyczna ,
                                która po
                                zaakceptowaniu
                                postanowień
                                Regulaminu,
                                zrealizuje
                                procedurę
                                rejestracji
                                w Serwisie
                                zakończoną
                                skutecznym
                                założeniem
                                Konta.</p>
                            <p>3. W celu
                                założenia
                                Konta
                                oraz
                                korzystania
                                z Serwisu
                                niezbędny
                                jest
                                dostęp do
                                Internetu,
                                przeglądarka
                                internetowa
                                oraz
                                posiadanie
                                dowolnej
                                aktywnej
                                Skrzynki
                                e-mail.</p>
                            <p>4.
                                Utworzenie
                                i
                                korzystanie
                                z Konta
                                w
                                Serwisie
                                jest
                                dobrowolne
                                i
                                bezpłatne.</p>
                            <p>5.
                                Odpłatne
                                usługi
                                świadczone
                                będą
                                przez
                                Administratora
                                na
                                rzecz
                                Użytkowników
                                wyłącznie
                                po
                                złożeniu
                                dyspozycji
                                i
                                wniesieniu
                                opłaty.
                                zapis
                                przyszłościowy</p>

                            <p>
                                6.
                                Akceptacja
                                Regulaminu
                                przez
                                Użytkownika
                                Serwisu
                                jest
                                równoznaczna
                                ze
                                złożeniem
                                oświadczeń
                                następujących
                                treści:
                                •
                                zapoznałem
                                się
                                z
                                Regulaminem
                                i
                                akceptuję
                                wszystkie
                                jego
                                postanowienia
                                •
                                dobrowolnie
                                przystąpiłem
                                do
                                korzystania
                                z
                                usług
                                Serwisu
                                dane
                                zawarte
                                w
                                formularzu
                                rejestracyjnym
                                oraz
                                na
                                Koncie
                                są
                                zgodne
                                z
                                prawdą
                                •
                                wyrażam
                                zgodę
                                na
                                przetwarzanie
                                moich
                                danych
                                osobowych
                                przez
                                Administratora,
                                przekazywanych
                                w
                                formularzu
                                rejestracyjnym,
                                dla
                                celów
                                należytego
                                wykonania
                                umowy
                                o
                                świadczeniu
                                usług
                                drogą
                                elektroniczną
                                •
                                zezwalam
                                na
                                wykorzystywanie
                                mojego
                                wizerunku
                                umieszczonego
                                w
                                Serwisie
                                dla
                                celów
                                świadczenia
                                przez
                                Administratora
                                usług
                                w
                                Serwisie,
                                jak
                                również
                                oświadczam,
                                iż
                                osoby,
                                których
                                wizerunek
                                umieściłem
                                w
                                Serwisie
                                udzieliły
                                takiego
                                zezwolenia
                                •
                                wyrażam
                                zgodę
                                na
                                otrzymywanie
                                wskazanych
                                w
                                formularzu
                                rejestracyjnym
                                informacji
                                systemowych,
                                wiadomości
                                od
                                Administratora
                                oraz
                                informacji
                                o
                                utrudnieniach,
                                zmianach
                                czy
                                przerwach
                                technicznych
                                w
                                działaniu
                                Serwisu
                            </p><p>
                            7.
                            Umowa
                            o
                            świadczeniu
                            usług
                            drogą
                            elektroniczną
                            zostaje
                            zawarta
                            z
                            chwilą
                            rejestracji,
                            na
                            czas
                            nieokreślony.

                        </p> <p>
                            8.
                            Jedna
                            osoba
                            może
                            mieć
                            maksymalnie
                            1
                            Konto
                            w
                            Serwisie.
                            Niedozwolone
                            jest
                            cedowanie
                            lub
                            udostępnianie
                            Konta
                            innym
                            osobom,
                            bez
                            wyraźnej
                            zgody
                            Administratora.
                        </p>   <p>
                            9.
                            W
                            przypadku
                            stwierdzenia
                            łamania
                            przez
                            Użytkownika
                            zasad
                            niniejszego
                            Regulaminu,
                            jego
                            konto
                            zostanie
                            zablokowane
                            .
                        </p>  <p>
                            10.
                            Użytkownik
                            może
                            zażądać
                            usunięcia
                            swojego
                            konta,
                            po
                            uprzednim
                            skontaktowaniu
                            się
                            z
                            administracją
                            serwisu.
                            Po
                            usunięciu
                            konta
                            nie
                            ma
                            możliwości
                            jego
                            przywrócenia.
                            Użytkownik
                            ma
                            możliwość
                            zmiany
                            lub
                            usunięcia
                            swoich
                            danych
                            za
                            pomocą
                            opcji
                            Konta.
                        </p>    <p>
                            11.
                            Z
                            Konta
                            może
                            korzystać
                            tylko
                            i
                            wyłącznie
                            Użytkownik,
                            zabrania
                            się
                            zakładania
                            Konta
                            z
                            którego
                            będzie
                            korzystać
                            kilka
                            osób.
                        </p>      <p>
                            12.
                            Każde
                            nowo
                            założone
                            konto
                            podlega
                            aktywacji,
                            polegającej
                            na
                            odebraniu
                            wiadomości
                            wysłanej
                            na
                            skrzynkę
                            e-mailową
                            Użytkownika
                            oraz
                            postępowaniu
                            według
                            instrukcji
                            w
                            niej
                            zawartych.
                        </p>       <p>
                            13.
                            Udostępniając
                            zdjęcie/film
                            itp.
                            w
                            Serwisie,
                            Użytkownik
                            oświadcza,
                            iż:
                            a)
                            posiada
                            on
                            uprawnienia
                            lub
                            zgody
                            wymagane
                            przepisami
                            prawa
                            do
                            tego,
                            aby
                            zdjęcie
                            to
                            mogło
                            być
                            udostępnione
                            publicznie
                            (opublikowane)
                            na
                            łamach
                            Serwisu
                            oraz
                            w
                            innych
                            mediach,
                            zgodnie
                            ze
                            zgodami
                            udzielonymi
                            przez
                            Użytkownika
                            na
                            podstawie
                            innych
                            postanowień
                            Regulaminu
                            i
                            nie
                            spowoduje
                            to
                            naruszenia
                            jakichkolwiek
                            praw
                            osób
                            trzecich;
                            b)
                            w
                            przypadku
                            skierowania
                            przez
                            podmioty
                            trzecie
                            jakichkolwiek
                            roszczeń
                            do
                            Administratora,
                            odnośnie
                            naruszania
                            jakichkolwiek
                            praw
                            do
                            opublikowanego
                            zdjęcia
                            lub
                            wizerunku
                            osobistego
                            osób
                            trzecich,
                            Użytkownik
                            zobowiązuje
                            się
                            złożyć
                            pisemne
                            oświadczenie
                            odpowiedniej
                            treści,
                            które
                            uwolni
                            Administratora
                            od
                            jakiejkolwiek
                            odpowiedzialności
                            w
                            tym
                            zakresie.
                        </p>          <p>
                            14.
                            Administrator
                            ma
                            prawo
                            zmniejszenia/skompresowania
                            zdjęć
                            Użytkownika,
                            bez
                            wcześniejszego
                            informowania
                            go
                            o
                            tym
                            fakcie.
                        </p>         <p>
                            15.
                            Administrator
                            Serwisu
                            nie
                            jest
                            w
                            jakikolwiek
                            sposób
                            ograniczony
                            czasem
                            w
                            jakim
                            musi
                            zweryfikować
                            zdjęcie/film
                            wstawione
                            poprzez
                            Użytkownika.
                        </p>         <p>
                            16.
                            Konto
                            może
                            zostać
                            usunięte
                            lub
                            zablokowane
                            przez
                            Administratora
                            bez
                            jakichkolwiek
                            uprzedzeń,
                            co
                            spowoduje
                            całkowite
                            i
                            bezpowrotne
                            usunięcie
                            Konta,
                            jeżeli
                            Administrator
                            stwierdzi,
                            iż:
                            a)
                            Użytkownik
                            narusza
                            postanowienia
                            Regulaminu;
                            b)
                            Użytkownik
                            rozpowszechnia
                            treści
                            pornograficzne;
                            c)
                            Użytkownik
                            uprawia
                            działalność
                            komercyjną,
                            reklamową,
                            promocyjną;
                            d)
                            Użytkownik
                            podejmuje
                            jakiekolwiek
                            działania,
                            poprzez
                            które
                            osoba
                            lub
                            podmiot
                            je
                            realizujący
                            stara
                            się
                            wpływać
                            na
                            Użytkowników,
                            działać
                            na
                            ich
                            szkodę,
                            szkodę
                            Administratora
                            lub
                            innych
                            podmiotów
                            trzecich;
                            e)
                            Użytkownik
                            publikuje
                            na
                            łamach
                            portalu
                            treści
                            naruszające
                            polskie
                            lub
                            międzynarodowe
                            przepisy
                            prawa,
                            dobre
                            obyczaje
                            lub
                            normy
                            moralne,
                            obrażające
                            godność
                            lub
                            naruszające
                            dobra
                            osobiste
                            innych
                            osób,
                            popierające
                            radykalne
                            postawy
                            społeczne
                            lub
                            głoszące
                            takie
                            poglądy
                            (wszelkiego
                            rodzaju
                            dyskryminacja
                            rasowa,
                            etniczna,
                            ze
                            względu
                            na
                            płeć,
                            wyznanie
                            itp.);
                            f)
                            Użytkownik
                            nie
                            zaakceptuje
                            zmian
                            wprowadzanych
                            przez
                            Administratora
                            w
                            Regulaminie;
                        </p>           <h4>
                            §
                            6.
                            Zastrzeżenia,
                            zapewnienia
                            i
                            odpowiedzialność
                        </h4>        <p>
                            1.
                            Użytkownik
                            zamieszczający
                            treści
                            zabronione
                            może
                            ponieść
                            odpowiedzialność
                            karną
                            lub
                            cywilną
                            względem
                            Administratora,
                            innych
                            Użytkowników
                            bądź
                            osób
                            trzecich.
                        </p>          <p>
                            2.
                            Administrator
                            nie
                            ponosi
                            odpowiedzialności
                            za
                            działania
                            innych
                            Użytkowników,
                            ani
                            za
                            wykorzystanie
                            przez
                            nie
                            danych
                            Użytkowników
                            niezgodnie
                            z
                            celem
                            działania
                            Portalu.
                        </p>           <p>
                            3.
                            Administrator
                            nie
                            ponosi
                            odpowiedzialności
                            w
                            przypadku
                            skierowania
                            do
                            niego
                            przez
                            innych
                            Użytkowników
                            lub
                            osoby
                            trzecie
                            roszczeń
                            związanych
                            z
                            opublikowaniem
                            zdjęć/filmów
                            lub
                            osobistego
                            wizerunku
                            tych
                            Użytkowników.
                            Odpowiedzialność
                            w
                            tym
                            zakresie
                            ponosi
                            wyłącznie
                            Użytkownik,
                            który
                            opublikował
                            sporne
                            zdjęcie
                            lub
                            wizerunek
                            innej
                            osoby.
                        </p>        <p>
                            4.Administrator
                            ma
                            obowiązek
                            udostępnić
                            podane
                            przez
                            Użytkownika
                            dane
                            osobowe
                            (w
                            tym
                            w
                            szczególności
                            IP
                            komputerów,
                            z
                            których
                            się
                            logował
                            lub
                            przesyłał
                            teksty)
                            na
                            żądanie
                            uprawnionych
                            organów
                            (sądu,
                            prokuratury,
                            policji).
                        </p>      <p>
                            5.
                            Administrator
                            zobowiązuje
                            się
                            do
                            zapewnienia
                            jak
                            najwyższej
                            jakości
                            świadczonych
                            usług.
                            Administrator
                            nie
                            ponosi
                            odpowiedzialności
                            za
                            zakłócenia
                            w
                            funkcjonowaniu
                            Portalu
                            wywołane
                            siłą
                            wyższą,
                            awarią
                            sprzętu
                            lub
                            niedozwoloną
                            ingerencją
                            Użytkowników,
                            nawet
                            jeśli
                            spowodowałyby
                            one
                            utratę
                            danych
                            na
                            Kontach
                            Użytkowników.
                        </p>      <h4>
                            §
                            7.
                            Postępowanie
                            reklamacyjne
                        </h4>      <p>
                            1.
                            Użytkownicy
                            mogą
                            składać
                            Prowadzącemu
                            Portal
                            reklamacje
                            dotyczące
                            nieprawidłowego
                            funkcjonowania
                            Portalu.
                        </p>        <p>
                            2.
                            Reklamację
                            składa
                            się
                            w
                            formie
                            pisemnej
                            za
                            pośrednictwem
                            poczty
                            elektronicznej
                            wysłanej
                            na
                            adres
                            nkonstytucja@gmail.com
                            lub
                            w
                            formie
                            listu
                            poleconego
                            wysłanego
                            na
                            adres
                            Świętego
                            Bonifacego
                            71/17
                            02-936
                            Warszawa
                        </p>  <p>
                            3.
                            Zaleca
                            się,
                            aby
                            reklamacja
                            zawierała
                            następujące
                            dane:
                            a.
                            adres
                            poczty
                            elektronicznej
                            lub
                            adres
                            zamieszkania
                            Użytkownika,
                            na
                            który
                            Użytkownik
                            chce
                            otrzymać
                            odpowiedź
                            na
                            złożoną
                            reklamację,
                            oraz
                            b.
                            zwięzłe
                            podanie
                            okoliczności
                            uzasadniających
                            składaną
                            reklamację
                            (w
                            tym
                            wskazanie
                            czego
                            reklamacja
                            dotyczy).
                        </p>     <p>
                            4.
                            Prowadzący
                            Portal
                            zobowiązany
                            jest
                            rozpatrzyć
                            reklamację,
                            w
                            terminie
                            nie
                            dłuższym
                            niż
                            14
                            (czternaście)
                            dni
                            od
                            dnia
                            jej
                            otrzymania.
                            W
                            przypadku
                            bezskutecznego
                            upływu
                            powyższego
                            terminu
                            uznaje
                            się,
                            iż
                            Prowadzący
                            Portal
                            uznał
                            reklamację
                            w
                            całości.

                        </p>  <h4>
                            §
                            8
                            .
                            Zmiany
                            Regulaminu
                        </h4>   <p>
                            1.
                            Wszelkie
                            postanowienia
                            Regulaminu
                            mogą
                            być
                            w
                            każdej
                            chwili
                            zmieniane
                            przez
                            Administratora,
                            bez
                            podawania
                            przyczyn.
                            Zmiany
                            będą
                            publikowane
                            na
                            bieżąco
                            w
                            postaci
                            ujednoliconego
                            tekstu
                            Regulaminu
                            na
                            łamach
                            Portalu
                            wraz
                            z
                            informacją
                            o
                            ich
                            dokonaniu.
                        </p>        <p>
                            2.
                            Po
                            ukazaniu
                            się
                            na
                            stronie
                            głównej
                            Portalu
                            lub
                            w
                            innych
                            miejscach
                            do
                            tego
                            przeznaczonych
                            informacji
                            o
                            zmianach
                            w
                            Regulaminie,
                            Użytkownik
                            powinien
                            niezwłocznie
                            zapoznać
                            się
                            ze
                            zmianami,
                            gdyż
                            zalogowanie
                            do
                            Serwisu
                            po
                            takim
                            ogłoszeniu
                            stanowi
                            bezwarunkową
                            akceptację
                            nowej
                            treści
                            Regulaminu
                            przez
                            Użytkownika
                        </p>   <p>
                            3.
                            Oświadczenie
                            o
                            nie
                            zaakceptowaniu
                            zmian
                            w
                            Regulaminie,
                            pociągnie
                            za
                            sobą
                            usunięcie
                            Konta
                            Użytkownika.</p>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}