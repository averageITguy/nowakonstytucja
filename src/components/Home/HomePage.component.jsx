import Jumbotron from './Jumbotron.component';
import PollSwiper from '../Polls/PollSwiper/PollSwiper.component';
import ConstitutionSwiper from '../Constitution/ConstitutionSwiper/ConstitutionSwiper.component';

import React from 'react';
import { Row, Col, Grid } from 'react-flexbox-grid';

export default class Home extends React.Component {

    render() {
        return (
        	<div className="home-page">
	            <Jumbotron header="Witamy na nowakonstytucja.org" hint="Wykrzycz swoją opinię, zainteresuj ludzi problemami kraju, proponuj rozwiązania i dyskutuj."/>
	            <PollSwiper />
	            <ConstitutionSwiper />
		      	<Grid fluid>
			        <Row>
			            <Col xs={12} md={10} mdOffset={1}>
	             
	            		</Col>
			        </Row>
		        </Grid>
        	</div>
        );
    }
}
