import React from 'react';

import AddNewComment from './_addNew.component';
import Comment from './_comment.component';

import CommentService from '../../../services/comment.service';

import '../../../styles/Comments.style.scss';

export default class extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            single: this.props.single || false,
            comments: [],
            sort: 'newest', /*[newest, popular]*/
            page: 1,
            moreCommentsToLoad: true,
        };

        this.getComments();
    }

    getComments(entityId) {
        CommentService.getComments(entityId || this.props.entityId, this.state.sort, this.state.page, this.state.single).then(comments=> {
            const skipSize = this.state.single ? 1 : 5; //must by in sync with backend value

            this.setState({
                comments,
                moreCommentsToLoad: this.state.page * skipSize === comments.length,
            });
        });
    }

    showMore() {
        this.setState({
            page: this.state.page + 1
        }, () => {
            this.getComments();
        });
    }

    removeSingle() {
        this.setState({
            single: false
        }, ()=> {
            this.getComments();
        })
    }

    votedHandler(commentId, vote, parentId) {
        const voteVal = vote === 'like';

        CommentService.vote(commentId, {value: voteVal}, parentId).then(() => {
            const comments = this.state.comments;

            let votedComment = null;

            if(parentId) {//voted on sub comment
                votedComment = comments.find(comment => comment._id === parentId).responses.find(comment => comment._id === commentId);
            } else {
                votedComment = comments.find(comment => comment._id === commentId);
            }

            voteVal ? votedComment.likes += 1 : votedComment.dislikes += 1;
            votedComment.userVote = voteVal;

            this.setState({comments});
        });
    }

    commentAddedHandler(newComment, parentId) {
        CommentService.addComment({
            ...newComment,
            parentId
        }, this.props.entityId).then(newDbComment => {
            const comments = this.state.comments;

            if(parentId) { //new comment is sub-comment
                const parent = comments.find(comment => comment._id === parentId);
                parent.responses.push(newDbComment);
            } else { //new comment is top lvl comment
                comments.push(newDbComment);
            }

            this.setState({comments});
        });
    }

    sortHandler(e) {
        const newSortValue = e.target.value;

        this.setState({
            sort: newSortValue
        }, ()=> {
            this.getComments();
        });
    }

    generateComments() {
        return this.state.comments.map(comment => {
            return <Comment key={comment._id}
                            votedHandler={this.votedHandler.bind(this)}
                            commentAddedHandler={this.commentAddedHandler.bind(this)}
                            showMoreResponsesHandler={this.showMoreResponsesHandler.bind(this)}
                            {...comment} />
        });
    }

    componentWillReceiveProps(nextProps) {
        //react to entityId change (in swiper)
        this.setState({
            single: nextProps.single //1) reset single property to initial value
        }, ()=> {
            this.getComments(nextProps.entityId); //2) get comments for new ID
        });
    }

    showMoreResponsesHandler(parentCommentId) {
        CommentService.showMoreResponses(parentCommentId, this.props.entityId).then(responses => {
            const comments = this.state.comments;
            const parent = comments.find(comment => comment._id === parentCommentId);

            parent.responses = responses;

            this.setState({comments});
        });
    }

    render() {
        const comments = this.generateComments();

        //show more than one comment
        const removeSingleBtn = this.state.single && this.state.comments.length > 0 ? (
            <button className="show-more-comments" onClick={this.removeSingle.bind(this)}>Wyświetl komentarze</button>
        ) : null;

        //show next fives comments (like next page)
        const showMoreBtn = !this.state.single && this.state.moreCommentsToLoad && this.state.comments.length > 0 ? (
            <button className="show-more-comments" onClick={this.showMore.bind(this)}>Pokaż kolejne</button>
        ) : null;

        return (
            <section className="comments-wrapper">
                <AddNewComment addResponse={this.commentAddedHandler.bind(this)} />

                {
                    this.state.comments.length > 0 ? (
                      <div>
                        <div className="comments-filter-wrapper">
                          <span>Filtruj komentarze po:</span>
                            <select onChange={this.sortHandler.bind(this)} defaultValue="newest" className="comments-filter">
                                <option value="newest">Najnowsze</option>
                                <option value="popular">Popularne</option>
                            </select>
                        </div>
                        {comments}
                        <hr></hr>
                      </div>
                    ) : null
                }

                {removeSingleBtn} {showMoreBtn}
            </section>
        );
    }
}
