import React from 'react';

import global from '../../../services/global.service';

export default class extends React.Component {
    constructor(props) {
        super(props);

        //multiple copies of this component can be simultaneously placed in the DOM
        this.uniqueId = Math.floor(Math.random() * 10000000000);
    }

    addResponse() {
        const form = document.getElementById('newCommentForm_' + this.uniqueId);
        global.markFormAsSubmitted(form);

        if(!form.checkValidity()) return;

        const content = document.getElementById('content_' + this.uniqueId).value;
        const author = document.getElementById('author_' + this.uniqueId).value;

        this.props.addResponse({content, author});
    }

    render() {
        return (
            <form className="reply" id={'newCommentForm_' + this.uniqueId}>
                <h2>Komentarze</h2>
                <h3>Dodaj nowy:</h3>
                <fieldset className="control">
                    <textarea id={'content_' + this.uniqueId} cols="30" rows="3"
                              placeholder="Treść komentarza..."
                              className="control__input reply__content" required></textarea>
                </fieldset>
                <fieldset className="control">
                    <input id={'author_' + this.uniqueId} type="text" placeholder="Podpis..."
                           className="control__input reply__author" required />

                    <div className="reply__btns">
                        <button type="reset"
                                className="reply__btns__remove"
                                onClick={this.props.hideResponse}>
                            Anuluj
                        </button>
                        <button type="button"
                                className="reply__btns__add"
                                onClick={this.addResponse.bind(this)}>
                            Dodaj
                        </button>
                    </div>
                </fieldset>
            </form>
        );
    }
}
