import React from 'react';
import moment from 'moment';

const anonymousImg = require('../../../images/anonymous.png');

import AddNew from './_addNew.component';
import LikesDislikes from './_likeDislike.component';

export default class Comment extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            responseVisible: false
        };
    }

    vote(vote) {
        if(!this.props.isSub) {
            this.props.votedHandler(this.props._id, vote);
        } else {
            this.props.votedHandler(this.props._id, vote, this.props.parentId);
        }
    }

    showAllResponses() {
        this.props.showMoreResponsesHandler(this.props._id);
    }

    generateResponses() {
        if(!this.props.responses) return null;

        const responses = this.props.responses.map(subresp => {
            return <Comment key={subresp._id}
                            parentId={this.props._id}
                            showParentResponse={this.showResponse.bind(this)}
                            votedHandler={this.props.votedHandler}
                            {...subresp} />
        });

        const nbOfVisibleResponses = this.props.responses.length;
        if(nbOfVisibleResponses !== this.props.allResponsesSize) {
            const showMoreResponsesBtn = (
                <button key={Math.floor(Math.random() * 10000)}
                        className="show-all-comments comment-btn-more"
                        onClick={this.showAllResponses.bind(this)}>
                    Wyświetl odpowiedzi
                </button>
            );

            responses.push(showMoreResponsesBtn);
        }

        return responses;
    }

    showResponse() {
        if(!this.props.isSub) { //parent - just show response cmp
            this.setState({responseVisible: true});
        } else {
            //we're in a child - show parent's response cmp
            this.props.showParentResponse();
        }
    }

    hideResponse() {
        this.setState({responseVisible: false});
    }

    addResponse(newComment) {
        this.props.commentAddedHandler(newComment, this.props.isSub ? null : this.props._id);
        this.hideResponse();
    }

    generateAddResponse() {
        if(this.state.responseVisible) {
            return <AddNew hideResponse={this.hideResponse.bind(this)}
                           addResponse={this.addResponse.bind(this)} />;
        } else {
            return null;
        }
    }

    render() {

        return (
            <div className="comment">
                <div className="avatar-section">
                    <img src={anonymousImg} className="avatar-section__img" />

                    <LikesDislikes likes={this.props.likes}
                                   vote={this.vote.bind(this)}
                                   userVote={this.props.userVote}
                                   dislikes={this.props.dislikes} />
                </div>
                <div className="main-section">
                    <div>
                        <strong>{this.props.author}</strong>,
                        <i>{' ' + moment(this.props.createdAt).fromNow()}</i>
                    </div>
                    <div className="comment-text">{this.props.content}</div>
                    <div className="reply-button-wrapper">
                        <button className="main-section__repl-btn comment-btn-more"
                                onClick={this.showResponse.bind(this)}>
                            Odpowiedz
                        </button>
                    </div>
                </div>
                <div className="subs">
                    {this.generateResponses()}

                    {this.generateAddResponse()}
                </div>
            </div>
        )
    }
}
