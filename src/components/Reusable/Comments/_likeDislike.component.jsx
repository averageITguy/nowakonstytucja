import React from 'react';

const like = require('../../../images/like.png');
const dislike = require('../../../images/dislike.png');
const like_disabled = require('../../../images/like_dis.png');
const dislike_disabled = require('../../../images/dislike_dis.png');

export default class extends React.Component {

    generateBtns() {
        if(typeof this.props.userVote === 'boolean' ) {
            return (
                <div className="likes-dislikes">
                    <img src={like_disabled}
                         className="like-dislike-btn"
                    />
                    <img src={dislike_disabled}
                         className="like-dislike-btn"
                    />
                </div>
            )
        } else {
            return (
                <div className="likes-dislikes">
                    <img src={like}
                         onClick={() => this.props.vote('like')}
                         className="like-dislike-btn"
                    />
                    <img src={dislike}
                         onClick={() => this.props.vote('dislike')}
                         className="like-dislike-btn"
                    />
                </div>
            )
        }
    }

    render() {
        return (
            <div className="likes-dislikes">
                {this.generateBtns()}

                <div className="likes-counter-wrapper">
                    <span className="likes">{this.props.likes}</span> - <span className="dislikes">{this.props.dislikes}</span>
                </div>

            </div>
        );
    }
}
