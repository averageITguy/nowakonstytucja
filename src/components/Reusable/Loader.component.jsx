import React from 'react';

const image  = require('../../images/loader.svg');

export default function Loader(props) {
    if(props.loaded) {
        return <div>{props.children}</div>;
    } else {
        return (<div id="loader-wrapper">
                <div className="table-row">
                    <div id="table-cell-loader">
                        <img src={image} />
                    </div>
                </div>
            </div>
        );
    }
}