require('styles/Footer.style.scss');

import React from 'react';

export default class FooterComponent extends React.Component {

    render() {
        return (
            <nav className="footer">
                <a className="terms" href="/regulamin">Regulamin</a>
                <a className="terms" href="/onas">O nas</a>
            </nav>
        );
    }
}
