require('styles/Filters.style.scss');

import React from 'react';

import {Row, Col} from 'react-flexbox-grid';

export default function FiltersComponent(props) {
    const {activeTab, tabClicked} = props;

    const isActive = tab => {
        return activeTab === tab ? 'filter active' : 'filter';
    };

    return (
        <Row>
            <Col xs={12} md={10} mdOffset={1}>
                <div className="filters">
                    <div className={isActive('popular')} onClick={() => tabClicked('popular')}>Popularne</div>
                    <div className={isActive('newest')} onClick={() => tabClicked('newest')}>Najnowsze</div>
                </div>
            </Col>
        </Row>
    );
}
