require('styles/Jumbotron.style.scss');
import React from 'react';
import { Row, Col, Grid } from 'react-flexbox-grid';

export default class JumbotronComponent extends React.Component {
	constructor(props) {
        super(props);
    }
	
	render(props) {
		return (
			<div className="col-xs-12 thin-jumbotron">
                <Grid fluid>
                    <Row>
                        <Col xs={12} md={10} mdOffset={1}>
							<h1>{this.props.header}</h1>
							<p>{this.props.hint}</p>
                        </Col>
                    </Row>
                </Grid>
			</div>
		);
	}
}