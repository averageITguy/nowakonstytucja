require('styles/Header.style.scss');

import React from 'react';

import {Link} from 'react-router';

import {Row, Col, Grid} from 'react-flexbox-grid';

import ToastMessages from './ToastMessages.component'

import LoggedInBtns from '../Auth/LoggedInBtns.component';

let logo = require('../../images/logo.png');

export default class MenuComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: null,
            isMobileMenuToggled: false,
            menuClasses: 'header-wrapper'
        };

        this.toggleMobileMenu = this.toggleMobileMenu.bind(this);
    }

    toggleMobileMenu() {
        if(this.state.isMobileMenuToggled) {
            this.setState({
                menuClasses: 'header-wrapper',
                isMobileMenuToggled: false
            });
        } else {
            this.setState({
                menuClasses: 'header-wrapper show-mobile-menu',
                isMobileMenuToggled: true
            });
        }
    }

    getLogInCmp() {
        if(this.state.user) {
            return <LoggedInBtns user={this.state.user} />;
        } else {
            return (
                <a className="rectangle-button">
                    <i className="icon icon-facebook"></i>Zaloguj się
                </a>);
        }
    }

    isRouteActive(pathSingle, pathMultiple, extra) {
        const path = location.pathname.split('/')[1];

        return pathSingle === path || pathMultiple === path || extra === path
            ? 'menu-element active' : 'menu-element';
    }

    render() {
        let logger = this.getLogInCmp();

        return (
            <Grid fluid className={this.state.menuClasses}>
                <Row>
                    <Col xs={12} lg={12}>
                        <Row>
                            <Col md={10} xs={12} mdOffset={1}>
                                <nav className="menu">
                                    <div onClick={this.toggleMobileMenu}
                                         className="mobile-menu-indicator">
                                         <div></div>
                                         <div></div>
                                         <div></div>
                                    </div>
                                    <Link className="menu-element menu-logo" to="/">
                                        <img src={logo} alt="logo" />
                                        <span>Nic o nas bez nas.</span>
                                    </Link>
                                    <div className="links">
                                        <Link onClick={this.toggleMobileMenu}
                                              className={this.isRouteActive('sondaz', 'sondaze')}
                                              to="/sondaze">
                                            Sondaże
                                        </Link>
                                        <Link onClick={this.toggleMobileMenu}
                                              className={this.isRouteActive('problem', 'problemy', 'rozwiazania') + ' constitution-menu-item'}
                                              to="/problemy">
                                            Pisz konstytucję!
                                            <div className="constitution-menu-item-info">
                                                Problemy i rozwiązania
                                            </div>
                                        </Link>
                                        <Link onClick={this.toggleMobileMenu}
                                              className={this.isRouteActive('ustawa', 'ustawy')}
                                              to="/ustawy">
                                            Oceń sejm
                                        </Link>
                                        <Link onClick={this.toggleMobileMenu}
                                              className={this.isRouteActive('petycje', 'petycja')}
                                              to="/petycje">
                                            Petycje
                                        </Link>
                                        <a href="http://wiadomosci.nowakonstytucja.org"
                                           className="menu-element">
                                            Wiadomości
                                        </a>
                                    </div>
                                    {/*<div onClick={this.toggleMobileMenu}*/}
                                         {/*className="menu-element login-button-wrapper">*/}
                                        {/*{logger}*/}
                                    {/*</div>*/}
                                </nav>
                            </Col>
                        </Row>
                        <ToastMessages />
                    </Col>
                </Row>
            </Grid>
        );
    }
}
