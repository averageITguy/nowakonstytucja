import React from 'react';
import Footer from './Footer.component';
import Header from './Header.component';

export default class MainLayout extends React.Component {
    render() {
        return (
            <div className="components-wrapper">
                <Header />
                {this.props.children}
                <Footer />
            </div>
        );
    }
}
