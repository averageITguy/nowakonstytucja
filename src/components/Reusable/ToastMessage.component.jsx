require('styles/ToastMessages.style.scss');
import React from 'react';

export default function ToastMessageComponent(props) {
    const {title, message, type} = props.toast;

    return (
        <div className="toast-message-wrapper">
            <div className={`toast-message ${type}`}>
                <h3>{title}</h3>
                <p>{message}</p>
            </div>
        </div>
    );

}