import React from 'react';

import ToastService from '../../services/toast.service';

import ToastMessage from './ToastMessage.component'

export default class ToastMessages extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            toasts: []
        };

        ToastService.registerDisplayer(this.toastAdded.bind(this));
    }

    toastAdded(toast) {
        this.state.toasts.push(toast);
        this.setState({toasts: this.state.toasts});

        setTimeout(()=> {
            this.state.toasts.shift();
            this.setState({toasts: this.state.toasts});
        }, 5000);
    }

    generateToats() {
        return this.state.toasts.map(toast => {
            return <ToastMessage key={toast.idx} toast={toast} />
        });
    }

    render() {
        return <div>{this.generateToats()}</div>;
    }
}