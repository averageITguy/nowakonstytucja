let displayToast = null;

class ToastService {
    registerDisplayer(displayFunction) {
        displayToast = displayFunction;
    }

    /***
     * @param {String} title
     * @param {String=} message
     * @param {String=} type
     ***/
    display(title, message, type = 'success') {
        const idx = Math.floor(Math.random() * 1000);

        displayToast({title, message, type, idx});
    }
}

export default new ToastService();