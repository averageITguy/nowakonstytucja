import BaseApiService from './base.service';
import {parseQueryString} from './common';

const apiUrl = '/api/polls';

class PollsService extends BaseApiService {
    constructor(apiUrl) {
        super(apiUrl);
    }

    getPolls(archived, filters, page) {
        let url = apiUrl + '?';

        if(archived) {
            url += 'archived=true&';
        }
        if(page) {
            url += `page=${page}&`;
        }
        if(filters) {
            url += parseQueryString(filters);
        }

        return super.get(null, url)
            .then(prepareUserVoteAndCalcNoVotesCtr);
    }

    getPollsForExchange() {
        const url = `${apiUrl}/exchange`;

        return super.get(null, url);
    }

    getPoll(id) {
        return super.get(id)
            .then(prepareUserVoteAndCalcNoVotesCtr);
    }

    exchange(pollId, proposal) {
        const url = apiUrl + '/exchange/' + (pollId || '');

        return super.post(proposal, url);
    }

    vote(pollId, voteObj) {
        const url = `${apiUrl}/vote/${pollId}`;

        return super.post(voteObj, url);
    }
}

export default new PollsService(apiUrl);

///helpers

const prepareUserVoteAndCalcNoVotesCtr = polls => {
    const _prepareUserVoteAndCalcNoVotesCtr = poll => {
        if(userVoted(poll)) {
            poll.userVote = poll.votes[0].value
        }

        poll.noVotesCtr = poll.votesCtr - poll.yesVotesCtr;
        delete poll.votes;
    };

    if(Array.isArray(polls)) {
        polls.forEach(poll => {
            _prepareUserVoteAndCalcNoVotesCtr(poll);
        });
    } else {
        _prepareUserVoteAndCalcNoVotesCtr(polls); //show poll
    }

    return polls;
};

const userVoted = poll => {
    return poll.votes && poll.votes.length > 0;
};
