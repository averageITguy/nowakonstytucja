import BaseApiService from './base.service';

const apiUrl = '/api/comment';

class CommentService extends BaseApiService {
    constructor(apiUrl) {
        super(apiUrl);
    }

    addComment(comment, entityId) {
        return super.post(comment, apiUrl + '/' + entityId)
            .then(prepareUserVoteAndCalcNoVotesCtr)
            .then(markSubcomments);
    }

    getComments(entityId, sort, page, single) {
        const url = apiUrl + `/${entityId}?sort=${sort}&page=${page}&single=${single}`;

        return super.get(null, url)
            .then(prepareUserVoteAndCalcNoVotesCtr)
            .then(markSubcomments);
    }

    showMoreResponses(parentId, entityId) {
        const url = apiUrl + `/responses/${entityId}/${parentId}`;

        return super.get(null, url)
            .then(prepareUserVoteAndCalcNoVotesCtr)
            .then(markSubcomments);
    }

    vote(parentId, voteObj, subCommentId) {
        const url = `${apiUrl}/${parentId}/vote/${subCommentId}`;

        return super.post(voteObj, url);
    }
}

export default new CommentService(apiUrl);


const prepareUserVoteAndCalcNoVotesCtr = result => {
    if(Array.isArray(result)) {
        result.forEach(_result => {
            prepareUserVoteAndCalcNoVotesCtr(_result);
        });
    } else {
        if(userVoted(result)) {
            result.userVote = result.votes[0].value
        }

        result.dislikes = result.votesCtr - result.yesVotesCtr;
        result.likes = result.yesVotesCtr;

        delete result.votes;
        delete result.yesVotesCtr;
        delete result.votesCtr;

        if(result.responses) {
            prepareUserVoteAndCalcNoVotesCtr(result.responses)
        }
    }

    return result;
};

const userVoted = result => {
    return result.votes && result.votes.length > 0;
};

const markSubcomments = response => {
    if(Array.isArray(response)) {
        response.forEach(_response => {
            markSubcomments(_response);
        });
    } else {
        if(Array.isArray(response.responses)) {
            response.responses.forEach(subcomment => {
                markSubcomments(subcomment);
            });
        }

        response.isSub = !Object.hasOwnProperty.call(response, 'entityId');
    }

    return response;
};
