import BaseApiService from './base.service';

const apiUrl = '/api/parliament';

class ParliamentService extends BaseApiService {
    constructor(apiUrl) {
        super(apiUrl);
    }

    getBills(sort, page) {
        let url = apiUrl + '?';

        if(sort === 'newest') {
            url += 'sort=newest&';
        }

        url += `page=${page}&`;

        return super.get(null, url)
            .then(bills => {
                bills.forEach(bill => {
                    if(bill.pdfFile) {
                        bill.pdfFileUrl = '/' + bill.pdfFile.replace('uploads', 'files');
                    }
                });

                return bills;
            })
            .then(prepareUserVoteAndCalcNoVotesCtr);
    }

    getBill(id) {
        return super.get(id)
            .then(bill => {
                if(bill.pdfFile) {
                    bill.pdfFileUrl = '/' + bill.pdfFile.replace('uploads', 'files');
                }
                return bill;
            })
            .then(prepareUserVoteAndCalcNoVotesCtr);
    }

    vote(billId, voteObj) {
        const url = `${apiUrl}/vote/${billId}`;

        return super.post(voteObj, url);
    }
}

export default new ParliamentService(apiUrl);

//TODO: do osobnego serwisu
const prepareUserVoteAndCalcNoVotesCtr = results => {
    const _prepareUserVoteAndCalcNoVotesCtr = result => {
        if(userVoted(result)) {
            result.userVote = result.votes[0].value
        }

        result.noVotesCtr = result.votesCtr - result.yesVotesCtr;
        delete result.votes;
    };

    if(Array.isArray(results)) {
        results.forEach(result => {
            _prepareUserVoteAndCalcNoVotesCtr(result);
        });
    } else {
        _prepareUserVoteAndCalcNoVotesCtr(results); //show result
    }

    return results;
};

const userVoted = result => {
    return result.votes && result.votes.length > 0;
};