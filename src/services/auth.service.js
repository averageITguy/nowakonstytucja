import global from './global.service';
import {fetchJSON} from './common';

import _ from 'lodash';

//ID of the user who has created account in the app
let accountId = null;

const authSubscribers = []; //TODO: do wyjebania jak moderator zrobiony
let user = null; //TODO: do wyjebania jak moderator zrobiony

class AuthService {
    init() {
        //Check if user has active session (user has account in the app)
        //TODO:

        //if not, treat him as anonymous user
        return this.initAnonymousUser();
    }

    /**
     * Returns either anonymous ID assigned to this client
     * or account ID if user has logged in
     */
    getUserId() {
        if(accountId) {
            return accountId;
        } else {
            return localStorage.getItem('nk_anonymousClientID');
        }
    }

    logOut() {
        return fetch('/logout', {credentials: 'include'}).then(() => {
            user = null;
            notifySubscribers();
        });
    }

    clear() {
        localStorage.removeItem('nk_anonymousClientID');
    }

    /**
     * @private
     */
    initAnonymousUser() {
        //1) Check local storage
        const nk_anonymousClientID = localStorage.getItem('nk_anonymousClientID');

        //if no ID is stored in the browser then check database for this client
        if(!nk_anonymousClientID || nk_anonymousClientID === 'undefined') {
            return this.checkDBForThisClient();
        } else {
            return Promise.resolve();
        }
    }

    /**
     * @private
     */
    checkDBForThisClient() {
        const clientCode = createClientCode();
        return fetchJSON('/auth/getClient/' + encodeURIComponent(clientCode), {credentials: 'include'})
            .then(({_id}) => {
                localStorage.setItem('nk_anonymousClientID', _id);
            });
    }

    //TEMP - DO CZASU ZALATWIENIA LEPIEJ MODERATORA
    getUserFromDb(callback) {
        if(_.isFunction(callback)) authSubscribers.push(callback);

        return fetchJSON('/auth/user', {credentials: 'include'}).then(_user => {
            user = _user;

            notifySubscribers();

            return user;
        }).catch((e) => {
            console.log(e);

            return user;
        });
    }

    openFacebookLoginForm(callback) {
        if(user) {//user already logged in
            notifySubscribers(); //TODO: not sure ...

            if(_.isFunction(callback)) return callback(user);
            else return;
        }

        const win = window.open('/auth/facebook/callback', 'login', 'width=1100,height=500');

        let timer = setInterval(() => {
            if(!win) return clearInterval(timer);

            if(win.closed) {
                clearInterval(timer);

                this.getUserFromDb().then((dbUser) => {

                    if(_.isFunction(callback) && dbUser) callback(dbUser);
                });
            }
        }, 500);
    }

    subscribeForUser(callback) {
        if(_.isFunction(callback)) authSubscribers.push(callback);
    }

    unsubscribe(callback) {
        var idx = authSubscribers.indexOf(callback);

        if(idx !== -1) authSubscribers.splice(idx, 1);
    }


////////////////////
}

export default new AuthService();

function createClientCode() {
    let plugins = '';
    try {
        plugins = global.parseToArray(navigator.plugins).map(p => p.filename).join('');
    } catch(e) {
        console.info('navigator.plugins not supported');
    }

    return (navigator.userAgent + navigator.language + navigator.platform + plugins).replace(/ /g, '');
}


////do wyjebanie jak moderator zrobiony
function notifySubscribers() {
    authSubscribers.forEach(callback => {
        callback(user);
    });
};
///////