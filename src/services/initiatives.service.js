import BaseApiService from './base.service';

const apiUrl = '/api/initiatives';

class InitiativesService extends BaseApiService {
    constructor(apiUrl) {
        super(apiUrl);
    }

    getInitiatives(sort, page) {
        let url = apiUrl + '?';

        if(sort === 'newest') {
            url += 'sort=newest&';
        }

        url += `page=${page}&`;

        return super.get(null, url);
    }

    getInitiative(id) {
        return super.get(id);
    }

    sign(initiativeId) {
        const url = `${apiUrl}/sign/${initiativeId}`;

        return super.post({}, url);
    }
}

export default new InitiativesService(apiUrl);
