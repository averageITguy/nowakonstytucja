import BaseApiService from './base.service';

const apiUrl = '/api/pollsproposals';

class PollsService extends BaseApiService {
    constructor(apiUrl) {
        super(apiUrl);
    }

    proposePoll(data) {
        return super.post(data);
    }

    getPollsProposals() {
        return super.get();
    }

    getProposal(id) {
        return super.get(id);
    }

    markAsInteresting(proposalId, interesting) {
        const overrideUrl = `${apiUrl}/interesting/${proposalId}`;

        return super.post({interesting}, overrideUrl);
    }

    rejectProposal(proposalId, interesting) {
        const overrideUrl = `${apiUrl}/reject/${proposalId}`;

        return super.post({}, overrideUrl);
    }
}

export default new PollsService(apiUrl);
