import moment from 'moment';

moment.updateLocale('en', {
    relativeTime: {
        past: '%s temu',
        s: 'kilka sek',
        m: 'minutę',
        mm: '%d min',
        h: 'godzinę',
        hh: '%d godz',
        d: 'dzień',
        dd: '%d dni',
        M: 'miesiąc',
        MM: '%d mies',
        y: 'rok',
        yy: '%d lat'
    }
});
