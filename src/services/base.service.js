import {fetchJSON} from './common';

export default class BaseApiService {
    constructor(apiUrl) {
        this.apiUrl = apiUrl;
    }

    get(id, overrideUrl) {
        const url = overrideUrl || (id ? this.apiUrl + '/' + id : this.apiUrl);
        return fetchJSON(url, {credentials: 'include'});
    }

    post(data, overrideUrl) {
        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            credentials: 'include',
            body: JSON.stringify(data)
        };
        return fetchJSON(overrideUrl || this.apiUrl, options);
    }

    postWithFile(formData) {
        const options = {
            method: 'POST',
            credentials: 'include',
            body: formData
        };
        return fetchJSON(this.apiUrl, options);
    }
}