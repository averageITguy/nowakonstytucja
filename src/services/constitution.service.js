import BaseApiService from './base.service';

import {parseQueryString} from './common';

const apiUrl = '/api/problem';

class ConstitutionService extends BaseApiService {
    constructor(apiUrl) {
        super(apiUrl);
    }

    getProblems(sort, filters, page) {
        let url = apiUrl + '?';

        if(sort === 'newest') {
            url += 'sort=newest&';
        }

        url += `page=${page}&`;

        if(filters) {
            url += parseQueryString(filters);
        }

        return super.get(null, url);
    }

    getSwiperData(category) {
        const overrideUrl = apiUrl + '/swiper/' + category;

        return super.get(null, overrideUrl);
    }

    getProblem(id) {
        return super.get(id)
            .then(problem => {
                if(problem.solutions) {
                    problem.solutions = prepareUserVoteAndCalcNoVotesCtr(problem.solutions);
                }

                return problem;
            });
    }

    getSolutionsForProblem(problemId, page) {
        const url = `${apiUrl}/${problemId}/solution?page=${page}`;

        return super.get(null, url)
            .then(solutions => {
                if(solutions && solutions.length > 0) {
                    solutions.forEach(solution => {
                        solution.createdBy = solution.createdBy[0];
                    });

                    return prepareUserVoteAndCalcNoVotesCtr(solutions);
                }
            });
    }

    getCategories() {
        return super.get('categories').then(res => res.categories);
    }

    signProblem(problemId) {
        const url = `${apiUrl}/sign/${problemId}`;

        return super.post({}, url);
    }

    getSolution(solutionId, problemId) {
        const url = `${apiUrl}/${problemId}/solution/${solutionId}`;

        return super.get(null, url)
            .then(prepareUserVoteAndCalcNoVotesCtr);
    }

    voteForSolution(problemId, solutionId, voteObj) {
        const url = `${apiUrl}/${problemId}/vote/${solutionId}`;

        return super.post(voteObj, url);
    }

    getSignsCounterMsg(signsCount, userVoted) {
        if(signsCount === 0) {
            return 'Nikt jeszcze nie uznał tego problemu jako ważny - bądź pierwszy!';
        } else if(signsCount === 1) {
            if(userVoted) return 'Narazie tylko Ty uznałeś ten problem jako ważny - brawo, ktoś musi zacząć!';
            else return 'Do tej pory tylko jeden użytkownik uznał ten problem jako ważny';
        } else {
            if(userVoted) {
                const counterWithoutUsersSign = signsCount - 1;

                if(counterWithoutUsersSign === 1) {
                    return `Ty i jeden użytkownik uznaliście ten problem jako ważny`;
                } else {
                    return `Ty i ${counterWithoutUsersSign} użytkowników uznaliście ten problem jako ważny`;
                }
            }
            else return `${signsCount} użytkowników uznało ten problem jako ważny`;
        }
    }
}

export default new ConstitutionService(apiUrl);

///

//TODO: do osobnego serwisu
const prepareUserVoteAndCalcNoVotesCtr = results => {
    const _prepareUserVoteAndCalcNoVotesCtr = result => {
        if(userVoted(result)) {
            result.userVote = result.votes[0].value
        }

        result.noVotesCtr = result.votesCtr - result.yesVotesCtr;
        delete result.votes;
    };

    if(Array.isArray(results)) {
        results.forEach(result => {
            _prepareUserVoteAndCalcNoVotesCtr(result);
        });
    } else {
        _prepareUserVoteAndCalcNoVotesCtr(results); //show result
    }

    return results;
};

const userVoted = result => {
    return result.votes && result.votes.length > 0;
};