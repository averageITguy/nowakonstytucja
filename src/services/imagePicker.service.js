import BaseApiService from './base.service';

const apiUrl = '/api/imagePicker';

class ImagePickerService extends BaseApiService {
    constructor(apiUrl) {
        super(apiUrl);
    }

    getImages(type) {
        return super.get(type);
    }

    addImage(entity, id, type, image) {
        let url = getEntityEndpoint(entity);

        url += '/addImage/' + id + '/' + type + '/' + image;

        return super.post({}, url);
    }

}

export default new ImagePickerService(apiUrl);

const getEntityEndpoint = entity => {
    switch(entity) {
        case 'poll':
            return '/api/pollsproposals';
        case 'parliament':
            return '/api/parliamentproposals';
        case 'initiative':
            return '/api/initiativesproposals';
    }
};