import auth from './auth.service';

export function fetchJSON(input, init) {
    return fetch(input, init)
        .then(checkStatus)
        .then(parseJSON);
}

function checkStatus(response) {
    if(response.status >= 200 && response.status < 300) {
        return response
    } else {
        if(response.status === 401) { //In case user deleted cookie - check him automatically again
            auth.clear();
            auth.init();
        }

        var error = new Error(response.statusText);
        error.response = response;
        throw error
    }
}

function parseJSON(response) {
    return response.json().catch(err => {
        if(err.message === 'Unexpected end of JSON input') return;

        throw err;
    });
}

export const parseQueryString = function(obj, prefix) {
    var str = [], p;
    for(p in obj) {
        if(obj.hasOwnProperty(p)) {
            var k = prefix ? prefix + '[' + p + ']' : p, v = obj[p];

            if(v !== null && typeof v === 'object') {
                str.push(parseQueryString(v, k));
            } else {
                if(v) {
                    str.push(encodeURIComponent(k) + '=' + encodeURIComponent(v));
                }
            }
        }
    }
    return str.join('&');
};
