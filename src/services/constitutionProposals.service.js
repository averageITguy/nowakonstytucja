import BaseApiService from './base.service';

const apiUrl = '/api/constitutionProposals';

class ConstitutionService extends BaseApiService {
    constructor(apiUrl) {
        super(apiUrl);
    }

    getProblemProposals() {
        const overrideUrl = apiUrl + '/problem';

        return super.get(null, overrideUrl);
    }

    proposeProblem(data) {
        const overrideUrl = apiUrl + '/problem';

        return super.post(data, overrideUrl);
    }

    getSolutionProposals() {
        const overrideUrl = apiUrl + '/solution';

        return super.get(null, overrideUrl);
    }

    proposeSolution(data) {
        const url = `${apiUrl}/solution`;

        return super.post(data, url);
    }

    acceptProposal(type, proposal) {
        const overrideUrl = `${apiUrl}/${type}/accept`;

        return super.post(proposal, overrideUrl);
    }

    markAsInteresting(type, proposalId, interesting) {
        const overrideUrl = `${apiUrl}/${type}/interesting/${proposalId}`;

        return super.post({interesting}, overrideUrl);
    }

    rejectProposal(type, proposalId) {
        const overrideUrl = `${apiUrl}/${type}/reject/${proposalId}`;

        return super.post({}, overrideUrl);
    }
}

export default new ConstitutionService(apiUrl);
