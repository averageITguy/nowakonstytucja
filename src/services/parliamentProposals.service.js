import BaseApiService from './base.service';

const apiUrl = '/api/parliamentproposals';

class InitiativesService extends BaseApiService {
    constructor(apiUrl) {
        super(apiUrl);
    }

    getProposals() {
        return super.get().then(proposals => {
            proposals.forEach(proposal => {
                if(proposal.pdfFile) {
                    proposal.pdfFileUrl = '/' + proposal.pdfFile.replace('uploads', 'files');
                }
            });

            return proposals;
        });
    }

    proposeBill(formData) {
        return super.postWithFile(formData);
    }

    acceptProposal(proposal) {
        const overrideUrl = `${apiUrl}/accept`;

        return super.post(proposal, overrideUrl);
    }

    markAsInteresting(proposalId, interesting) {
        const overrideUrl = `${apiUrl}/interesting/${proposalId}`;

        return super.post({interesting}, overrideUrl);
    }

    rejectProposal(proposalId) {
        const overrideUrl = `${apiUrl}/reject/${proposalId}`;

        return super.post({}, overrideUrl);
    }
}

export default new InitiativesService(apiUrl);
