'use strict';

const path = require('path');

//export configuration specific for current environment
const webpackEnvCfg = path.join(__dirname, 'cfg/' + process.env.NODE_ENV);
console.log(webpackEnvCfg);

module.exports = require(webpackEnvCfg);
