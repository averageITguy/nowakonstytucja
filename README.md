# ReadME #

## Start project: ##
npm install
mongod
npm start

## Webpack compile: ##
NODE_ENV=development webpack

## Collection cleaning: ##
1. type mongo
2. use constitution
3. e.g. for polls -> db.polls.remove({})

## Used Adobe Stock photo numbers ##
* 5112308
* 91477772
* 91794420

## Collections: ##
![collections.png](https://bitbucket.org/repo/daa6dp4/images/193478556-collections.png)

##joining article and entity
1. type mongo
2. use constitution
3. e.g. for polls -> db.polls.update(_id: "idFromNKUrl", {$set: {article: {url: "articleId", title: "articleTitle"}}}, {"multi" : false, "upsert" : false})



db.users.update({'facebook.name': 'Marcin Trybulec'}, {$set: {moderator: true}})