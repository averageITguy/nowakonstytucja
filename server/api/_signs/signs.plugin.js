const mongoose = require('mongoose'),
    ObjectId = mongoose.Schema.Types.ObjectId;

module.exports = function(schema) {
    schema.add({
        signsCtr: {
            type: Number,
            default: 0
        },
        signs: {
            type: [ObjectId],
            default: []
        }
    });
};