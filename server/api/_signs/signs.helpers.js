const apiHelpers = require('../api.helpers');

const api = {
    sign(req, res, collection) {
        collection.find({_id: req.params.id, signs: req.signedCookies.anonymousID}, '_id')
            .then(result => {
                if(result.length > 0) {
                    return res.send({
                        uVoted: true
                    });
                } else {
                    collection.update({_id: req.params.id}, {
                        '$push': {signs: req.signedCookies.anonymousID},
                        '$inc': {signsCtr: 1}
                    }).then(() => {
                        res.end();
                    }).catch(apiHelpers.handleApiError(res));
                }
            });
    }
};

module.exports = {
    api
};