const mongoose = require('mongoose');

module.exports = function(schema) {
    schema.add({
        createdBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        }
    })
};