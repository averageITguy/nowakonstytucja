const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const ArticleSchema = new Schema({
    title: String,
    url: String
});

module.exports = function(schema) {
    schema.add({
        'article': ArticleSchema
    });
};