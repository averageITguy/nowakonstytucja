const _ = require('lodash');
const apiHelpers = require('../api.helpers');
const ERROR_MESSAGES = require('../../config/globals').ERROR_MESSAGES.votes;

const mongoose = require('mongoose');

const api = {
    vote(req, res, collection) {
        const pollId = mongoose.Types.ObjectId(req.params.id);

        const filters = {
            '$match': {
                '_id': pollId
            }
        };

        const fields = {
            '$project': {
                'votes': 1
            }
        };

        const returnOnlyUserVote = {
            $redact: {
                $cond: {
                    if: {
                        $or: [
                            {
                                $eq: ['$userId', req.signedCookies.anonymousID]
                            },
                            {
                                $not: ['$userId']
                            }
                        ]
                    },
                    then: '$$DESCEND',
                    else: '$$PRUNE'
                }
            }
        };

        collection.aggregate([filters, fields, returnOnlyUserVote]).then(([poll]) => {
            const userAlreadyVoted = poll.votes.length > 0;

            if(userAlreadyVoted) {
                res.send({
                    err: ERROR_MESSAGES.ALREADY_VOTED,
                    data: poll.votes[0].value
                });
            } else {
                const vote = {
                    userId: req.signedCookies.anonymousID,
                    value: req.body.value
                };

                const update = {
                    $push: {votes: vote},
                    $inc: {votesCtr: 1}
                };

                if(vote.value) { //yes-vote
                    update.$inc.yesVotesCtr = 1;
                }

                collection.update({_id: pollId}, update).then(() => {
                    res.status(200).end();
                });
            }
        }).catch(apiHelpers.handleApiError(res));
    }
};


module.exports = {
    api
};