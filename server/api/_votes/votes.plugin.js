'use strict';

const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;

const VotesSchema = new Schema({
    userId: {
        type: ObjectId,
        required: true
    },
    value: {
        type: Boolean,
        required: true
    }
});

module.exports = function(schema) {
    schema.add({
        yesVotesCtr: {
            type: Number,
            default: 0
        },
        votesCtr: {
            type: Number,
            default: 0
        },
        votes: {
            type: [VotesSchema],
            default: []
        }
    });
};