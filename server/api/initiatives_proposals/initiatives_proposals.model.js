const mongoose = require('mongoose');

const createdByPlugin = require('../_createdBy.plugin');
const imagesPlugin = require('../_image/image.plugin');

const Schema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        maxlength: 200
    },
    descShort: {
        type: String,
        required: true,
        maxlength: 200
    },
    descDetailed: {
        type: String,
        maxlength: 620
    },
    interesting: {
        type: Boolean,
        default: false
    },
    rejected: {
        type: Boolean,
        default: false
    }
}, {timestamps: true});

Schema.plugin(createdByPlugin);
Schema.plugin(imagesPlugin, true);

Schema.virtual('signsCounter').get(function() {
    return Array.isArray(this.signs) ? this.signs.length : 0;
});

module.exports = mongoose.model('initiativeProposals', Schema);
