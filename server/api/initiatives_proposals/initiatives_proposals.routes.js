const express = require('express');
const multer = require('multer');
const upload = multer();

const onlyModerator = require('../../auth/auth.routes').onlyModerator;

const ctrl = require('./initiatives_proposals.controller.js');

const route = express.Router();

/**********************************/
/************** LIST **************/
/**********************************/
route.get('/', onlyModerator, ctrl.list);

/**********************************/
/************* CREATE *************/
/**********************************/
route.post('/', upload.none(), ctrl.create);

/**********************************/
/************* ACCEPT *************/
/**********************************/
route.post('/accept', onlyModerator, ctrl.accept);

/**********************************/
/********** INTERESTING ***********/
/**********************************/
route.post('/interesting/:proposalId', onlyModerator, ctrl.markAsInteresting);

/**********************************/
/************* REJECT *************/
/**********************************/
route.post('/reject/:proposalId', onlyModerator, ctrl.reject);

/**********************************/
/*********** ADD IMAGE ************/
/**********************************/
route.post('/addImage/:id/:type/:image', onlyModerator, ctrl.addImage);

module.exports = route;
