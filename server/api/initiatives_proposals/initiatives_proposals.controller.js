const InitiativesProposals = require('./initiatives_proposals.model');
const Initiatives = require('../initiatives/initiatives.model');

const apiHelpers = require('../api.helpers');
const imageHelpers = require('../_image/image.helpers');


/**********************************/
/************** LIST **************/
/**********************************/
const list = (req, res) => {
    InitiativesProposals.find({rejected: false}).lean()
        .then(results => {
            res.send(results);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************* CREATE *************/
/**********************************/
const create = (req, res) => {
    const newInitiativeProposal = req.body;

    newInitiativeProposal.createdBy = req.signedCookies.anonymousID;
    InitiativesProposals.create(newInitiativeProposal)
        .then(dbNewInitiativeProposal => {
            return res.status(201).json(dbNewInitiativeProposal.id);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************* ACCEPT *************/
/**********************************/
const accept = (req, res) => {
    const proposal = req.body;

    Initiatives.create({
        title: proposal.title,
        descShort: proposal.descShort,
        descDetailed: proposal.descDetailed,
        createdBy: proposal.createdBy,
        '200x150': proposal['200x150'],
        '300x240': proposal['300x240']
    }).then(() => {
        InitiativesProposals.findByIdAndRemove(proposal._id).then(() => {
            res.end();
        });
    }).catch(apiHelpers.handleApiError(res));
};


/**********************************/
/********** INTERESTING ***********/
/**********************************/
const markAsInteresting = (req, res) => {
    InitiativesProposals.findByIdAndUpdate(req.params.proposalId, {interesting: req.body.interesting})
        .then(() => {
            res.end();
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************* REJECT *************/
/**********************************/
const reject = (req, res) => {
    InitiativesProposals.findByIdAndUpdate(req.params.proposalId, {rejected: true})
        .then(() => {
            res.end();
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/*********** ADD IMAGE ************/
/**********************************/
const addImage = (req, res) => {
    return imageHelpers.api.addImage(req, res, InitiativesProposals);
};


module.exports = {
    list,
    create,
    accept,
    markAsInteresting,
    reject,
    addImage
};