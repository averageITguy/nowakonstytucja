const handleApiError = response => {
    return err => {
        response.status(500).send(err);
    };
};

const checkUser = (req, res) => {
    if(!req.signedCookies.anonymousID) {
        res.status(401).send();
    }

    return !!req.signedCookies.anonymousID;
};

module.exports = {
    handleApiError,
    checkUser
};