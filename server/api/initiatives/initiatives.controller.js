const mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId;

const Initiatives = require('./initiatives.model');

const apiHelpers = require('../api.helpers');
const signsHelpers = require('../_signs/signs.helpers');


/**********************************/
/************** LIST **************/
/**********************************/
const list = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    //pagination
    const page = req.query.page || 1;
    const pageSize = 4;
    const skip = {
        $skip: (page - 1 ) * pageSize
    };
    const limit = {$limit: pageSize};

    let sort = null;
    if(req.query.sort === 'newest') {
        sort = {$sort: {createdAt: -1}};
    } else {
        sort = {$sort: {signsCtr: -1, _id: 1}};
    }

    const fields = {
        '$project': {
            'title': 1,
            'descShort': 1,
            'signsCtr': 1,
            '200x150': 1,
            'userSigned': {$setIsSubset: [[ObjectId(req.signedCookies.anonymousID)], '$signs']}
        }
    };


    Initiatives.aggregate([sort, skip, limit, fields])
        .then(results => {
            res.send(results);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************** SHOW **************/
/**********************************/
const show = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    const filters = {
        '$match': {
            '_id': mongoose.Types.ObjectId(req.params.id)
        }
    };

    const fields = {
        '$project': {
            'title': 1,
            'article': 1,
            'descShort': 1,
            'descDetailed': 1,
            'signsCtr': 1,
            '300x240': 1,
            'userSigned': {$setIsSubset: [[ObjectId(req.signedCookies.anonymousID)], '$signs']}
        }
    };

    Initiatives.aggregate([filters, fields])
        .then(([result]) => {
            res.send(result);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************* SIGN ***************/
/**********************************/
const sign = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    signsHelpers.api.sign(req, res, Initiatives);
};


module.exports = {
    list,
    show,
    sign
};