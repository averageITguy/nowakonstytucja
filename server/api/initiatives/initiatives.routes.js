const express = require('express');

const InitiativesCtrl = require('./initiatives.controller');

const route = express.Router();

/**********************************/
/************** LIST **************/
/**********************************/
route.get('/', InitiativesCtrl.list);

/**********************************/
/************** SHOW **************/
/**********************************/
route.get('/:id', InitiativesCtrl.show);

/**********************************/
/************* SIGN ***************/
/**********************************/
route.post('/sign/:id', InitiativesCtrl.sign);


module.exports = route;
