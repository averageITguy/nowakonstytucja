const mongoose = require('mongoose');

const createdByPlugin = require('../_createdBy.plugin');
const signsPlugin = require('../_signs/signs.plugin');
const imagesPlugin = require('../_image/image.plugin');
const articlePlugin = require('../_article/article.plugin');

const Schema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        maxlength: 200
    },
    descShort: {
        type: String,
        required: true,
        maxlength: 200
    },
    descDetailed: {
        type: String,
        maxlength: 620
    }
}, { timestamps: true });

Schema.plugin(createdByPlugin);
Schema.plugin(signsPlugin);
Schema.plugin(imagesPlugin);
Schema.plugin(articlePlugin);

module.exports = mongoose.model('initiative', Schema);
