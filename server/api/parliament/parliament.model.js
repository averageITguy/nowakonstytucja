const mongoose = require('mongoose');

const createdByPlugin = require('../_createdBy.plugin');
const votesPlugin = require('../_votes/votes.plugin.js');
const imagesPlugin = require('../_image/image.plugin');
const articlePlugin = require('../_article/article.plugin');

const Schema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        maxlength: 350
    },
    printNb: {
        type: Number,
        required: true
    },
    descDetailed: {
        type: String,
        maxlength: 620
    },
    legislationUrl: String,
    pdfFile: String
}, { timestamps: true });

Schema.plugin(createdByPlugin);
Schema.plugin(votesPlugin);
Schema.plugin(imagesPlugin);
Schema.plugin(articlePlugin);

module.exports = mongoose.model('bill', Schema);
