const express = require('express');

const ctrl = require('./parliament.controller.js');

const route = express.Router();

/**********************************/
/************** LIST **************/
/**********************************/
route.get('/', ctrl.list);

/**********************************/
/************** SHOW **************/
/**********************************/
route.get('/:id', ctrl.show);

/**********************************/
/************** VOTE **************/
/**********************************/
route.post('/vote/:id', ctrl.vote);

module.exports = route;
