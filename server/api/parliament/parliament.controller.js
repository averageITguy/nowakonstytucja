'use strict';

const mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId;

const Parliament = require('./parliament.model.js');

const votesHelpers = require('../_votes/votes.helpers');
const apiHelpers = require('../api.helpers');


/**********************************/
/************** LIST **************/
/**********************************/
const list = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    //pagination
    const page = req.query.page || 1;
    const pageSize = 4;
    const skip = {
        $skip: (page - 1 ) * pageSize
    };
    const limit = {$limit: pageSize};


    let sort = null;

    if(req.query.sort === 'newest') {
        sort = {$sort: {createdAt: -1}};
    } else {
        sort = {$sort: {votesCtr: -1, _id: 1}};
    }

    const returnOnlyUserVote = {
        $redact: {
            $cond: {
                if: {
                    $or: [
                        {$eq: ['$userId', ObjectId(req.signedCookies.anonymousID)]},
                        {$not: ['$userId']}
                    ]
                },
                then: '$$DESCEND',
                else: '$$PRUNE'
            }
        }
    };

    const removeUnnecessaryFields = {
        $project: {
            'yesVotesCtr': 1,
            'votesCtr': 1,
            'title': 1,
            'printNb': 1,
            'pdfFile': 1,
            '200x150': 1,
            'votes': 1
        }
    };

    Parliament.aggregate([sort, skip, limit, returnOnlyUserVote, removeUnnecessaryFields])
        .then(results => {
            res.send(results);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************** SHOW **************/
/**********************************/
const show = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    const filters = {
        '$match': {
            '_id': mongoose.Types.ObjectId(req.params.id)
        }
    };

    const returnOnlyUserVote = {
        $redact: {
            $cond: {
                if: {
                    $or: [
                        {
                            $eq: ['$userId', ObjectId(req.signedCookies.anonymousID)]
                        },
                        {
                            $not: ['$userId']
                        }
                    ]
                },
                then: '$$DESCEND',
                else: '$$PRUNE'
            }
        }
    };

    const removeUnnecessaryFields = {
        $project: {
            'yesVotesCtr': 1,
            'votesCtr': 1,
            'article': 1,
            'title': 1,
            'printNb': 1,
            'descDetailed': 1,
            'legislationUrl': 1,
            'pdfFile': 1,
            '300x240': 1,
            'votes': 1
        }
    };

    Parliament.aggregate([filters, returnOnlyUserVote, removeUnnecessaryFields])
        .then(([result]) => {
            res.send(result);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************** VOTE **************/
/**********************************/
const vote = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    return votesHelpers.api.vote(req, res, Parliament);
};


module.exports = {
    list,
    show,
    vote
};