const ParliamentProposal = require('./parliament_proposals.model.js');
const Parliament = require('../parliament/parliament.model');

const apiHelpers = require('../api.helpers');
const imageHelpers = require('../_image/image.helpers');


/**********************************/
/************** LIST **************/
/**********************************/
const list = (req, res) => {
    const sort = {createdAt: -1};

    ParliamentProposal.find({}).lean().sort(sort)
        .then(results => {
            res.send(results);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************* CREATE *************/
/**********************************/
const create = (req, res) => {
    const newBill = req.body;

    if(req.file) {
        newBill.pdfFile = req.file.path;
    }

    newBill.legislationUrl = `http://www.sejm.gov.pl/Sejm8.nsf/PrzebiegProc.xsp?nr=${newBill.printNb}`;
    newBill.createdBy = req.signedCookies.anonymousID;

    ParliamentProposal.create(newBill)
        .then(dbNewBill => {
            return res.status(201).json(dbNewBill.id);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************* ACCEPT *************/
/**********************************/
const accept = (req, res) => {
    const proposal = req.body;

    Parliament.create({
        title: proposal.title,
        printNb: proposal.printNb,
        descDetailed: proposal.descDetailed,
        pdfFile: proposal.pdfFile,
        legislationUrl: proposal.legislationUrl,
        createdBy: proposal.createdBy,
        '200x150': proposal['200x150'],
        '300x240': proposal['300x240']
    }).then(() => {
        ParliamentProposal.findByIdAndRemove(proposal._id).then(() => {
            res.end();
        });
    }).catch(apiHelpers.handleApiError(res));
};


/**********************************/
/********** INTERESTING ***********/
/**********************************/
const markAsInteresting = (req, res) => {
    ParliamentProposal.findByIdAndUpdate(req.params.proposalId, {interesting: req.body.interesting})
        .then(() => {
            res.end();
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************* REJECT *************/
/**********************************/
const reject = (req, res) => {
    ParliamentProposal.findByIdAndUpdate(req.params.proposalId, {rejected: true})
        .then(() => {
            res.end();
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/*********** ADD IMAGE ************/
/**********************************/
const addImage = (req, res) => {
    return imageHelpers.api.addImage(req, res, ParliamentProposal);
};


module.exports = {
    list,
    create,
    accept,
    markAsInteresting,
    reject,
    addImage
};