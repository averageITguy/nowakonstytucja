const express = require('express');

const onlyModerator = require('../../auth/auth.routes').onlyModerator;

const ParliamentConfig = require('./parliament_proposals.config.js');
const ctrl = require('./parliament_proposals.controller.js');

const upload = ParliamentConfig.multerCfg();

const route = express.Router();


/**********************************/
/************** LIST **************/
/**********************************/
route.get('/', onlyModerator, ctrl.list);

/**********************************/
/************* CREATE *************/
/**********************************/
route.post('/', upload.single('pdfFile'), ctrl.create);

/**********************************/
/************* ACCEPT *************/
/**********************************/
route.post('/accept', onlyModerator, ctrl.accept);

/**********************************/
/********** INTERESTING ***********/
/**********************************/
route.post('/interesting/:proposalId', onlyModerator, ctrl.markAsInteresting);

/**********************************/
/************* REJECT *************/
/**********************************/
route.post('/reject/:proposalId', onlyModerator, ctrl.reject);

/**********************************/
/*********** ADD IMAGE ************/
/**********************************/
route.post('/addImage/:id/:type/:image', onlyModerator, ctrl.addImage);

module.exports = route;
