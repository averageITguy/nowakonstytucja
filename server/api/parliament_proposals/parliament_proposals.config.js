//TODO: wspolny config dla wszystkich uploads
const multerCfg = () => {
    const multer = require('multer');

    const storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, 'uploads')
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + '.pdf')
        }
    });

    return multer({
        storage: storage,
        limits: {
            fieldSize: 52428800 //50MB
        }
    });
};


module.exports = {
    multerCfg
};