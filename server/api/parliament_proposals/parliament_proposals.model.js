const mongoose = require('mongoose');

const createdByPlugin = require('../_createdBy.plugin');
const imagesPlugin = require('../_image/image.plugin');

const Schema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        maxlength: 350
    },
    printNb: {
        type: Number,
        required: true
    },
    descDetailed: {
        type: String,
        maxlength: 620
    },
    legislationUrl: String,
    pdfFile: String,
    interesting: {
        type: Boolean,
        default: false
    },
    rejected: {
        type: Boolean,
        default: false
    }
}, {timestamps: true});

Schema.plugin(createdByPlugin);
Schema.plugin(imagesPlugin, true);

module.exports = mongoose.model('billProposals', Schema);
