const apiHelpers = require('../api.helpers');

const api = {
    addImage(req, res, collection) {
        collection.findByIdAndUpdate(req.params.id, {[req.params.type]: req.params.image})
            .then(() => {
                res.send({});
            }).catch(apiHelpers.handleApiError(res));
    }
};

module.exports = {
    api
};