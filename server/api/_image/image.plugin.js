module.exports = function(schema, proposal) {
    schema.add({
        '200x150': {
            type: String,
            required: !proposal
        },
        '300x240': {
            type: String,
            required: !proposal
        },
        '600x200': {
            type: String
        }
    });
};