'use strict';

const express = require('express');

const onlyModerator = require('../../auth/auth.routes').onlyModerator;

const ctrl = require('./polls_proposals.controller.js');

const route = express.Router();


/**********************************/
/************** LIST **************/
/**********************************/
route.get('/', onlyModerator, ctrl.list);

/**********************************/
/************** SHOW **************/
/**********************************/
route.get('/:id', onlyModerator, ctrl.show);

/**********************************/
/************* CREATE *************/
/**********************************/
route.post('/', ctrl.create);

/**********************************/
/********** INTERESTING ***********/
/**********************************/
route.post('/interesting/:proposalId', onlyModerator, ctrl.markAsInteresting);

/**********************************/
/************* REJECT *************/
/**********************************/
route.post('/reject/:proposalId', onlyModerator, ctrl.reject);

/**********************************/
/*********** ADD IMAGE ************/
/**********************************/
route.post('/addImage/:id/:type/:image', onlyModerator, ctrl.addImage);

module.exports = route;
