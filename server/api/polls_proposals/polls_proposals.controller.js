'use strict';

const PollsProposals = require('./polls_proposals.model.js');

const apiHelpers = require('../api.helpers');
const imageHelpers = require('../_image/image.helpers');

/**********************************/
/************** LIST **************/
/**********************************/
const list = (req, res) => {
    PollsProposals.find({rejected: false}).lean()
        .then(results => {
            res.json(results);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************** SHOW **************/
/**********************************/
const show = (req, res) => {
    PollsProposals.findById(req.params.id)
        .select({
            question: 1,
            descShort: 1,
            createdBy: 1,
            '200x150': 1,
            '300x240': 1,
            '600x200': 1
        }).lean()
        .then(proposal => {
            res.send(proposal);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************* CREATE *************/
/**********************************/
const create = (req, res) => {
    const newPollProposal = new PollsProposals({
        question: req.body.question,
        descShort: req.body.descShort,
        createdBy: req.signedCookies.anonymousID
    });

    newPollProposal.save()
        .then(function() {
            res.status(201).end();
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/********** INTERESTING ***********/
/**********************************/
const markAsInteresting = (req, res) => {
    PollsProposals.findByIdAndUpdate(req.params.proposalId, {interesting: req.body.interesting})
        .then(() => {
            res.end();
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************* REJECT *************/
/**********************************/
const reject = (req, res) => {
    PollsProposals.findByIdAndUpdate(req.params.proposalId, {rejected: true})
        .then(() => {
            res.end();
        })
        .catch(apiHelpers.handleApiError(res));
};

/**********************************/
/*********** ADD IMAGE ************/
/**********************************/
const addImage = (req, res) => {
   return imageHelpers.api.addImage(req, res, PollsProposals);
};

module.exports = {
    list,
    show,
    create,
    markAsInteresting,
    reject,
    addImage
};