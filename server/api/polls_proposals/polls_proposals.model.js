'use strict';

const mongoose = require('mongoose');
const createdByPlugin = require('../_createdBy.plugin');
const imagesPlugin = require('../_image/image.plugin');

const PollProposalSchema = new mongoose.Schema({
    question: {
        type: String,
        required: true,
        maxlength: 200
    },
    descShort: {
        type: String,
        maxlength: 400
    },
    interesting: {
        type: Boolean,
        default: false
    },
    rejected: {
        type: Boolean,
        default: false
    }
}, {timestamps: true});

PollProposalSchema.plugin(createdByPlugin);
PollProposalSchema.plugin(imagesPlugin, true);

module.exports = mongoose.model('pollProposal', PollProposalSchema);
