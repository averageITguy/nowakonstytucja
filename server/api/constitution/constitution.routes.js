const express = require('express');

const ctrl = require('./constitution.controller.js');

const route = express.Router();

/**********************************/
/********** LIST PROBLEMS *********/
/**********************************/
route.get('/', ctrl.listProblems);

/**********************************/
/******** SWIPER PROBLEMS *********/
/**********************************/
route.get('/swiper/:category', ctrl.swiperProblems);

/**********************************/
/******* PROBLEM CATEGORIES *******/
/**********************************/
route.get('/categories', ctrl.getProblemCategories);

/**********************************/
/********** SHOW PROBLEM **********/
/**********************************/
route.get('/:id', ctrl.showProblem);

/**********************************/
/********* SIGN PROBLEM ***********/
/**********************************/
route.post('/sign/:id', ctrl.signProblem);

/**********************************/
/********* SHOW SOLUTION **********/
/**********************************/
route.get('/:problemId/solution/:solutionId', ctrl.showSolution);

/**********************************/
/******** LIST SOLUTIONS **********/
/**********************************/
route.get('/:problemId/solution', ctrl.listSolutionsByProblem);

/**********************************/
/********* VOTE SOLUTION **********/
/**********************************/
route.post('/:problemId/vote/:solutionId', ctrl.voteSolution);

module.exports = route;
