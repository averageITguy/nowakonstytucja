const mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId;

const Problem = require('./constitution.model').model;
const categories = require('./constitution.model').categories;

const apiHelpers = require('../api.helpers');
const signsHelpers = require('../_signs/signs.helpers');

const ERROR_MESSAGES = require('../../config/globals').ERROR_MESSAGES.votes;


/***********************************/
/********** LIST PROBLEMS **********/
/***********************************/
const listProblems = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    //pagination
    const page = req.query.page || 1;
    const pageSize = 4;
    const skip = {
        $skip: (page - 1 ) * pageSize
    };
    const limit = {$limit: pageSize};

    let sort = null;
    if(req.query.sort === 'newest') {
        sort = {$sort: {createdAt: -1}};
    } else {
        sort = {$sort: {signsCtr: -1, _id: 1}};
    }

    let aggregationPipeline = [sort, skip, limit];

    if(req.query.filters) {
        const match = {
            $match: {}
        };

        if(req.query.filters.category) {
            match.$match.category = req.query.filters.category;
        }

        if(req.query.filters.phrase) {
            match.$match.$text = {$search: req.query.filters.phrase};
            sort.$sort.score = {$meta: 'textScore'};
        }

        aggregationPipeline.unshift(match);
    }

    const fields = {
        $project: {
            'descShort': 1,
            'category': 1,
            'createdAt': 1,
            'signsCtr': 1,
            'createdBy': {
                'facebook': {
                    'name': 1,
                    'id': 1
                }
            },
            'userSigned': {$setIsSubset: [[ObjectId(req.signedCookies.anonymousID)], '$signs']}
        }
    };

    Problem.aggregate([...aggregationPipeline, fields])
        .then(results => {
            res.send(results);
        })
        .catch(apiHelpers.handleApiError(res));
};


/***********************************/
/********* SWIPER PROBLEMS *********/
/***********************************/
const swiperProblems = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    const matchProblemByCategory = {
        $match: {category: req.params.category}
    };

    const filterUserVote = {
        $redact: {
            $cond: {
                if: {
                    $or: [
                        {
                            $eq: ['$userId', ObjectId(req.signedCookies.anonymousID)]
                        },
                        {
                            $not: ['$userId']
                        }
                    ]
                },
                then: '$$DESCEND',
                else: '$$PRUNE'
            }
        }
    };

    const fields = {
        $project: {
            '_id': 1,
            'descShort': 1,
            'descDetailed': 1,
            'category': 1,
            'solutions': {
                $filter: {
                    input: "$solutions",
                    as: "solution",
                    cond: {$eq: [0, {$size: [{$ifNull: ["$$solution.votes", []]}]}]}
                }
            }
        }
    };

    const sortProblemsWithSolutionsFirst = {
        $sort: {
            'solutions': -1
        }
    };

    Problem.aggregate([matchProblemByCategory, filterUserVote, fields, sortProblemsWithSolutionsFirst])
        .then(problems => {
            res.json(problems);
        }).catch(apiHelpers.handleApiError(res));
};


/**********************************/
/********** SHOW PROBLEM **********/
/**********************************/
const showProblem = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    const filterProblem = {
        $match: {
            '_id': ObjectId(req.params.id)
        }
    };

    const unwindSolutions = {
        $unwind: {
            'path': '$solutions',
            'preserveNullAndEmptyArrays': true
        }
    };

    //pagination
    const pageSize = 5;
    const limit = {$limit: pageSize};
    const sort = {$sort: {'solutions.yesVotesCtr': -1, 'solutions._id': 1}};

    const returnOnlyUserVote = {
        $redact: {
            $cond: {
                if: {
                    $or: [
                        {
                            $eq: ['$userId', ObjectId(req.signedCookies.anonymousID)]
                        },
                        {
                            $not: ['$userId']
                        }
                    ]
                },
                then: '$$DESCEND',
                else: '$$PRUNE'
            }
        }
    };

    const groupBySolution = {
        $group: {
            '_id': '$_id',
            'createdAt': {$first: '$createdAt'},
            'article': {$first: '$article'},
            'descShort': {$first: '$descShort'},
            'descDetailed': {$first: '$descDetailed'},
            'category': {$first: '$category'},
            'createdBy': {$first: '$createdBy'},
            'signsCtr': {$first: '$signsCtr'},
            'solutions': {$push: '$solutions'},
            'signs': {$first: '$signs'}
        }
    };

    const fields = {
        $project: {
            signsCtr: 1,
            createdAt: 1,
            article: 1,
            descShort: 1,
            descDetailed: 1,
            category: 1,
            createdBy: {
                facebook: {
                    name: 1,
                    id: 1
                }
            },
            userSigned: {$setIsSubset: [[ObjectId(req.signedCookies.anonymousID)], '$signs']},
            solutions: {
                yesVotesCtr: 1,
                votesCtr: 1,
                createdAt: 1,
                descShort: 1,
                createdBy: {
                    facebook: {
                        name: 1,
                        id: 1
                    }
                },
                _id: 1,
                votes: {
                    value: 1
                }

            }
        }
    };

    Problem.aggregate([filterProblem, unwindSolutions, sort, limit, returnOnlyUserVote, groupBySolution, fields])
        .then(([result]) => {
            result.solutions = result.solutions.filter(
                solution => typeof solution._id !== 'undefined');

            if(result.solutions.length === 0) delete result.solutions;

            res.send(result);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/******** LIST SOLUTIONS **********/
/**********************************/
const listSolutionsByProblem = (req, res) => {
    const filterProblem = {
        $match: {
            '_id': ObjectId(req.params.problemId)
        }
    };

    const unwindSolutions = {
        $unwind: {
            'path': '$solutions',
            'preserveNullAndEmptyArrays': true
        }
    };

    //pagination
    const page = req.query.page || 2;
    const pageSize = 5;
    const skip = {
        $skip: (page - 1 ) * pageSize
    };
    const limit = {$limit: pageSize};
    const sort = {$sort: {'solutions.yesVotesCtr': -1, 'solutions._id': 1}};

    const returnOnlyUserVote = {
        $redact: {
            $cond: {
                if: {
                    $or: [
                        {
                            $eq: ['$userId', ObjectId(req.signedCookies.anonymousID)]
                        },
                        {
                            $not: ['$userId']
                        }
                    ]
                },
                then: '$$DESCEND',
                else: '$$PRUNE'
            }
        }
    };

    const groupBySolution = {
        $group: {
            '_id': '$_id',
            'createdAt': {$first: '$createdAt'},
            'article': {$first: '$article'},
            'descShort': {$first: '$descShort'},
            'descDetailed': {$first: '$descDetailed'},
            'category': {$first: '$category'},
            'createdBy': {$first: '$createdBy'},
            'signsCtr': {$first: '$signsCtr'},
            'solutions': {$push: '$solutions'},
            'signs': {$first: '$signs'}
        }
    };

    const fields = {
        $project: {
            solutions: {
                yesVotesCtr: 1,
                votesCtr: 1,
                createdAt: 1,
                descShort: 1,
                createdBy: {
                    facebook: {
                        name: 1,
                        id: 1
                    }
                },
                _id: 1,
                votes: {
                    value: 1
                }
            }
        }
    };

    Problem.aggregate([filterProblem, unwindSolutions, sort, skip, limit, returnOnlyUserVote, groupBySolution, fields])
        .then(([result]) => {
            res.send(result && result.solutions || []);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/********* SIGN PROBLEM ***********/
/**********************************/
const signProblem = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    signsHelpers.api.sign(req, res, Problem);
};


/**********************************/
/******* PROBLEM CATEGORIES *******/
/**********************************/
const getProblemCategories = (req, res) => {
    res.json({categories});
};


/**********************************/
/********* SHOW SOLUTION **********/
/**********************************/
const showSolution = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    const problemId = req.params.problemId;
    const solutonId = req.params.solutionId;

    const filterProblem = {
        '$match': {
            '_id': ObjectId(problemId)
        }
    };

    const unwindSolutions = {
        $unwind: '$solutions'
    };

    const filterSolution = {
        $match: {
            'solutions._id': ObjectId(solutonId)
        }
    };

    const returnOnlyUserVote = {
        $redact: {
            $cond: {
                if: {
                    $or: [
                        {
                            $eq: ['$userId', ObjectId(req.signedCookies.anonymousID)]
                        },
                        {
                            $not: ['$userId']
                        }
                    ]
                },
                then: '$$DESCEND',
                else: '$$PRUNE'
            }
        }
    };


    const removeUnnecessaryFields = {
        $project: {
            _id: 0,
            solutions: {
                yesVotesCtr: 1,
                votesCtr: 1,
                descShort: 1,
                descDetailed: 1,
                createdBy: 1,
                createdAt: 1,
                votes: {
                    value: 1
                },
                _id: 1
            }
        }
    };

    Problem.aggregate([filterProblem, unwindSolutions, filterSolution, returnOnlyUserVote, removeUnnecessaryFields])
        .then(([result]) => {
            res.send(result.solutions);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/********* VOTE SOLUTION **********/
/**********************************/
const voteSolution = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    const problemId = req.params.problemId;
    const solutionId = req.params.solutionId;

    const filterProblem = {
        '$match': {
            '_id': ObjectId(problemId)
        }
    };

    const filterSolution = {
        $project: {
            solutions: {
                $filter: {
                    input: '$solutions',
                    as: 'solution',
                    cond: {
                        $eq: ['$$solution._id', ObjectId(solutionId)]
                    }
                }
            }
        }
    };

    const returnOnlyUserVote = {
        $redact: {
            $cond: {
                if: {
                    $or: [
                        {
                            $eq: ['$userId', ObjectId(req.signedCookies.anonymousID)]
                        },
                        {
                            $not: ['$userId']
                        }
                    ]
                },
                then: '$$DESCEND',
                else: '$$PRUNE'
            }
        }
    };

    Problem.aggregate([filterProblem, filterSolution, returnOnlyUserVote])
        .then(([problem]) => {
            const userAlreadyVoted = problem.solutions[0].votes.length > 0;

            if(userAlreadyVoted) {
                res.send({
                    err: ERROR_MESSAGES.ALREADY_VOTED,
                    data: problem.solutions[0].votes[0].value
                });
            } else {
                const vote = {
                    userId: req.signedCookies.anonymousID,
                    value: req.body.value
                };

                const update = {
                    $push: {'solutions.$.votes': vote},
                    $inc: {'solutions.$.votesCtr': 1}
                };

                if(vote.value) { //yes-vote
                    update.$inc['solutions.$.yesVotesCtr'] = 1;
                }

                Problem.update(
                    {'solutions._id': solutionId}, update
                ).then(() => {
                    res.end();
                }).catch(apiHelpers.handleApiError(res));
            }
        })
        .catch(apiHelpers.handleApiError(res));
};


module.exports = {
    listProblems,
    showProblem,
    signProblem,
    getProblemCategories,
    showSolution,
    listSolutionsByProblem,
    voteSolution,
    swiperProblems
};