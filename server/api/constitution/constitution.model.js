const mongoose = require('mongoose');

const createdByPlugin = require('../_createdBy.plugin');
const votesPlugin = require('../_votes/votes.plugin.js');
const signsPlugin = require('../_signs/signs.plugin');
const articlePlugin = require('../_article/article.plugin');

const categories = ['Ekonomia i gospodarka',
    'Zagrożenia i bezpieczeństwo',
    'Nowe technologie',
    'Wymiar sprawiedliwości',
    'Edukacja',
    'Sport i turystyka',
    'Komunikacja i infrastruktura',
    'Zdrowie i opieka medyczna',
    'Rolnictwo',
    'Kultura i sztuka',
    'Media (TV, radio, prasa, Internet)',
    'Wojsko i służby mundurowe',
    'Środowisko'
];


const SolutionSchema = new mongoose.Schema({
    descShort: {
        type: String,
        required: true,
        maxlength: 200
    },
    descDetailed: {
        type: String,
        maxlength: 620
    },
}, {timestamps: true});

SolutionSchema.plugin(createdByPlugin);
SolutionSchema.plugin(votesPlugin);


const ProblemSchema = new mongoose.Schema({
    descShort: {
        type: String,
        text: true,
        required: true,
        maxlength: 200
    },
    descDetailed: {
        type: String,
        maxlength: 620
    },
    category: {
        type: {enum: categories},
        requred: true
    },
    solutions: {
        type: [SolutionSchema],
        default: []
    }
}, {timestamps: true});

ProblemSchema.plugin(createdByPlugin);
ProblemSchema.plugin(signsPlugin);
ProblemSchema.plugin(articlePlugin);

module.exports.model = mongoose.model('problem', ProblemSchema);
module.exports.categories = categories;
