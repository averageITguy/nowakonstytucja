const mongoose = require('mongoose');

const votesPlugin = require('../_votes/votes.plugin.js');

const Subcomment = new mongoose.Schema({
    author: {
        type: String,
        required: true,
        maxlength: 100
    },
    content: {
        type: String,
        required: true,
        maxlength: 620
    },
}, {timestamps: true});

Subcomment.plugin(votesPlugin);

const CommentSchema = new mongoose.Schema({
    entityId: {
        type: mongoose.Schema.Types.ObjectId,
        index: true,
        required: true
    },
    author: {
        type: String,
        required: true,
        maxlength: 100
    },
    content: {
        type: String,
        required: true,
        maxlength: 620
    },
    responses: {
        type: [Subcomment],
        default: []
    }
}, {timestamps: true});

CommentSchema.plugin(votesPlugin);

module.exports = mongoose.model('comment', CommentSchema);
