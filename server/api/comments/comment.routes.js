const express = require('express');

const ctrl = require('./comment.controller.js');

const route = express.Router();

/**********************************/
/********** LIST COMMENTS *********/
/**********************************/
route.get('/:entityId', ctrl.listComments);

/**********************************/
/********** ADD COMMENT ***********/
/**********************************/
route.post('/:entityId', ctrl.addComment);

/**********************************/
/********** VOTE COMMENT **********/
/**********************************/
route.post('/:commentId/vote/:parentCommentId?', ctrl.voteComment);

/**********************************/
/********* LIST RESPONSES *********/
/**********************************/
route.get('/responses/:entityId/:commentId', ctrl.listResponses);

module.exports = route;
