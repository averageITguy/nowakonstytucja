const mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId;

const Comment = require('./comment.model.js');

const apiHelpers = require('../api.helpers');
const ERROR_MESSAGES = require('../../config/globals').ERROR_MESSAGES.votes;

/**********************************/
/********** LIST COMMENTS *********/
/**********************************/
const listComments = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    const filterByEntity = {
        $match: {
            entityId: ObjectId(req.params.entityId)
        }
    };

    //pagination
    const page = req.query.page || 1;
    const pageSize = req.query.single === 'true' ? 1 : 5;
    const limit = {$limit: page * pageSize};

    const sort = req.query.sort === 'newest' ? {$sort: {updatedAt: -1}} : {
        $sort: {
            votesCtr: -1,
            _id: 1
        }
    };

    //subcomments pagination
    const subcommentsPage = req.query.subPage || 1;
    const subcommentsPageSize = req.query.single === 'true' ? 1 : 2;
    const skipSub = (subcommentsPage - 1) * subcommentsPageSize;

    const limitSubcomments = {
        $project: {
            entityId: 1,
            allResponsesSize: {$size: ['$responses']},
            responses: {$slice: ["$responses", skipSub, subcommentsPageSize]},
            author: 1,
            content: 1,
            createdAt: 1,
            votes: 1,
            votesCtr: 1,
            yesVotesCtr: 1,
        }
    };

    const returnOnlyUserVote = {
        $redact: {
            $cond: {
                if: {
                    $or: [
                        {$eq: ['$userId', ObjectId(req.signedCookies.anonymousID)]},
                        {$not: ['$userId']}
                    ]
                },
                then: '$$DESCEND',
                else: '$$PRUNE'
            }
        }
    };

    Comment.aggregate([filterByEntity, sort, limit, limitSubcomments, returnOnlyUserVote])
        .then(results => {
            res.send(results);
        })
        .catch(apiHelpers.handleApiError(res));
};


/***********************************/
/*********** ADD COMMENT ***********/
/***********************************/
const addComment = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    const newComment = req.body;
    newComment.entityId = req.params.entityId;

    if(newComment.parentId) {
        Comment.findById(newComment.parentId)
            .then(parentCommentDb => {
                const newSubCommId = new ObjectId();
                newComment._id = newSubCommId;
                parentCommentDb.responses.push(newComment);

                parentCommentDb.save(() => {
                    const newSubCommDb = parentCommentDb.responses.id(newSubCommId);
                    res.status(201).send(newSubCommDb);
                });
            }).catch(apiHelpers.handleApiError(res));
    } else {
        Comment.create(newComment)
            .then(newDbComment => {
                res.status(201).send(newDbComment);
            }).catch(apiHelpers.handleApiError(res));
    }
};


/**********************************/
/********** VOTE COMMENT **********/
/**********************************/
const voteComment = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    const commentId = req.params.commentId;
    const parentCommentId = req.params.parentCommentId;

    const filterComment = {
        '$match': {
            '_id': ObjectId(parentCommentId !== 'undefined' ? parentCommentId : commentId)
        }
    };

    let aggregationPipeline = [filterComment];


    if(parentCommentId !== 'undefined') {
        const filterSubcomment = {
            $project: {
                responses: {
                    $filter: {
                        input: '$responses',
                        as: 'response',
                        cond: {
                            $eq: ['$$response._id', ObjectId(commentId)]
                        }
                    }
                }
            }
        };
        aggregationPipeline.push(filterSubcomment);
    } else {
        const onlyVotes = {
            $project: {
                votes: 1
            }
        };
        aggregationPipeline.push(onlyVotes);
    }

    const returnOnlyUserVote = {
        $redact: {
            $cond: {
                if: {
                    $or: [
                        {
                            $eq: ['$userId', ObjectId(req.signedCookies.anonymousID)]
                        },
                        {
                            $not: ['$userId']
                        }
                    ]
                },
                then: '$$DESCEND',
                else: '$$PRUNE'
            }
        }
    };

    aggregationPipeline.push(returnOnlyUserVote);

    Comment.aggregate(aggregationPipeline)
        .then(([comment]) => {
            if(parentCommentId !== 'undefined') { //voted on subcomment
                const userAlreadyVoted = comment.responses[0].votes.length > 0;

                if(userAlreadyVoted) {
                    res.send({
                        err: ERROR_MESSAGES.ALREADY_VOTED,
                        data: comment.responses[0].votes[0].value
                    });
                } else {
                    const vote = {
                        userId: req.signedCookies.anonymousID,
                        value: req.body.value
                    };

                    const update = {
                        $push: {'responses.$.votes': vote},
                        $inc: {'responses.$.votesCtr': 1}
                    };

                    if(vote.value) { //yes-vote
                        update.$inc['responses.$.yesVotesCtr'] = 1;
                    }

                    Comment.update(
                        {'responses._id': commentId}, update
                    ).then(() => {
                        res.end();
                    }).catch(apiHelpers.handleApiError(res));
                }
            } else {
                const userAlreadyVoted = comment.votes.length > 0;

                if(userAlreadyVoted) {
                    res.send({
                        err: ERROR_MESSAGES.ALREADY_VOTED,
                        data: comment.votes[0].value
                    });
                } else {
                    const vote = {
                        userId: req.signedCookies.anonymousID,
                        value: req.body.value
                    };

                    const update = {
                        $push: {'votes': vote},
                        $inc: {'votesCtr': 1}
                    };

                    if(vote.value) { //yes-vote
                        update.$inc['yesVotesCtr'] = 1;
                    }

                    Comment.update({_id: commentId}, update)
                        .then(() => {
                            res.end();
                        }).catch(apiHelpers.handleApiError(res));
                }
            }
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/********* LIST RESPONSES *********/
/**********************************/
const listResponses = (req, res)=> {
    Comment.find({
        entityId: req.params.entityId,
        _id: req.params.commentId
    }, {responses: 1}).then(([{responses}]) => {
        res.send(responses);
    });
};

module.exports = {
    listComments,
    addComment,
    voteComment,
    listResponses
};