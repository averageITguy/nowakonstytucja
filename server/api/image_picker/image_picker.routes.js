const express = require('express');

const path = require('path');
const fs = require('fs');

const route = express.Router();

route.get('/:size', (req, res) => {
    const images = fs.readdirSync(path.resolve(__dirname, '../../../', `src/images/picker/${req.params.size}`));

    res.send(images);
});

module.exports = route;