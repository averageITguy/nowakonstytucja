const SolutionProposal = require('./solutions_proposals.model');
const Problem = require('../constitution/constitution.model').model;

const apiHelpers = require('../api.helpers');


/**********************************/
/**** LIST SOLUTION PROPOSALS *****/
/**********************************/
const listSolutionProposals = (req, res) => {
    SolutionProposal.aggregate([
        {
            $match: {
                'rejected': false
            }
        },
        {
            $group: {
                '_id': '$problemId',
                'solutions': {$push: '$$ROOT'}
            }
        },
        {
            $lookup: {
                from: 'problems',
                localField: '_id',
                foreignField: '_id',
                as: 'problem'
            }

        },
        {
            $unwind: '$problem'
        },
        {
            $project: {
                'solutions': 1,
                'problem': {
                    'descShort': 1
                }
            }
        }])
        .then(proposals => {
            res.send(proposals);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/**** CREATE SOLUTION PROPOSAL ****/
/**********************************/
const createSolutionProposal = (req, res) => {
    const newSolutionProposal = new SolutionProposal(req.body);

    newSolutionProposal.createdBy = req.signedCookies.anonymousID;

    newSolutionProposal.save()
        .then(function(dbNewSolutionProposal) {
            return res.status(201).json(dbNewSolutionProposal.id);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************* ACCEPT *************/
/**********************************/
const accept = (req, res) => {
    const solutionProposal = req.body;

    Problem.findById(solutionProposal.problemId)
        .then(problem => {
            problem.solutions.push({
                descDetailed: solutionProposal.descDetailed,
                descShort: solutionProposal.descShort,
                createdBy: solutionProposal.createdBy
            });
            problem.save().then((dbUpdatedProblem) => {
                SolutionProposal.findByIdAndRemove(solutionProposal._id).then(() => {
                    res.status(201).json(dbUpdatedProblem.id);
                });
            });
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/********** INTERESTING ***********/
/**********************************/
const markAsInteresting = (req, res) => {
    SolutionProposal.findByIdAndUpdate(req.params.proposalId, {interesting: req.body.interesting})
        .then(() => {
            res.end();
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************* REJECT *************/
/**********************************/
const reject = (req, res) => {
    SolutionProposal.findByIdAndUpdate(req.params.proposalId, {rejected: true})
        .then(() => {
            res.end();
        })
        .catch(apiHelpers.handleApiError(res));
};


module.exports = {
    listSolutionProposals,
    createSolutionProposal,
    accept,
    markAsInteresting,
    reject
};