const mongoose = require('mongoose');

const categories = require('../constitution/constitution.model').categories;

const createdByPlugin = require('../_createdBy.plugin');

const ProblemProposalSchema = new mongoose.Schema({
    descShort: {
        type: String,
        required: true,
        maxlength: 200
    },
    descDetailed: {
        type: String,
        maxlength: 620
    },
    category: {
        type: {enum: categories},
        requred: true
    },
    interesting: {
        type: Boolean,
        default: false
    },
    rejected: {
        type: Boolean,
        default: false
    }
}, {timestamps: true});

ProblemProposalSchema.plugin(createdByPlugin);

module.exports = mongoose.model('problemProposals', ProblemProposalSchema);
