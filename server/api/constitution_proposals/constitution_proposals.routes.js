const express = require('express');

const onlyModerator = require('../../auth/auth.routes').onlyModerator;

const ctrlProblemProposals = require('./problems_proposals.controller');
const ctrlSolutionProposals = require('./solutions_proposals.controller');

const route = express.Router();

/**********************************/
/***** LIST PROBLEM PROPOSALS *****/
/**********************************/
route.get('/problem', onlyModerator, ctrlProblemProposals.listProblemProposals);

/**********************************/
/**** CREATE PROBLEM PROPOSAL *****/
/**********************************/
route.post('/problem', ctrlProblemProposals.createProblemProposal);

/**********************************/
/********* ACCEPT PROBLEM *********/
/**********************************/
route.post('/problem/accept', onlyModerator, ctrlProblemProposals.accept);

/**********************************/
/****** INTERESTING PROBLEM *******/
/**********************************/
route.post('/problem/interesting/:proposalId', onlyModerator, ctrlProblemProposals.markAsInteresting);

/**********************************/
/********* REJECT PROBLEM *********/
/**********************************/
route.post('/problem/reject/:proposalId', onlyModerator, ctrlProblemProposals.reject);

/**********************************/
/***** LIST SOLUTION PROPOSALS ****/
/**********************************/
route.get('/solution', onlyModerator, ctrlSolutionProposals.listSolutionProposals);

/**********************************/
/**** CREATE SOLUTION PROPOSAL ****/
/**********************************/
route.post('/solution', ctrlSolutionProposals.createSolutionProposal);

/**********************************/
/********* ACCEPT SOLUTION ********/
/**********************************/
route.post('/solution/accept', onlyModerator, ctrlSolutionProposals.accept);

/**********************************/
/****** INTERESTING SOLUTION ******/
/**********************************/
route.post('/solution/interesting/:proposalId', onlyModerator, ctrlSolutionProposals.markAsInteresting);

/**********************************/
/********* REJECT SOLUTION ********/
/**********************************/
route.post('/solution/reject/:proposalId', onlyModerator, ctrlSolutionProposals.reject);

module.exports = route;
