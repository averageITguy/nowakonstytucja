const ProblemProposal = require('./problems_proposals.model');
const Problem = require('../constitution/constitution.model').model;

const apiHelpers = require('../api.helpers');


/**********************************/
/***** LIST PROBLEM PROPOSALS *****/
/**********************************/
const listProblemProposals = (req, res) => {
    ProblemProposal.find({rejected: false}).lean()
        .then(problemProposals => {
            res.send(problemProposals);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/**** CREATE PROBLEM PROPOSAL *****/
/**********************************/
const createProblemProposal = (req, res) => {
    const newProblemProposal = new ProblemProposal(req.body);

    newProblemProposal.createdBy = req.signedCookies.anonymousID;

    newProblemProposal.save()
        .then(function(dbNewProblemProposal) {
            return res.status(201).json(dbNewProblemProposal.id);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************* ACCEPT *************/
/**********************************/
const accept = (req, res) => {
    const proposal = req.body;

    Problem.create({
        descShort: proposal.descShort,
        descDetailed: proposal.descDetailed,
        category: proposal.category,
        createdBy: proposal.createdBy
    }).then(() => {
        ProblemProposal.findByIdAndRemove(proposal._id).then(() => {
            res.end();
        });
    }).catch(apiHelpers.handleApiError(res));
};


/**********************************/
/********** INTERESTING ***********/
/**********************************/
const markAsInteresting = (req, res) => {
    ProblemProposal.findByIdAndUpdate(req.params.proposalId, {interesting: req.body.interesting})
        .then(() => {
            res.end();
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************* REJECT *************/
/**********************************/
const reject = (req, res) => {
    ProblemProposal.findByIdAndUpdate(req.params.proposalId, {rejected: true})
        .then(() => {
            res.end();
        })
        .catch(apiHelpers.handleApiError(res));
};


module.exports = {
    listProblemProposals,
    createProblemProposal,
    accept,
    markAsInteresting,
    reject
};