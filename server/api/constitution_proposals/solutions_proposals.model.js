const mongoose = require('mongoose');

const createdByPlugin = require('../_createdBy.plugin');

const SolutionProposalSchema = new mongoose.Schema({
    descShort: {
        type: String,
        required: true,
        maxlength: 200
    },
    descDetailed: {
        type: String,
        maxlength: 620
    },
    problemId: mongoose.Schema.Types.ObjectId,
    interesting: {
        type: Boolean,
        default: false
    },
    rejected: {
        type: Boolean,
        default: false
    }
}, {timestamps: true});

SolutionProposalSchema.plugin(createdByPlugin);

module.exports = mongoose.model('solutionProposal', SolutionProposalSchema);