const express = require('express');

const ctrl = require('./polls.controller');

const route = express.Router();

/**********************************/
/************** LIST **************/
/**********************************/
route.get('/', ctrl.list);

/**********************************/
/********* LIST EXCHANGE **********/
/**********************************/
route.get('/exchange', ctrl.listExchange);

/**********************************/
/************** SHOW **************/
/**********************************/
route.get('/:id', ctrl.show);

/**********************************/
/************ EXCHANGE ************/
/**********************************/
route.post('/exchange/:pollId?', ctrl.exchange);

/**********************************/
/************** VOTE **************/
/**********************************/
route.post('/vote/:id', ctrl.vote);

module.exports = route;
