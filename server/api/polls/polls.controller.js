'use strict';

const mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId;

const Polls = require('./polls.model');
const PollsProposals = require('../polls_proposals/polls_proposals.model.js');

const votesHelpers = require('../_votes/votes.helpers');
const apiHelpers = require('../api.helpers');


/**********************************/
/************** LIST **************/
/**********************************/
const list = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    const filters = {
        '$match': {
            'active': req.query.archived ? false : true
        }
    };

    const aggregationPipeline = [filters];

    if(req.query.archived) { //pagination only for archive list
        const page = req.query.page || 1;
        const pageSize = 4;
        const skip = {
            $skip: (page - 1) * pageSize
        };
        const limit = {$limit: pageSize};

        aggregationPipeline.push(skip);
        aggregationPipeline.push(limit);
    }

    if(req.query.filters) {
        const match = {
            $match: {}
        };

        if(req.query.filters.phrase) {
            match.$match.$text = {$search: req.query.filters.phrase};
            sort.$sort.score = {$meta: 'textScore'};
        }

        aggregationPipeline.unshift(match);
    }

    const returnOnlyUserVote = {
        $redact: {
            $cond: {
                if: {
                    $or: [
                        {
                            $eq: ['$userId', ObjectId(req.signedCookies.anonymousID)]
                        },
                        {
                            $not: ['$userId']
                        }
                    ]
                },
                then: '$$DESCEND',
                else: '$$PRUNE'
            }
        }
    };
    aggregationPipeline.push(returnOnlyUserVote);

    const removeUnnecessaryFields = {
        '$project': {
            'question': 1,
            'yesVotesCtr': 1,
            'votesCtr': 1,
            'createdAt': 1,
            '200x150': 1,
            '600x200': 1,
            'votes': 1,
            'votes_length': {$size: {"$ifNull": ["$votes", []]}}
        }
    };

    aggregationPipeline.push(removeUnnecessaryFields);

    const sort = {$sort: {votes_length: 1, createdAt: -1}};

    aggregationPipeline.push(sort);

    Polls.aggregate(aggregationPipeline)
        .then(results => {
            res.send(results);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/********* LIST EXCHANGE **********/
/**********************************/
const listExchange = (req, res) => {
    Polls.aggregate()
        .match({active: true})
        .project({
            question: 1,
            descShort: 1,
            votesCtr: 1,
            //TODO: dołozyć ilość głosów przez ostatnie 2 dni
        })
        .then(results => {
            res.send(results);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************** SHOW **************/
/**********************************/
const show = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    const filters = {
        '$match': {
            '_id': ObjectId(req.params.id)
        }
    };

    const aggregationPipeline = [filters];

    const returnOnlyUserVote = {
        $redact: {
            $cond: {
                if: {
                    $or: [
                        {
                            $eq: ['$userId', ObjectId(req.signedCookies.anonymousID)]
                        },
                        {
                            $not: ['$userId']
                        }
                    ]
                },
                then: '$$DESCEND',
                else: '$$PRUNE'
            }
        }
    };

    aggregationPipeline.push(returnOnlyUserVote);


    const fields = {
        '$project': {
            'yesVotesCtr': 1,
            'votesCtr': 1,
            'article': 1,
            'question': 1,
            'descShort': 1,
            '300x240': 1,
            'votes': 1
        }
    };

    aggregationPipeline.push(fields);

    Polls.aggregate(aggregationPipeline)
        .then(([result]) => {
            res.send(result);
        })
        .catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************ EXCHANGE ************/
/**********************************/
const exchange = (req, res) => {
    const pollId = req.params.pollId;
    const proposal = req.body;

    Polls.create({
        question: proposal.question,
        descShort: proposal.descShort,
        createdBy: proposal.createdBy,
        '200x150': proposal['200x150'],
        '300x240': proposal['300x240'],
        '600x200': proposal['600x200']
    }).then(() => {
        if(pollId) {
            Polls.update({_id: pollId}, {active: false}).then(() => {
                PollsProposals.remove({_id: proposal._id}).then(() => {
                    res.end();
                });
            });
        } else {
            PollsProposals.remove({_id: proposal._id}).then(() => {
                res.end();
            });
        }
    }).catch(apiHelpers.handleApiError(res));
};


/**********************************/
/************** VOTE **************/
/**********************************/
const vote = (req, res) => {
    if(!apiHelpers.checkUser(req, res)) return;

    return votesHelpers.api.vote(req, res, Polls);
};


module.exports = {
    list,
    listExchange,
    show,
    exchange,
    vote
};