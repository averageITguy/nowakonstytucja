'use strict';

const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const createdByPlugin = require('../_createdBy.plugin');
const votesPlugin = require('../_votes/votes.plugin.js');
const imagesPlugin = require('../_image/image.plugin');
const articlePlugin = require('../_article/article.plugin');

const PollSchema = new Schema({
    question: {
        type: String,
        required: true,
        text : true,
        maxlength: 200
    },
    descShort: {
        type: String,
        maxlength: 400
    },
    active: {
        type: Boolean,
        default: true
    }
}, {timestamps: true});

PollSchema.plugin(createdByPlugin);
PollSchema.plugin(votesPlugin);
PollSchema.plugin(imagesPlugin);
PollSchema.plugin(articlePlugin);

module.exports = mongoose.model('poll', PollSchema);
