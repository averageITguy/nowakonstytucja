'use strict';

const express = require('express');

const route = express.Router();

const pollsRoute = require('./polls/polls.routes'),
    pollsProposalsRoute = require('./polls_proposals/polls_proposals.routes'),
    initiativesRoute = require('./initiatives/initiatives.routes'),
    initiativesProposalsRoute = require('./initiatives_proposals/initiatives_proposals.routes'),
    parliamentRoute = require('./parliament/parliament.routes'),
    parliamentProposalsRoute = require('./parliament_proposals/parliament_proposals.routes'),
    constitutionRoute = require('./constitution/constitution.routes'),
    constitutionProposalsRoute = require('./constitution_proposals/constitution_proposals.routes'),
    imagePickerRoute = require('./image_picker/image_picker.routes'),
    commentRoute = require('./comments/comment.routes');

route.use('/polls', pollsRoute);
route.use('/pollsProposals', pollsProposalsRoute);
route.use('/initiatives', initiativesRoute);
route.use('/initiativesProposals', initiativesProposalsRoute);
route.use('/parliament', parliamentRoute);
route.use('/parliamentProposals', parliamentProposalsRoute);
route.use('/problem', constitutionRoute);
route.use('/constitutionProposals', constitutionProposalsRoute);
route.use('/imagePicker', imagePickerRoute);
route.use('/comment', commentRoute);

module.exports = route;
