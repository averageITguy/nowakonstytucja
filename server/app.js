'use strict';

//run appropriate server depending on the environment
if(process.env.NODE_ENV === 'development') {
    require('./config/server.dev');
} else {
    require('./config/server.prod');
}
