const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

let url = '';
if(process.env.NODE_ENV === 'development') {
    url = 'mongodb://localhost/constitution';
} else {
    url = 'mongodb://uakhut7svqrtwwp:fu1NW91murxM8usaknwf@bzjazdickcdqpmo-mongodb.services.clever-cloud.com:27017/bzjazdickcdqpmo';
}

const options = {
    promiseLibrary: global.Promise,
    //useMongoClient: true,
};

function connect() {
    mongoose.connect(url, options)
        .then(()=> {
            const admin = new mongoose.mongo.Admin(mongoose.connection.db);
            admin.buildInfo(function(err, info) {
                if(err) {
                    console.err(`Error getting MongoDB info: ${err}`);
                } else {
                    console.log(`Connection to MongoDB (version ${info.version}) opened successfully!`);
                }
            });
        })
        .catch(err => console.error(`Error connecting to MongoDB: ${err}`));
}

module.exports.connect = connect;
module.exports.url = url;