const apiRoutes = require('../api/api.routes');

module.exports = function (app) {
    app.use('/api', apiRoutes);
};