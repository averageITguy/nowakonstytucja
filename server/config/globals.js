const ERROR_MESSAGES = {
    votes: {
        ALREADY_VOTED: 'Już głosowałeś w tym sondażu'
    }
};

module.exports.ERROR_MESSAGES = ERROR_MESSAGES;
