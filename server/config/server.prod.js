/**
 * This file is used to run production server
 */

/*eslint no-console:0 */
'use strict';


// Include the cluster module
const cluster = require('cluster');

if(cluster.isMaster) {// Code to run if we're in the master process
    // Count the machine's CPUs
    const cpuCount = require('os').cpus().length;

    console.log('Master cluster setting up ' + cpuCount + ' workers...');

    // Create a worker for each CPU
    for(var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    cluster.on('online', function(worker) {
        console.log('Worker ' + worker.process.pid + ' is online');
    });

    cluster.on('exit', function(worker, code, signal) {
        console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
        console.log('Starting a new worker');
        cluster.fork();
    });

} else {// Code to run if we're in a worker process
    const path = require('path');
    const express = require('express');

    const port = (process.env.PORT || 8080);

    const app = express();

    const cookieParser = require('cookie-parser');
    const bodyParser = require('body-parser');
    var serveIndex = require('serve-index');
    const session = require('express-session');
    const requestIp = require('request-ip');
    const MongoDBStore = require('connect-mongodb-session')(session);
    const store = new MongoDBStore({
        uri: require('./db').url,
        collection: 'mySessions'
    });

    const helmet = require('helmet');

    const passport = require('passport');

    //configure authentication
    require('../auth/auth.passport')(passport);

    app.use(bodyParser.json());
    app.use(cookieParser('da asd as89d 7as89d7 89as7d 98792719d 712897 9asd7s 9as9d 7asdad s'));
    app.use(helmet());
    app.use(session({
        secret: 'this is very secret alans secret zxc7zx86c78zx6c8z6x78c6zx8czx',
        resave: true,
        saveUninitialized: true,
        store: store,
        sameSite: true,
        cookie: {
            maxAge: 1000 * 60 * 60 * 24 * 7 // 1 week
        }
    }));
    app.use(requestIp.mw());

    app.use(passport.initialize());
    app.use(passport.session()); // persistent login sessions

    //configure and connect to db
    require('./db').connect();

    //configure expressJs API routes
    require('./routes')(app);

    app.use('/files', serveIndex('uploads'));
    app.use('/files', express.static('uploads'));

    require('../auth/auth.routes.js').routes(app, passport); // load our routes and pass in our app and fully configured passport

    const indexPath = path.join(__dirname, '../../dist', 'index.html');
    const publicPath = express.static('dist');

    app.use('/', publicPath);
    app.get('/*', (req, res) => {
        res.sendFile(indexPath);
    });

    app.listen(port, function() {
        console.log('Production server is running on port: %d', port);
    });
}
