/**
 * This file is used to run development server
 */

/*eslint no-console:0 */
'use strict';


/// 1. Webpack part of configuration
const express = require('express');

const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');

const config = require('../../webpack.config');
config.devServer.setup = setupApp;

const compiler = webpack(config);

new WebpackDevServer(compiler, config.devServer)
    .listen(config.port, 'localhost', err => {
        if (err) {
            console.log(err);
        }
        console.log('Listening at localhost:' + config.port);
    });


/// 2. ExpressJs part of configuration
function setupApp(app) {
    const cookieParser = require('cookie-parser');
    const bodyParser = require('body-parser');
    const session = require('express-session');
    const requestIp = require('request-ip');

    const passport = require('passport');

    //configure authentication
    require('../auth/auth.passport')(passport);

    app.use(bodyParser.json());
    app.use(cookieParser('da asd as89d 7as89d7 89as7d 98792719d 712897 9asd7s 9as9d 7asdad s'));
    app.use(session({
        secret: 'keyboard cat',
        resave: false,
        sameSite: true,
        saveUninitialized: true
    }));
    app.use(requestIp.mw());

    app.use(passport.initialize());
    app.use(passport.session()); // persistent login sessions

    //configure and connect to db
    require('./db').connect();

    //configure expressJs API routes
    require('./routes')(app);

    app.use('/files', express.static('uploads'));

    require('../auth/auth.routes.js').routes(app, passport); // load our routes and pass in our app and fully configured passport
}