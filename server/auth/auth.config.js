let facebookCallback = '';
let facebookClientID = '';
let facebookClientSecret = '';

if (process.env.NODE_ENV === 'development') {
    facebookCallback = 'http://localhost:8000/auth/facebook/callback';
    facebookClientID = '1938971716383339';
    facebookClientSecret = '89ffcf69d4af472d9c45c6e2ccddc0cc';
} else {
    facebookCallback = 'http://nowakonstytucja.org/auth/facebook/callback';
    facebookClientID = '1641793195845728';
    facebookClientSecret = 'e326e640f65359f43d5ecf72da75dac4';
}

module.exports = {
    'facebookAuth': {
        'clientID': facebookClientID,
        'clientSecret': facebookClientSecret,
        'callbackURL': facebookCallback
    },

    'twitterAuth': {
        'consumerKey': 'your-consumer-key-here',
        'consumerSecret': 'your-client-secret-here',
        'callbackURL': 'http://localhost:8080/auth/twitter/callback'
    },

    'googleAuth': {
        'clientID': 'your-secret-clientID-here',
        'clientSecret': 'your-client-secret-here',
        'callbackURL': 'http://localhost:8080/auth/google/callback'
    }

};