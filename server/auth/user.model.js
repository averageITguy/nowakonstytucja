const mongoose = require('mongoose');

const anonymousSchema = mongoose.Schema({
    cliendCode: {
        type: String,
        required: true,
        index: true
    }
});

const userSchema = mongoose.Schema({
    facebook: {
        id: String,
        token: String,
        email: String,
        name: String
    },
    nickname: {
        type: String,
        required: true
    }
});

module.exports.anonymous = mongoose.model('Anonymous', anonymousSchema);
module.exports.user = mongoose.model('user', userSchema);