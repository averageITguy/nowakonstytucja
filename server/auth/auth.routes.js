const Anonymous = require('./user.model').anonymous;

module.exports.routes = function(app, passport) {

    app.get('/auth/getClient/:clientCode', (req, res)=> {
        const code = req.params.clientCode + req.clientIp;

        //return from cookie if exists
        if(req.signedCookies.anonymousID) return res.send({_id: req.signedCookies.anonymousID});

        Anonymous.find({cliendCode: code}, {_id: 1}).lean()
            .then(([client])=> {
                if(client) {
                    res.cookie('anonymousID', client._id, {
                        maxAge: (new Date()).valueOf() + (3 * 365 * 24 * 60 * 60),
                        signed: true,
                        sameSite: true,
                        httpOnly: true
                    });

                    return res.send({_id: client._id});
                } else {
                    const newAnonymous = new Anonymous({cliendCode: code});

                    newAnonymous.save().then(()=> {
                        res.cookie('anonymousID', newAnonymous._id, {
                            maxAge: (new Date()).valueOf() + (3 * 365 * 24 * 60 * 60),
                            signed: true,
                            sameSite: true,
                            httpOnly: true
                        });

                        res.send({_id: newAnonymous._id});
                    });
                }
            })
    });

    // route for facebook authentication and login
    app.get('/auth/facebook', passport.authenticate('facebook', {scope: 'email'}));

    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect: '/closeAfterLoggedIn',
            failureRedirect: '/closeAfterLoggedIn' //TODO: zmienic na jakis ktory tez zamknie ale powie ze sie nie udalo
        }));

    // route for logging out
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    app.get('/auth/user', function(req, res) {
        if(req.user) {
            res.json(req.user);
        } else {
            res.status(401).end();
        }
    });

};

module.exports.onlyModerator = function(req, res, next) {
    if(req.user && req.user._doc.moderator) return next();

    res.status(403).end();
};
